package gov.tangerang.siap.datamiskin.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.adapter.ListPersonalAdapter;
import gov.tangerang.siap.datamiskin.interf.OnLoadMoreListener;
import gov.tangerang.siap.datamiskin.interf.OnRowClickListener;
import gov.tangerang.siap.datamiskin.interf.OnRowLongClickListener;
import gov.tangerang.siap.datamiskin.model.DTOList;
import gov.tangerang.siap.datamiskin.model.response.Warga;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.util.Constant;
import gov.tangerang.siap.datamiskin.util.MyProgressDialog;
import gov.tangerang.siap.datamiskin.util.PrefUtil;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by tri on 8/13/16.
 */
public class ListPersonal extends AppCompatActivity implements OnLoadMoreListener, OnRowClickListener,
        OnRowLongClickListener{

    @Bind(R.id.progresbar)
    ProgressBar mProgresbar;
    @Bind(R.id.recycle_keluarga)
    RecyclerView mRecycleKeluarga;

    int page = 0;
    ListPersonalAdapter adapter;
    LinearLayoutManager mLayoutManager;
    List<Warga> mWargas = new ArrayList<>();
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;
    private String KEYWORD = "";
    private MyProgressDialog mProgresDialog;
    private CancellationTokenSource mCancellation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_list);
        ButterKnife.bind(this);
        mProgresDialog = new MyProgressDialog(this, "Mohon tunggu", false, true, "");
        mCancellation = new CancellationTokenSource();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Task.delay(1000).onSuccess(new Continuation<Void, Response<DTOList<Warga>>>() {
            @Override
            public Response<DTOList<Warga>> then(Task<Void> task) {
                Response<DTOList<Warga>> res = null;
                try {
                    res = new Service().getApi(ListPersonal.this).getPersonalPagging(page, KEYWORD).execute();
                    if (res.isSuccessful()) {
                        return res;
                    }
                } catch (IOException e) {
                    finish();
                    ToastUtil.toastOnUi(ListPersonal.this,getResources().getString(R.string.error_message1));
                }
                return null;
            }
        }, Task.BACKGROUND_EXECUTOR, mCancellation.getToken()).onSuccess(new Continuation<Response<DTOList<Warga>>, Object>() {
            @Override
            public Object then(Task<Response<DTOList<Warga>>> task) throws Exception {
                if(task.getResult() == null){
                    ToastUtil.toastShow(ListPersonal.this,getResources().getString(R.string.error_message1));
                    finish();
                }
                if(task.getResult().body().getErrorCode().equals("20")) {
                    PrefUtil.setStringPref(ListPersonal.this, Constant.isLogedIn, "0");
                    Intent intent = new Intent(ListPersonal.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    ToastUtil.toastOnUi(ListPersonal.this, task.getResult().body().getMessage());
                    finish();
                }else if(task.getResult().body().getErrorCode().equals("00")) {
                    mWargas = task.getResult().body().getData();
//                    mRecycleKeluarga.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(ListPersonal.this);
                    mRecycleKeluarga.setLayoutManager(mLayoutManager);
                    adapter = new ListPersonalAdapter(mWargas, mRecycleKeluarga);
                    mRecycleKeluarga.setAdapter(adapter);
                    adapter.setOnLoadMoreListener(ListPersonal.this);
                    adapter.setOnRowClickListener(ListPersonal.this);
                    adapter.setRowLongClickListener(ListPersonal.this);
                    mProgresbar.setVisibility(View.GONE);
                    mRecycleKeluarga.setVisibility(View.VISIBLE);
                }else{
                    ToastUtil.toastShow(ListPersonal.this,task.getResult().body().getMessage());
                    finish();
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR, mCancellation.getToken());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_search:
                handleMenuSearch();
                return true;
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.menu_search);
        return super.onPrepareOptionsMenu(menu);
    }

    private void doSearch(final String kk) {
        mRecycleKeluarga.setVisibility(View.GONE);
        mProgresbar.setVisibility(View.VISIBLE);
        new Service().getApi(ListPersonal.this).getPersonalPagging(0, kk).enqueue(new Callback<DTOList<Warga>>() {
            @Override
            public void onResponse(Call<DTOList<Warga>> call, Response<DTOList<Warga>> response) {
                mProgresbar.setVisibility(View.GONE);
                mRecycleKeluarga.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().getErrorCode().equals("00")) {
                        if (!response.body().getData().isEmpty()) {
                            KEYWORD = kk;
                            mWargas = response.body().getData();
                            adapter = new ListPersonalAdapter(mWargas, mRecycleKeluarga);
                            mRecycleKeluarga.setAdapter(adapter);
                            adapter.setOnLoadMoreListener(ListPersonal.this);
                            adapter.setOnRowClickListener(ListPersonal.this);
                            adapter.setRowLongClickListener(ListPersonal.this);
                            page = 0;
                        } else {
                            ToastUtil.toastOnUi(ListPersonal.this, "Data tidak ditemukan");
                        }
                    } else {
                        ToastUtil.toastOnUi(ListPersonal.this, response.body().getMessage());
                    }
                } else {
                    ToastUtil.toastOnUi(ListPersonal.this, getResources().getString(R.string.error_message1));
                }
            }

            @Override
            public void onFailure(Call<DTOList<Warga>> call, Throwable t) {
                mProgresbar.setVisibility(View.GONE);
                mRecycleKeluarga.setVisibility(View.VISIBLE);
                ToastUtil.toastOnUi(ListPersonal.this, getResources().getString(R.string.error_message1));
            }
        });
    }

    private void loadMore(String status) {
        page++;
        if (page > 400) {
            mWargas.remove(mWargas.size() - 1);
            adapter.notifyItemRemoved(mWargas.size());
            ToastUtil.toastShow(ListPersonal.this, "Tidak ada lagi data");
            return;
        }
        new Service().getApi(ListPersonal.this).getPersonalPagging(page, status).enqueue(new Callback<DTOList<Warga>>() {
            @Override
            public void onResponse(Call<DTOList<Warga>> call, Response<DTOList<Warga>> response) {
                mWargas.remove(mWargas.size() - 1);
                adapter.notifyItemRemoved(mWargas.size());
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        for (int x = 0; x < response.body().getData().size(); x++) {
                            mWargas.add(response.body().getData().get(x));
                            adapter.notifyItemInserted(mWargas.size());
                        }
                        adapter.setLoaded();
                        if (response.body().getData().size() < 1) {
                            page = 500;
                        }
                    } else {
                        mWargas.remove(mWargas.size() - 1);
                        adapter.notifyItemRemoved(mWargas.size());
                        ToastUtil.toastOnUi(ListPersonal.this, response.body().getMessage());
                    }
                } else {
                    mWargas.remove(mWargas.size() - 1);
                    adapter.notifyItemRemoved(mWargas.size());
                }
            }

            @Override
            public void onFailure(Call<DTOList<Warga>> call, Throwable t) {
                mWargas.remove(mWargas.size() - 1);
                adapter.notifyItemRemoved(mWargas.size());
                ToastUtil.toastOnUi(ListPersonal.this, getResources().getString(R.string.error_message1));
            }
        });
    }

    @Override
    public void onLoadMore() {
        Timber.e(KEYWORD);
        mWargas.add(null);
        adapter.notifyItemInserted(mWargas.size() - 1);
        loadMore(KEYWORD);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCancellation.cancel();
    }

    @Override
    public void onRowClicked(int position) {
        Timber.e(mWargas.get(position).getNik());
        Intent i = new Intent(ListPersonal.this, PersonalProfile.class);
        i.putExtra("nik", mWargas.get(position).getNik());
        i.putExtra("status",mWargas.get(position).getStatusKota());
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        if (isSearchOpened) {
            handleMenuSearch();
            return;
        }
        super.onBackPressed();
    }


    protected void handleMenuSearch() {
        ActionBar action = getSupportActionBar();
        if (isSearchOpened) { //test if the search is open
            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
            action.setDisplayShowTitleEnabled(true); //show the title in the action bar
            InputMethodManager imm = (InputMethodManager) ListPersonal.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtSeach.getWindowToken(), 0);

            mSearchAction.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_search, null));

            isSearchOpened = false;
        } else { //open the search entry
            action.setDisplayShowCustomEnabled(true); //enable it to display a
            action.setCustomView(R.layout.search_bar);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title
            edtSeach = (EditText) action.getCustomView().findViewById(R.id.edtSearch); //the text editor

            edtSeach.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    v.setInputType(InputType.TYPE_CLASS_NUMBER);
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        Timber.e(v.getText().toString());
                        doSearch(v.getText().toString());
                        return true;
                    }
                    return false;
                }
            });

            edtSeach.requestFocus();
            InputMethodManager imm = (InputMethodManager) ListPersonal.this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);
            mSearchAction.setIcon(ContextCompat.getDrawable(ListPersonal.this,
                    android.R.drawable.ic_menu_close_clear_cancel));
            isSearchOpened = true;
        }
    }

    @Override
    public void onLongRowClick(int position) {
        ClipboardManager clipboard = (ClipboardManager) this.getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = android.content.ClipData.newPlainText("Text Coppied", mWargas.get(position).getNik());
        clipboard.setPrimaryClip(clip);
        ToastUtil.toastShow(this,"NIK telah dicopy");
    }
}
