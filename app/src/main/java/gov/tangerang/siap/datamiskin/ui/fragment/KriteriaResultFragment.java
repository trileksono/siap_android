package gov.tangerang.siap.datamiskin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.adapter.GridFotoAdapter;
import gov.tangerang.siap.datamiskin.model.response.KriteriaRes;
import gov.tangerang.siap.datamiskin.ui.dialog.ImgZoom;
import gov.tangerang.siap.datamiskin.util.ExpandableHeightGridView;
import timber.log.Timber;

/**
 * Created by tri on 8/12/16.
 */
public class KriteriaResultFragment extends Fragment {

    @Bind(R.id.tx_penghasilan_rata)
    TextView mTxPenghasilanRata;
    @Bind(R.id.tx_pengeluaran_sebulan)
    TextView mTxPengeluaranSebulan;
    @Bind(R.id.multi_mkn_pokok)
    TextView mMultiMknPokok;
    @Bind(R.id.multi_mkn_lauk)
    TextView mMultiMknLauk;
    @Bind(R.id.multi_mkn_sayur)
    TextView mMultiMknSayur;
    @Bind(R.id.multi_mkn_buah)
    TextView mMultiMknBuah;
    @Bind(R.id.spn_kesehatan1)
    TextView mSpnKesehatan1;
    @Bind(R.id.mlt_kesehatan2)
    TextView mMltKesehatan2;
    @Bind(R.id.txt_pakaian_baru)
    TextView mTxtPakaianBaru;
    @Bind(R.id.tx_pakaian)
    TextView mTxPakaian;
    @Bind(R.id.spn_pendidikan_maks)
    TextView mSpnPendidikanMaks;
    @Bind(R.id.spn_pendidikan_dana)
    TextView mSpnPendidikanDana;
    @Bind(R.id.spn_dinding_rumah)
    TextView mSpnDindingRumah;
    @Bind(R.id.spn_kondisi_dinding)
    TextView mSpnKondisiDinding;
    @Bind(R.id.container_foto_dinding)
    ExpandableHeightGridView mContainerFotoDinding;
    @Bind(R.id.spn_lantai_rumah)
    TextView mSpnLantaiRumah;
    @Bind(R.id.spn_kondisi_lantai)
    TextView mSpnKondisiLantai;
    @Bind(R.id.container_foto_lantai)
    ExpandableHeightGridView mContainerFotoLantai;
    @Bind(R.id.et_lantai_rumah)
    TextView mEtLantaiRumah;
    @Bind(R.id.et_jumlah_keluarga)
    TextView mEtJumlahKeluarga;
    @Bind(R.id.spn_atap_rumah)
    TextView mSpnAtapRumah;
    @Bind(R.id.spn_kondisi_atap)
    TextView mSpnKondisiAtap;
    @Bind(R.id.container_foto_atap)
    ExpandableHeightGridView mContainerFotoAtap;
    @Bind(R.id.spn_mck)
    TextView mSpnMck;
    @Bind(R.id.container_foto_mck)
    ExpandableHeightGridView mContainerFotoMck;
    @Bind(R.id.spn_daya)
    TextView mSpnDaya;
    @Bind(R.id.spn_air_minum)
    TextView mSpnAirMinum;
    @Bind(R.id.penghasilan_tetap)
    TextView mPenghasilanTetap;
    @Bind(R.id.penghasilan_cukup)
    TextView mPenghasilanCukup;
    @Bind(R.id.container_pakaian)
    LinearLayout mContainerPakaian;
    @Bind(R.id.container_pendidikan)
    LinearLayout mContainerPendidikan;
    @Bind(R.id.txt_punya_rumah)
    TextView mTxtPunyaRumah;
    @Bind(R.id.container_daya)
    LinearLayout mContainerDaya;
    @Bind(R.id.container_rumah_ya)
    LinearLayout mContainerRumahYa;
    @Bind(R.id.spn_penerangan)
    TextView spnPenerangan;
    @Bind(R.id.tv_foto_atap)
    TextView tvFotoAtap;
    @Bind(R.id.tv_foto_dinding)
    TextView tvFotoDinding;
    @Bind(R.id.tv_foto_lantai)
    TextView tvFotoLantai;
    @Bind(R.id.tv_foto_mck)
    TextView tvFotoMck;
    @Bind(R.id.txt_susu)
    TextView mTxtSusu;
    @Bind(R.id.progresbar)
    ProgressBar mProgressBar;
    @Bind(R.id.scroll_view)
    ScrollView mScrollView;

    List<KriteriaRes.DatasBean> mKriteriaResList = new ArrayList<>();
    KriteriaRes mKriteriaRes;
    String makanan = "";

    String penghasilanCukup = "";
    String penghasilanTetap = "";
    String penghasilanRata = "";

    String pengeluaranRata = "";
    String pokok = "";
    String lauk = "";
    String sayur = "";
    String buah = "";
    String susu = "";

    String lokasiBerobat = "";
    String penyakit = "";

    String bajuSetahun = "";
    String totalBaju = "";

    String pendidikanAhir = "";
    String sumberDana = "";

    String punyaRumah = "";
    String jenisDinding = "";
    String kondisiDinding = "";
    String lantai = "";
    String kondisiLantai = "";
    String luasLantai = "";
    String jumlahKeluarga = "";
    String atap = "";
    String kondisiAtap = "";
    String mck = "";
    String penerangan = "";
    String daya = "";
    String minum = "";

    private List<String> fotoDinding = new ArrayList<>();
    private List<String> fotoLantai = new ArrayList<>();
    private List<String> fotoMck = new ArrayList<>();
    private List<String> fotoAtap = new ArrayList<>();

    private GridFotoAdapter adapterDinding;
    private GridFotoAdapter adapterLantai;
    private GridFotoAdapter adapterMck;
    private GridFotoAdapter adapterAtap;

    ImgZoom imgZoom;

    public static KriteriaResultFragment instance(KriteriaRes kriteriaRes) {
        KriteriaResultFragment fragment = new KriteriaResultFragment();
        Bundle args = new Bundle();
        args.putParcelable("response", kriteriaRes);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.kriteria_frag, container, false);
        ButterKnife.bind(this, mView);
        mKriteriaRes = getArguments().getParcelable("response");
        Timber.d(mKriteriaRes.toString());
        initView();
        return mView;
    }

    private void initView() {
        final DecimalFormat df = new DecimalFormat("#,###");
        Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                mKriteriaResList = mKriteriaRes.getDatas();
                for (int x = 0; x < mKriteriaResList.size(); x++) {
                    if (mKriteriaResList.get(x).getIdKriteria() == 2) {
                        if (mKriteriaResList.get(x).getKeterangan().equals("1")) {
                            penghasilanTetap = "Ya";
                        } else {
                            penghasilanTetap = "Tidak";
                        }
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 3) {
                        if (mKriteriaResList.get(x).getKeterangan().equals("1")) {
                            penghasilanCukup = "Ya";
                        } else {
                            penghasilanCukup = "Tidak";
                        }
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 4) {
                        penghasilanRata = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 6) {
                        pengeluaranRata = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 9) {
                        lokasiBerobat = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 10) {
                        penyakit = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 12) {
                        if (mKriteriaResList.get(x).getKeterangan().equals("1")) {
                            bajuSetahun = "Ya";
                        } else {
                            bajuSetahun = "Tidak";
                            mContainerPakaian.setVisibility(View.GONE);
                        }
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 13) {
                        totalBaju = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 15) {
                        pendidikanAhir = mKriteriaResList.get(x).getKeterangan();
                        if(pendidikanAhir.equals("Tidak Sekolah")){
                            mContainerPendidikan.setVisibility(View.GONE);
                        }
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 16) {
                        sumberDana = mKriteriaResList.get(x).getKeterangan();

                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 18) {
                        if (mKriteriaResList.get(x).getKeterangan().equals("1")) {
                            punyaRumah = "Ya";
                        } else {
                            punyaRumah = "Tidak";
                            mContainerRumahYa.setVisibility(View.GONE);
                        }
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 19) {
                        jenisDinding = mKriteriaResList.get(x).getKeterangan();

                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 20) {
                        kondisiDinding = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 22) {
                        lantai = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 23) {
                        kondisiLantai = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 24) {
                        luasLantai = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 25) {
                        jumlahKeluarga = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 27) {
                        atap = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 28) {
                        kondisiAtap = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 30) {
                        mck = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 32) {
                        penerangan = mKriteriaResList.get(x).getKeterangan();
                        if (!penerangan.equals("Listrik")) {
                            mContainerDaya.setVisibility(View.GONE);
                        }
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 33) {
                        daya = mKriteriaResList.get(x).getKeterangan();
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 35) {
                        minum = mKriteriaResList.get(x).getKeterangan();
                    }
                    //makanan
                    if (mKriteriaResList.get(x).getIdKriteria() == 7) {
                        makanan = mKriteriaResList.get(x).getKeterangan();
                        pokok = makanan.split(";")[0];
                        lauk = makanan.split(";")[1];
                        sayur = makanan.split(";")[2];
                        buah = makanan.split(";")[3];
                        susu = makanan.split(";")[4];
                        if(susu.equals("1")){
                            susu = "Susu:Ya";
                        }else{
                            susu = "Susu:Tidak";
                        }
                    }
                    //fotoDinding
                    if (mKriteriaResList.get(x).getIdKriteria() == 21) {
                        String foto = mKriteriaResList.get(x).getKeterangan();
                        for (int f = 0; f < foto.split("::").length; f++) {
                            fotoDinding.add(foto.split("::")[f]);
                        }
                        if (!fotoDinding.isEmpty()) {
                            tvFotoDinding.setVisibility(View.VISIBLE);
                        }
                    }

                    if (mKriteriaResList.get(x).getIdKriteria() == 26) {
                        String foto = mKriteriaResList.get(x).getKeterangan();
                        for (int f = 0; f < foto.split("::").length; f++) {
                            fotoLantai.add(foto.split("::")[f]);
                        }
                        if (!fotoLantai.isEmpty()) {
                            tvFotoLantai.setVisibility(View.VISIBLE);
                        }
                    }
                    if (mKriteriaResList.get(x).getIdKriteria() == 29) {
                        String foto = mKriteriaResList.get(x).getKeterangan();
                        for (int f = 0; f < foto.split("::").length; f++) {
                            fotoAtap.add(foto.split("::")[f]);
                        }
                        if (!fotoAtap.isEmpty()) {
                            tvFotoAtap.setVisibility(View.VISIBLE);
                        }
                    }

                    if (mKriteriaResList.get(x).getIdKriteria() == 31) {
                        String foto = mKriteriaResList.get(x).getKeterangan();
                        for (int f = 0; f < foto.split("::").length; f++) {
                            fotoMck.add(foto.split("::")[f]);
                        }
                        if (!fotoMck.isEmpty()) {
                            tvFotoMck.setVisibility(View.VISIBLE);
                        }
                    }
                }
                return null;
            }
        }).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {
                mPenghasilanCukup.setText(penghasilanCukup);
                mPenghasilanTetap.setText(penghasilanTetap);
                mTxPenghasilanRata.setText(df.format(Double.parseDouble(penghasilanRata)));
                mTxPengeluaranSebulan.setText(df.format(Double.parseDouble(pengeluaranRata)));
                mMultiMknPokok.setText(pokok);
                mMultiMknLauk.setText(lauk);
                mMultiMknBuah.setText(buah);
                mMultiMknSayur.setText(sayur);
                mTxtSusu.setText(susu);
                mSpnKesehatan1.setText(lokasiBerobat);
                mMltKesehatan2.setText(penyakit);

                mTxtPakaianBaru.setText(bajuSetahun);
                mTxPakaian.setText(totalBaju);
                mSpnPendidikanMaks.setText(pendidikanAhir);
                mSpnPendidikanDana.setText(sumberDana);

                mTxtPunyaRumah.setText(punyaRumah);
                mSpnDindingRumah.setText(jenisDinding);
                mSpnKondisiDinding.setText(kondisiDinding);
                mSpnLantaiRumah.setText(lantai);
                mSpnKondisiLantai.setText(kondisiLantai);
                mEtLantaiRumah.setText(luasLantai);
                mEtJumlahKeluarga.setText(jumlahKeluarga);
                mSpnAtapRumah.setText(atap);
                mSpnKondisiAtap.setText(kondisiAtap);
                mSpnMck.setText(mck);
                spnPenerangan.setText(penerangan);
                mSpnDaya.setText(daya);
                mSpnAirMinum.setText(minum);
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);

        adapterAtap = new GridFotoAdapter(fotoAtap, getActivity().getApplicationContext(), false, 0);
        adapterDinding = new GridFotoAdapter(fotoDinding, getActivity().getApplicationContext(), false, 0);
        adapterLantai = new GridFotoAdapter(fotoLantai, getActivity().getApplicationContext(), false, 0);
        adapterMck = new GridFotoAdapter(fotoMck, getActivity().getApplicationContext(), false, 0);

        mContainerFotoDinding.setAdapter(adapterDinding);
        mContainerFotoDinding.setExpanded(true);
        mContainerFotoAtap.setAdapter(adapterAtap);
        mContainerFotoAtap.setExpanded(true);
        mContainerFotoLantai.setAdapter(adapterLantai);
        mContainerFotoLantai.setExpanded(true);
        mContainerFotoMck.setAdapter(adapterMck);
        mContainerFotoMck.setExpanded(true);

        adapterDinding.setOnImageClick(new GridFotoAdapter.OnImageClick() {
            @Override
            public void onImageClickListener(int position, int v) {
                ImgZoom imgZoom = new ImgZoom(getActivity(), fotoDinding.get(position));
                imgZoom.setCanceledOnTouchOutside(true);
                imgZoom.show();
            }
        });
        adapterAtap.setOnImageClick(new GridFotoAdapter.OnImageClick() {
            @Override
            public void onImageClickListener(int position, int v) {
                ImgZoom imgZoom = new ImgZoom(getActivity(), fotoAtap.get(position));
                imgZoom.setCanceledOnTouchOutside(true);
                imgZoom.show();
            }
        });
        adapterLantai.setOnImageClick(new GridFotoAdapter.OnImageClick() {
            @Override
            public void onImageClickListener(int position, int v) {
                ImgZoom imgZoom = new ImgZoom(getActivity(), fotoLantai.get(position));
                imgZoom.setCanceledOnTouchOutside(true);
                imgZoom.show();
            }
        });
        adapterMck.setOnImageClick(new GridFotoAdapter.OnImageClick() {
            @Override
            public void onImageClickListener(int position, int v) {
                ImgZoom imgZoom = new ImgZoom(getActivity(), fotoMck.get(position));
                imgZoom.setCanceledOnTouchOutside(true);
                imgZoom.show();
            }
        });

        mProgressBar.setVisibility(View.GONE);
        mScrollView.setVisibility(View.VISIBLE);
    }
}

