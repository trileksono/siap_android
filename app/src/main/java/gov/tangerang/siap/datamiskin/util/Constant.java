package gov.tangerang.siap.datamiskin.util;

/**
 * Created by tri on 7/27/16.
 */
public class Constant {

//    public static String URL_BASE = "http://10.0.3.2:8080/"; // GENYMOTION
//    public static String IMG_URL = "http://10.0.3.2:8080/"; // GENYMOTION

//    public static String URL_BASE = "http://192.168.1.93:8080/"; //TLR
//    public static String IMG_URL = "http://192.168.1.93:8080/";
//
//    public static String URL_BASE = "http://192.168.0.11:8080/"; //Rumah
//    public static String IMG_URL = "http://192.168.0.11:8080/"; //Rumah

    public static String URL_BASE = "http://opendata.tangerangkota.go.id:8080/";
    public static String IMG_URL = "http://opendata.tangerangkota.go.id:8080/";

    public static long CONNECT_TIME_OUT = 15;
    public static long WRITE_TIME_OUT = 15;
    public static long READ_TIME_OUT = 30;
    public static String IMAGE_DIRECTORY_NAME = "SIAP";

    public static String PREF_SES= "SIAP_PREF";
    public static String PREF_KK = "kk";
    public static String NIP = "nip";
    public static String isLogedIn = "isLogedIn";

}
