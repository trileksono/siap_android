package gov.tangerang.siap.datamiskin.database;

/**
 * Created by tri on 7/29/16.
 */
public class DbKecamatan {

    int idKecamatan;
    String nama;

    public DbKecamatan() {
    }

    public DbKecamatan(int idKecamatan, String nama) {
        this.idKecamatan = idKecamatan;
        this.nama = nama;
    }

    public DbKecamatan(String nama) {
        this.nama = nama;
    }

    public int getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(int idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
