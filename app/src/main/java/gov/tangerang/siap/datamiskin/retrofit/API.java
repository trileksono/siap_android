package gov.tangerang.siap.datamiskin.retrofit;

import java.util.List;

import gov.tangerang.siap.datamiskin.model.DTO;
import gov.tangerang.siap.datamiskin.model.DTOList;
import gov.tangerang.siap.datamiskin.model.KriteriaReq;
import gov.tangerang.siap.datamiskin.model.LoginRequest;
import gov.tangerang.siap.datamiskin.model.response.CariKeluarga;
import gov.tangerang.siap.datamiskin.model.response.FotoKeluarga;
import gov.tangerang.siap.datamiskin.model.response.KriteriaRes;
import gov.tangerang.siap.datamiskin.model.response.ListJaminan;
import gov.tangerang.siap.datamiskin.model.response.LoginResp;
import gov.tangerang.siap.datamiskin.model.response.Warga;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by tri on 7/27/16.
 */
public interface API {

    @POST("api/login")
    Call<DTO<LoginResp>> doLogin(@Body LoginRequest loginRequest, @Header("Content-Type") String contentType);

    @GET("api/findKeluarga?")
    Call<DTO<CariKeluarga>> findKeluarga(@Query("nik") String nik);

    @GET("api/jaminan?")
    Call<DTOList<ListJaminan>> getJaminan(@Query("tipe") String tipe);

    @GET("api/cariPenduduk")
    Call<DTO<Warga>> cariPenduduk(@Query("nik") String nik);

    @GET("api/findPenduduk")
    Call<DTO<Warga>> findPenduduk(@Query("nik") String nik);

    @POST("api/jPersonal")
    Call<DTO> simpanPenduduk(@Body RequestBody requestBody);

    @POST("api/updateJPersonal")
    Call<DTO> updatePenduduk(@Body RequestBody requestBody);

    @POST("api/profilePenduduk/{nik}")
    Call<DTO> updateFotoPersonal(@Body RequestBody requestBody, @Path("nik") String nik);

    @GET("api/findJKeluarga")
    Call<DTO<CariKeluarga>> cariJKeluarga(@Query("kk") String kk);

    @POST("api/jKeluarga")
    Call<DTO> saveJKeluarga(@Body RequestBody requestBody);

    @POST("api/updateJKeluarga")
    Call<DTOList<KriteriaRes.DatasBean>> updateJKeluarga(@Body RequestBody requestBody);

    @POST("api/addFotoKeluarga/{kk}")
    Call<DTOList<FotoKeluarga>> tambahFoto(@Body RequestBody requestBody, @Path("kk") String kk);

    @POST("api/kriteriaKeluarga")
    Call<DTO> saveKriteria(@Body RequestBody body);

    @GET("api/getKriteriaKeluarga")
    Call<DTO<KriteriaRes>> getAllKriteria(@Query("kk") String kk);

    @POST("api/updateKriteriaKeluarga")
    Call<DTO> updateKriteriaKeluarga(@Body List<KriteriaReq> list);

    @POST("api/updateFotoLantai")
    Call<DTOList> updateFotoLantai(@Body RequestBody body);

    @POST("api/updateFotoAtap")
    Call<DTOList> updateFotoAtap(@Body RequestBody body);

    @POST("api/updateFotoDinding")
    Call<DTOList> updateFotoDinding(@Body RequestBody body);

    @POST("api/updateFotoMck")
    Call<DTOList> updateFotoMck(@Body RequestBody body);

    @GET("api/getKeluarga/{page}?")
    Call<DTOList<CariKeluarga>> getKeluargaPagging(@Path("page") int page, @Query("noKk") String kk);

    @GET("api/getPersonal/{page}?")
    Call<DTOList<Warga>> getPersonalPagging(@Path("page") int page, @Query("nik") String nik);
}
