package gov.tangerang.siap.datamiskin.ui.fragment;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.adapter.MenuAdapter;
import gov.tangerang.siap.datamiskin.ui.JKeluargaActivity;
import gov.tangerang.siap.datamiskin.ui.JPersonalActivity;
import gov.tangerang.siap.datamiskin.ui.ListKeluargaActivity;
import gov.tangerang.siap.datamiskin.ui.ListPersonal;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import timber.log.Timber;

/**
 * Created by tri on 8/11/16.
 */
public class FragmentMenu extends Fragment implements MenuAdapter.OnClickMenu {

    private String TAG = "FRAGMENT_HOME";

    @Bind(R.id.recycle_menu)
    RecyclerView mRecycleMenu;

    private FragmentManager fm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, mView);

        ActionBar bar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(false);
        MenuAdapter adapter = new MenuAdapter(getActivity().getApplicationContext());
        mRecycleMenu.setHasFixedSize(true);
        mRecycleMenu.setLayoutManager(new GridLayoutManager(getActivity().getApplicationContext(), 2));
        mRecycleMenu.setAdapter(adapter);

        adapter.setOnClickMenu(this);
        fm = getFragmentManager();
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClickMenu(int position) {
        FragmentTransaction transaction = fm.beginTransaction();
        switch (position) {
            case 0:
                startActivity(new Intent(getActivity().getApplicationContext(), ListKeluargaActivity.class));
                break;
            case 1:
                startActivity(new Intent(getActivity().getApplicationContext(), ListPersonal.class));
                break;
            case 3:
                startActivity(new Intent(getActivity().getApplicationContext(), JPersonalActivity.class));
                break;
            case 2:
                if (Build.VERSION.SDK_INT >= 23) {
                    Nammu.askForPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION, new PermissionCallback() {
                        @Override
                        public void permissionGranted() {
                            startActivity(new Intent(getActivity().getApplicationContext(), JKeluargaActivity.class));
                        }

                        @Override
                        public void permissionRefused() {
//                            ToastUtil.toastShow(getActivity(), "Dibutuhkan kordinat sebagai detail lokasi keluarga miskin");
                            Nammu.askForPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION, new PermissionCallback() {
                                @Override
                                public void permissionGranted() {
                                    startActivity(new Intent(getActivity().getApplicationContext(), JKeluargaActivity.class));
                                }

                                @Override
                                public void permissionRefused() {
                                    ToastUtil.toastShow(getActivity(),"Aplikasi tidak diizinkan");
                                }
                            });
                        }

                    });
                } else {
                    startActivity(new Intent(getActivity().getApplicationContext(), JKeluargaActivity.class));
                }
                break;
        }
        Timber.i("" + position);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
