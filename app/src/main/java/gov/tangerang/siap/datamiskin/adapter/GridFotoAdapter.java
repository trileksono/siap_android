package gov.tangerang.siap.datamiskin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.util.Constant;
import timber.log.Timber;

/**
 * Created by tri on 7/31/16.
 */
public class GridFotoAdapter extends BaseAdapter {

    private List<String> pathFoto;
    private Context mContext;
    private LayoutInflater inflater;
    private onDeleteImage deleteImageListener;
    private OnImageClick mOnImageClick;
    private boolean canDelete;
    private int jenis;

    public interface onDeleteImage{
        public void onDeleteImageListener(int position, int jenis);
    }
    public void setOnDeleteImage(onDeleteImage listener) {
        this.deleteImageListener = listener;
    }

    public interface OnImageClick{
        public void onImageClickListener(int position, int jenis);
    }

    public void setOnImageClick(OnImageClick onImageClick){
        this.mOnImageClick = onImageClick;
    }

    public GridFotoAdapter(List<String> pathFoto, Context mContext,boolean canDelete, int jenis) {
        this.pathFoto = pathFoto;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
        this.canDelete = canDelete;
        this.jenis = jenis;
    }

    public void setData(List<String> pathFoto){
        this.pathFoto = pathFoto;
        notifyDataSetChanged();
    }
    @Override
    public int getCount() {
        return pathFoto.size();
    }

    @Override
    public Object getItem(int position) {
        return pathFoto.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final FotoHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.content_foto_keluarga, parent, false);
            mViewHolder = new FotoHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (FotoHolder) convertView.getTag();
        }
        if (pathFoto.get(position).startsWith("/")) {
            Picasso.with(mContext).load(new File(pathFoto.get(position))).error(R.drawable.no_foto)
                    .fit().centerCrop().into(mViewHolder.imgKeluarga);
        } else {
            Picasso.with(mViewHolder.imgKeluarga.getContext())
                    .load(Constant.IMG_URL+pathFoto.get(position)).error(R.drawable.no_foto).fit().centerCrop()
                    .into(mViewHolder.imgKeluarga);
        }

        if (!canDelete) {
            mViewHolder.imgDelete.setVisibility(View.GONE);
        }

        mViewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(deleteImageListener != null){
                    deleteImageListener.onDeleteImageListener(position,jenis);
                }
            }
        });

        mViewHolder.imgKeluarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnImageClick != null) {
                    Timber.e(pathFoto.get(position));
                    mOnImageClick.onImageClickListener(position,jenis);
                }
            }
        });
        return convertView;
    }

    public void add(String item) {
        pathFoto.add(item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
//        int position = pathFoto.indexOf(item);
        pathFoto.remove(position);
        notifyDataSetChanged();
    }

    public class FotoHolder{
        @Bind(R.id.img_keluarga)
        ImageView imgKeluarga;
        @Bind(R.id.img_delete)
        ImageView imgDelete;

        public FotoHolder(View itemView) {
            ButterKnife.bind(this, itemView);
        }
    }
}
