package gov.tangerang.siap.datamiskin.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.adapter.GridFotoAdapter;
import gov.tangerang.siap.datamiskin.model.DTO;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.ui.dialog.ImgZoom;
import gov.tangerang.siap.datamiskin.util.Constant;
import gov.tangerang.siap.datamiskin.util.ExpandableHeightGridView;
import gov.tangerang.siap.datamiskin.util.ImgCompress;
import gov.tangerang.siap.datamiskin.util.ImgUri;
import gov.tangerang.siap.datamiskin.util.MultiSpinner;
import gov.tangerang.siap.datamiskin.util.MyProgressDialog;
import gov.tangerang.siap.datamiskin.util.NumberFormat;
import gov.tangerang.siap.datamiskin.util.PrefUtil;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by tri on 7/28/16.
 */
public class KriteriaNewActivity extends AppCompatActivity implements MultiSpinner.MultiSpinnerListener,
        RadioGroup.OnCheckedChangeListener, GridFotoAdapter.OnImageClick, GridFotoAdapter.onDeleteImage {

    @Bind(R.id.rg_penghasilan_tetap_ya)
    RadioButton mRgPenghasilanTetapYa;
    @Bind(R.id.rg_penghasilan_tetap_tidak)
    RadioButton mRgPenghasilanTetapTidak;
    @Bind(R.id.rg_penghasilan_tetap)
    RadioGroup mRgPenghasilanTetap;
    @Bind(R.id.rg_penghasilan_cukup_ya)
    RadioButton mRgPenghasilanCukupYa;
    @Bind(R.id.rg_penghasilan_cukup_tidak)
    RadioButton mRgPenghasilanCukupTidak;
    @Bind(R.id.rg_penghasilan_cukup)
    RadioGroup mRgPenghasilanCukup;
    @Bind(R.id.tx_penghasilan_rata)
    EditText mTxPenghasilanRata;
    @Bind(R.id.tx_pengeluaran_sebulan)
    EditText mTxPengeluaranSebulan;
    @Bind(R.id.multi_mkn_pokok)
    MultiSpinner mMultiMknPokok;
    @Bind(R.id.multi_mkn_lauk)
    MultiSpinner mMultiMknLauk;
    @Bind(R.id.multi_mkn_sayur)
    MultiSpinner mMultiMknSayur;
    @Bind(R.id.multi_mkn_buah)
    MultiSpinner mMultiMknBuah;
    @Bind(R.id.spn_kesehatan1)
    Spinner mSpnKesehatan1;
    @Bind(R.id.mlt_kesehatan2)
    MultiSpinner mMltKesehatan2;
    @Bind(R.id.rb_pakaian_ya)
    RadioButton mRbPakaianYa;
    @Bind(R.id.rb_pakaian_tidak)
    RadioButton mRbPakaianTidak;
    @Bind(R.id.rg_pakaian)
    RadioGroup mRgPakaian;
    @Bind(R.id.tx_pakaian)
    EditText mTxPakaian;
    @Bind(R.id.spn_pendidikan_maks)
    Spinner mSpnPendidikanMaks;
    @Bind(R.id.spn_pendidikan_dana)
    Spinner mSpnPendidikanDana;
    @Bind(R.id.rb_rumah1_ya)
    RadioButton mRbRumah1Ya;
    @Bind(R.id.rb_rumah1_tidak)
    RadioButton mRbRumah1Tidak;
    @Bind(R.id.rg_rumah1)
    RadioGroup mRgRumah1;
    @Bind(R.id.spn_dinding_rumah)
    Spinner mSpnDindingRumah;
    @Bind(R.id.spn_kondisi_dinding)
    Spinner mSpnKondisiDinding;
    @Bind(R.id.btn_foto_dinding)
    Button mBtnFotoDinding;
    @Bind(R.id.spn_lantai_rumah)
    Spinner mSpnLantaiRumah;
    @Bind(R.id.spn_kondisi_lantai)
    Spinner mSpnKondisiLantai;
    @Bind(R.id.btn_foto_lantai)
    Button mBtnFotoLantai;
    @Bind(R.id.et_lantai_rumah)
    EditText mEtLantaiRumah;
    @Bind(R.id.et_jumlah_keluarga)
    EditText mEtJumlahKeluarga;
    @Bind(R.id.spn_atap_rumah)
    Spinner mSpnAtapRumah;
    @Bind(R.id.spn_mck)
    Spinner mSpnMck;
    @Bind(R.id.btn_foto_mck)
    Button mBtnFotoMck;
    @Bind(R.id.spn_penerangan)
    Spinner mSpnPenerangan;
    @Bind(R.id.spn_daya)
    Spinner mSpnDaya;
    @Bind(R.id.container_rumah_ya)
    LinearLayout mContainerRumahYa;
    @Bind(R.id.spn_air_minum)
    Spinner mSpnAirMinum;
    @Bind(R.id.container_foto_dinding)
    ExpandableHeightGridView mViewFotoDinding;
    @Bind(R.id.container_foto_lantai)
    ExpandableHeightGridView mViewFotoLantai;
    @Bind(R.id.container_foto_mck)
    ExpandableHeightGridView mViewFotoMck;
    @Bind(R.id.btn_simpan)
    Button btnSave;
    @Bind(R.id.container_pakaian)
    LinearLayout mContainerPakaian;
    @Bind(R.id.container_daya)
    LinearLayout mContainerDaya;
    @Bind(R.id.container_pendidikan)
    LinearLayout mContainerPendidikan;
    @Bind(R.id.spn_kondisi_atap)
    Spinner mSpnKondisiAtap;
    @Bind(R.id.container_foto_atap)
    ExpandableHeightGridView mContainerFotoAtap;
    @Bind(R.id.btn_foto_atap)
    Button mBtnFotoAtap;
    @Bind(R.id.rb_susu_ya)
    RadioButton mRbSusuYa;
    @Bind(R.id.rb_susu_tidak)
    RadioButton mRbSusuTidak;
    @Bind(R.id.rg_susu)
    RadioGroup mRgSusu;

    private String[] pokok;
    private String[] lauk;
    private String[] sayur;
    private String[] buah;
    private String[] berobat;
    private String[] penyakit;
    private String[] pendidikan;
    private String[] biayaPendidikan;
    private String[] dinding;
    private String[] kondisiFull;
    private String[] kondisi;
    private String[] lantai;
    private String[] lampu;
    private String[] atap;
    private String[] mck;
    private String[] daya;
    private String[] air;

    private GridFotoAdapter mAdapterDinding;
    private GridFotoAdapter mAdapterLantai;
    private GridFotoAdapter mAdapterMck;
    private GridFotoAdapter mAdapterAtap;

    private List<String> mListFotoDinding = new ArrayList<>();
    private List<String> mListFotoMck = new ArrayList<>();
    private List<String> mListFotoLantai = new ArrayList<>();
    private List<String> mListFotoAtap = new ArrayList<>();

    private int REQUEST_FILE_DINDING = 201;
    private int REQUEST_FILE_LANTAI = 301;
    private int REQUEST_FILE_MCK = 401;
    private int REQUEST_CAMERA_DINDING = 200;
    private int REQUEST_CAMERA_LANTAI = 300;
    private int REQUEST_CAMERA_MCK = 400;
    private int REQUEST_CAMERA_ATAP = 500;
    private int REQUEST_FILE_ATAP = 501;

    private ImgCompress mImgCompress;
    private Uri fileUri; // file url to store image
    private String mfilePath;
    private MyProgressDialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kriteria_base);
        ButterKnife.bind(this);

        mTxPengeluaranSebulan.addTextChangedListener(new NumberFormat(mTxPengeluaranSebulan));
        mTxPenghasilanRata.addTextChangedListener(new NumberFormat(mTxPenghasilanRata));

        Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                initializeAdapter();
                return null;
            }
        }).continueWith(new Continuation<Object, Object>() {
            @Override
            public Object then(Task<Object> task) throws Exception {
                initializeSpinner();
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);

        mImgCompress = new ImgCompress();
        mDialog = new MyProgressDialog(this, "Mohon tunggu..", false, true, "");
    }

    @Override
    public void onItemsSelected(boolean[] selected) {
    }

    @OnClick(R.id.btn_simpan)
    void saveKriteria() {

        if (mTxPenghasilanRata.getText().toString().equals("")) {
            mTxPenghasilanRata.setError("Silahkan diisi terlebih dahulu");
            return;
        }
        if (mTxPengeluaranSebulan.getText().toString().equals("")) {
            mTxPengeluaranSebulan.setError("Silahkan diisi terlebih dahulu");
            return;
        }

        if (mMltKesehatan2.getText().toString().equals("")) {
            ToastUtil.toastShow(KriteriaNewActivity.this, "Silahkan pilih minimal satu penyakit");
            return;
        }
        if (mMultiMknPokok.getText().toString().equals("") || mMultiMknLauk.getText().toString().equals("")) {
            ToastUtil.toastShow(KriteriaNewActivity.this, "Silahkan lengkapi makanan sehari-hari untuk keluarga ini");
            return;
        }
        if (mMultiMknSayur.getText().toString().equals("") || mMultiMknBuah.getText().toString().equals("")) {
            ToastUtil.toastShow(KriteriaNewActivity.this, "Silahkan lengkapi makanan sehari-hari untuk keluarga ini");
            return;
        }

        int penghasilanTetap = selectedRadio(mRgPenghasilanTetap);
        int penghasilanCukup = selectedRadio(mRgPenghasilanCukup);
        String penghasilanBulan = mTxPenghasilanRata.getText().toString();

        String pengeluaranBulan = mTxPengeluaranSebulan.getText().toString();
        String pokok = mMultiMknPokok.getText().toString();
        String lauk = mMultiMknLauk.getText().toString();
        String sayur = mMultiMknSayur.getText().toString();
        String buah = mMultiMknBuah.getText().toString();
        int susu = selectedRadio(mRgSusu);

        String lokasiBerobat = mSpnKesehatan1.getSelectedItem().toString();
        String penyakit = mMltKesehatan2.getText().toString();

        int pakaianBaru = selectedRadio(mRgPakaian);
        String jumlahPakaian = mTxPakaian.getText().toString();

        String pendidikan1 = mSpnPendidikanMaks.getSelectedItem().toString();
        String pendidikan2 = mSpnPendidikanDana.getSelectedItem().toString();

        int punyaRumah = selectedRadio(mRgRumah1);
        String dinding = (String) mSpnDindingRumah.getSelectedItem();
        String kondisiDinding = mSpnKondisiDinding.getSelectedItem().toString();
        String lantai = mSpnLantaiRumah.getSelectedItem().toString();
        String kondisiLantai = mSpnKondisiLantai.getSelectedItem().toString();
        String luasLantai = mEtLantaiRumah.getText().toString();
        String anggotaKeluarga = mEtJumlahKeluarga.getText().toString();
        String atapRumah = mSpnAtapRumah.getSelectedItem().toString();
        String mck = mSpnMck.getSelectedItem().toString();
        String penerangan = mSpnPenerangan.getSelectedItem().toString();
        String daya = mSpnDaya.getSelectedItem().toString();
        String minum = mSpnAirMinum.getSelectedItem().toString();
        String kondisiAtap = mSpnKondisiAtap.getSelectedItem().toString();

        String makanan = "Pokok:" + pokok + ";Lauk:" + lauk + ";Sayur:" + sayur + ";Buah:" + buah +";Susu:" +susu;

        MultipartBody.Builder body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("kk", PrefUtil.getStringPref(KriteriaNewActivity.this, Constant.PREF_KK, ""))
                .addFormDataPart("2", penghasilanTetap + "")
                .addFormDataPart("3", penghasilanCukup + "")
                .addFormDataPart("4", penghasilanBulan)
                .addFormDataPart("6", pengeluaranBulan)
                .addFormDataPart("7", makanan)
                .addFormDataPart("9", lokasiBerobat)
                .addFormDataPart("10", penyakit)
                .addFormDataPart("12", pakaianBaru + "")
                .addFormDataPart("15", pendidikan1)
                .addFormDataPart("18", punyaRumah + "")
                .addFormDataPart("35", minum);

        if (pakaianBaru == 1) {
            body.addFormDataPart("13", jumlahPakaian);
        }
        if (mSpnPendidikanMaks.getSelectedItemPosition() != 0) {
            body.addFormDataPart("16", pendidikan2);
        }

        if (punyaRumah == 1) {
            body.addFormDataPart("19", dinding)
                    .addFormDataPart("20", kondisiDinding)
                    .addFormDataPart("22", lantai)
                    .addFormDataPart("23", kondisiLantai)
                    .addFormDataPart("24", luasLantai)
                    .addFormDataPart("25", anggotaKeluarga)
                    .addFormDataPart("27", atapRumah)
                    .addFormDataPart("28", kondisiAtap)
                    .addFormDataPart("30", mck)
                    .addFormDataPart("32", penerangan);

            for (String s : mListFotoDinding) {
                File file = new File(s);
                if (file.exists()) {
                    body.addFormDataPart("fotoDinding", file.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), file));
                } else {
                    ToastUtil.toastShow(KriteriaNewActivity.this, "Error : Foto dinding tidak ditemukan");
                    return;
                }
            }
            for (String s : mListFotoLantai) {
                File file = new File(s);
                if (file.exists()) {
                    body.addFormDataPart("fotoLantai", file.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), file));
                    Timber.e("%s", file);
                } else {
                    ToastUtil.toastShow(KriteriaNewActivity.this, "Error : Foto lantai tidak ditemukan");
                    return;
                }
            }
            for (String s : mListFotoAtap) {
                File file = new File(s);
                if (file.exists()) {
                    body.addFormDataPart("fotoAtap", file.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), file));
                    Timber.e("%s", file);
                } else {
                    ToastUtil.toastShow(KriteriaNewActivity.this, "Error : Foto atap tidak ditemukan");
                    return;
                }
            }

            for (String s : mListFotoMck) {
                File file = new File(s);
                if (file.exists()) {
                    body.addFormDataPart("fotoMck", file.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), file));
                    Timber.e("%s", file);
                } else {
                    ToastUtil.toastShow(KriteriaNewActivity.this, "Error : Foto mck tidak ditemukan");
                    return;
                }
            }
        }
        if (penerangan.equals("Listrik")) {
            body.addFormDataPart("33", daya);
        }

        mDialog.show();
        final RequestBody requestBody = body.build();
        new Service().getApi(KriteriaNewActivity.this).saveKriteria(requestBody).enqueue(new Callback<DTO>() {
            @Override
            public void onResponse(Call<DTO> call, Response<DTO> response) {
                mDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getErrorCode().equals("00")) {
                        Intent i = new Intent(KriteriaNewActivity.this, HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                        ToastUtil.toastOnUi(KriteriaNewActivity.this, "Kriteria berhasil disimpan");
                    } else {
                        ToastUtil.toastOnUi(KriteriaNewActivity.this, response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DTO> call, Throwable t) {
                mDialog.dismiss();
                ToastUtil.toastOnUi(KriteriaNewActivity.this, getResources().getString(R.string.error_message1));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if ((requestCode == REQUEST_CAMERA_DINDING || requestCode == REQUEST_CAMERA_LANTAI)
                    || requestCode == REQUEST_CAMERA_MCK || requestCode == REQUEST_CAMERA_ATAP) {
                try {
                    String path = fileUri.getPath();
                    Timber.e(fileUri.getPath());
                    path = mImgCompress.compressImage(KriteriaNewActivity.this, path);
                    mfilePath = path;
                    Timber.e(path);
                    BitmapFactory.decodeFile(path);

                    if (requestCode == REQUEST_CAMERA_DINDING) {
                        mListFotoDinding.add(path);
                        updateFotoAdapter(mAdapterDinding);
                        checkMaksFoto(mListFotoDinding, mBtnFotoDinding);
                    } else if (requestCode == REQUEST_CAMERA_LANTAI) {
                        mListFotoLantai.add(path);
                        updateFotoAdapter(mAdapterLantai);
                        checkMaksFoto(mListFotoLantai, mBtnFotoLantai);
                    } else if (requestCode == REQUEST_CAMERA_MCK) {
                        mListFotoMck.add(path);
                        updateFotoAdapter(mAdapterMck);
                        checkMaksFoto(mListFotoMck, mBtnFotoMck);
                    } else {
                        mListFotoAtap.add(path);
                        updateFotoAdapter(mAdapterAtap);
                        checkMaksFoto(mListFotoAtap, mBtnFotoAtap);
                    }
                } catch (Exception e) {
                    ToastUtil.toastShow(KriteriaNewActivity.this, "File path tidak ditemukan");
                }
            } else if ((requestCode == REQUEST_FILE_DINDING || requestCode == REQUEST_FILE_LANTAI)
                    || requestCode == REQUEST_FILE_MCK || requestCode == REQUEST_FILE_ATAP) {

                try {

                    Uri selectedImageUri = data.getData();
                    fileUri = selectedImageUri;
                    String selectedImagePath = ImgUri.selectedImagePath(KriteriaNewActivity.this, fileUri);
                    if (null != selectedImagePath) {
                        if (selectedImageUri.toString().contains("images")) {
                            selectedImagePath = mImgCompress.compressImage(KriteriaNewActivity.this,
                                    selectedImagePath);
                            mfilePath = selectedImagePath;
                            BitmapFactory.decodeFile(selectedImagePath);
                            Timber.i("%s", mfilePath);

                            if (requestCode == REQUEST_FILE_DINDING) {
                                mListFotoDinding.add(mfilePath);
                                updateFotoAdapter(mAdapterDinding);
                                checkMaksFoto(mListFotoDinding, mBtnFotoDinding);
                            } else if (requestCode == REQUEST_FILE_LANTAI) {
                                mListFotoLantai.add(mfilePath);
                                updateFotoAdapter(mAdapterLantai);
                                checkMaksFoto(mListFotoLantai, mBtnFotoLantai);
                            } else if (requestCode == REQUEST_FILE_MCK) {
                                mListFotoMck.add(mfilePath);
                                updateFotoAdapter(mAdapterMck);
                                checkMaksFoto(mListFotoMck, mBtnFotoMck);
                            } else {
                                mListFotoAtap.add(mfilePath);
                                updateFotoAdapter(mAdapterAtap);
                                checkMaksFoto(mListFotoAtap, mBtnFotoAtap);
                            }
                        } else {
                            ToastUtil.toastShow(KriteriaNewActivity.this, "Mohon pilih salah satu foto");
                        }
                    } else {
                        Toast.makeText(KriteriaNewActivity.this, "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    ToastUtil.toastShow(KriteriaNewActivity.this, "Error file path tidak ditemukan");
                }
            }
        }
    }

    private void checkMaksFoto(List<String> list, Button b) {
        if (list.size() >= 3) {
            b.setEnabled(false);
        } else {
            b.setEnabled(true);
        }
    }

    void updateFotoAdapter(final GridFotoAdapter adapter) {
        KriteriaNewActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    @OnClick({R.id.btn_foto_dinding, R.id.btn_foto_lantai, R.id.btn_foto_mck, R.id.btn_foto_atap})
    void tambahFotoClick(View v) {
        if (v == mBtnFotoDinding) {
            showDialogPilihan(REQUEST_FILE_DINDING, REQUEST_CAMERA_DINDING);
        } else if (v == mBtnFotoLantai) {
            showDialogPilihan(REQUEST_FILE_LANTAI, REQUEST_CAMERA_LANTAI);
        } else if (v == mBtnFotoMck) {
            showDialogPilihan(REQUEST_FILE_MCK, REQUEST_CAMERA_MCK);
        } else {
            showDialogPilihan(REQUEST_FILE_ATAP, REQUEST_CAMERA_ATAP);
        }
    }

    private void showDialogPilihan(final int FILE_CODE, final int CAMERA_CODE) {
        final CharSequence[] items = {"Ambil Foto", "Cari dari Galery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(KriteriaNewActivity.this);
        builder.setTitle("Masukan Foto");
        builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Ambil Foto")) {
                            cameraIntent(CAMERA_CODE);
                        } else if (items[item].equals("Cari dari Galery")) {
                            galleryIntent(FILE_CODE);
                        }
                    }
                }
        );
        builder.show();
    }

    private void galleryIntent(int FILE_CODE) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, FILE_CODE);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(intent, FILE_CODE);
        }
    }

    private void cameraIntent(int CAMERA_CODE) {
        if (KriteriaNewActivity.this.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = ImgUri.getOutputMediaFileUri();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, CAMERA_CODE);
        } else {
            ToastUtil.toastShow(KriteriaNewActivity.this, "Sorry! Your device doesn't support camera");
        }
    }

    private int selectedRadio(RadioGroup rg) {
        RadioButton selectRadio = (RadioButton) findViewById(rg.getCheckedRadioButtonId());
        if (selectRadio.getText().toString().equals("Ya")) {
            return 1;
        } else {
            return 0;
        }
    }

    private void initializeSpinner() {
        ArrayAdapter arrPokok = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, pokok);
        ArrayAdapter arrLauk = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, lauk);
        ArrayAdapter arrSayur = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, sayur);
        ArrayAdapter arrBuah = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, buah);
        ArrayAdapter arrBerobat = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, berobat);
        ArrayAdapter arrPenyakit = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, penyakit);
        ArrayAdapter arrPendidikan = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, pendidikan);
        ArrayAdapter arrBiayaPendidikan = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, biayaPendidikan);
        ArrayAdapter arrDinding = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, dinding);
        ArrayAdapter arrKondisi = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, kondisi);
        ArrayAdapter arrKondisiFull = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, kondisiFull);
        ArrayAdapter arrLantai = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, lantai);
        ArrayAdapter arrLampu = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, lampu);
        ArrayAdapter arrAtap = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, atap);
        ArrayAdapter arrMck = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, mck);
        ArrayAdapter arrAir = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, air);
        ArrayAdapter arrDaya = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, daya);

        mMultiMknPokok.setAdapter(arrPokok, false, this);
        mMultiMknLauk.setAdapter(arrLauk, false, this);
        mMultiMknSayur.setAdapter(arrSayur, false, this);
        mMultiMknBuah.setAdapter(arrBuah, false, this);
        mSpnKesehatan1.setAdapter(arrBerobat);
        mMltKesehatan2.setAdapter(arrPenyakit, false, this);
        mSpnPendidikanMaks.setAdapter(arrPendidikan);
        mSpnPendidikanDana.setAdapter(arrBiayaPendidikan);
        mSpnDindingRumah.setAdapter(arrDinding);
        mSpnKondisiDinding.setAdapter(arrKondisiFull);
        mSpnLantaiRumah.setAdapter(arrLantai);
        mSpnKondisiLantai.setAdapter(arrKondisi);
        mSpnAtapRumah.setAdapter(arrAtap);
        mSpnMck.setAdapter(arrMck);
        mSpnPenerangan.setAdapter(arrLampu);
        mSpnDaya.setAdapter(arrDaya);
        mSpnAirMinum.setAdapter(arrAir);
        mSpnKondisiAtap.setAdapter(arrKondisi);

        mRgRumah1.setOnCheckedChangeListener(this);
        mRgPakaian.setOnCheckedChangeListener(this);

        mViewFotoDinding.setAdapter(mAdapterDinding);
        mViewFotoLantai.setAdapter(mAdapterLantai);
        mViewFotoMck.setAdapter(mAdapterMck);
        mContainerFotoAtap.setAdapter(mAdapterAtap);
    }

    private void initializeAdapter() {
        pokok = getResources().getStringArray(R.array.makan_pokok);
        lauk = getResources().getStringArray(R.array.makan_lauk);
        sayur = getResources().getStringArray(R.array.makan_sayur);
        buah = getResources().getStringArray(R.array.makan_buah);
        berobat = getResources().getStringArray(R.array.lokasi_berobat);
        penyakit = getResources().getStringArray(R.array.list_penyakit);
        pendidikan = getResources().getStringArray(R.array.list_pendidikan);
        biayaPendidikan = getResources().getStringArray(R.array.biaya_pendidikan);
        dinding = getResources().getStringArray(R.array.list_dinding);
        kondisi = getResources().getStringArray(R.array.list_kondisi);
        kondisiFull = getResources().getStringArray(R.array.list_kondisi_full);
        lantai = getResources().getStringArray(R.array.list_lantai);
        lampu = getResources().getStringArray(R.array.list_lampu);
        atap = getResources().getStringArray(R.array.list_atap);
        mck = getResources().getStringArray(R.array.list_mck);
        air = getResources().getStringArray(R.array.list_air);
        daya = getResources().getStringArray(R.array.list_daya);

        mAdapterDinding = new GridFotoAdapter(mListFotoDinding, this, true, 1);
        mAdapterLantai = new GridFotoAdapter(mListFotoLantai, this, true, 2);
        mAdapterAtap = new GridFotoAdapter(mListFotoAtap, this, true, 3);
        mAdapterMck = new GridFotoAdapter(mListFotoMck, this, true, 4);

        mAdapterDinding.setOnImageClick(this);
        mAdapterDinding.setOnDeleteImage(this);
        mAdapterLantai.setOnImageClick(this);
        mAdapterLantai.setOnDeleteImage(this);
        mAdapterAtap.setOnImageClick(this);
        mAdapterAtap.setOnDeleteImage(this);
        mAdapterMck.setOnImageClick(this);
        mAdapterMck.setOnDeleteImage(this);
    }

    @OnItemSelected({R.id.spn_penerangan, R.id.spn_pendidikan_maks})
    void showData(Spinner spinner, int position) {
        if (mSpnPenerangan.getSelectedItem().equals("Listrik")) {
            mContainerDaya.setVisibility(View.VISIBLE);
        } else {
            mContainerDaya.setVisibility(View.GONE);
        }

        if (mSpnPendidikanMaks.getSelectedItemPosition() == 0) {
            mContainerPendidikan.setVisibility(View.GONE);
        } else {
            mContainerPendidikan.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (group == mRgRumah1) {
            if (mRbRumah1Ya.isChecked()) {
                mContainerRumahYa.setVisibility(View.VISIBLE);
            } else {
                mContainerRumahYa.setVisibility(View.GONE);
            }
        } else if (group == mRgPakaian) {
            if (mRbPakaianYa.isChecked()) {
                mContainerPakaian.setVisibility(View.VISIBLE);
            } else {
                mContainerPakaian.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onImageClickListener(int position, int v) {
        String path = null;
        switch (v) {
            case 1:
                path = mListFotoDinding.get(position);
                break;
            case 2:
                path = mListFotoLantai.get(position);
                break;
            case 3:
                path = mListFotoAtap.get(position);
                break;
            case 4:
                path = mListFotoMck.get(position);
                break;
            default:
                path = null;
        }
        if (path == null) {
            ToastUtil.toastShow(KriteriaNewActivity.this, "Gambar tidak dapat di tampilkan");
            return;
        }

        ImgZoom imgZoom = new ImgZoom(KriteriaNewActivity.this, path);
        imgZoom.setCanceledOnTouchOutside(true);
        imgZoom.show();
    }

    @Override
    public void onDeleteImageListener(int position, int x) {
        switch (x) {
            case 1:
                mAdapterDinding.remove(position);
                break;
            case 2:
                mAdapterLantai.remove(position);
                break;
            case 3:
                mAdapterAtap.remove(position);
                break;
            case 4:
                mAdapterMck.remove(position);
                break;
        }

    }
}
