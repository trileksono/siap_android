package gov.tangerang.siap.datamiskin.interf;

/**
 * Created by tri on 8/23/16.
 */
public interface OnRowLongClickListener {

    public void onLongRowClick(int position);

}
