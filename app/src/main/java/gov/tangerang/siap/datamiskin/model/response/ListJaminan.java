package gov.tangerang.siap.datamiskin.model.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tri on 7/28/16.
 */
public class ListJaminan implements Parcelable {
    private int tipeJaminan;
    private int total;
    private String jaminan;
    private int idJaminan;

    public int getTipeJaminan() {
        return tipeJaminan;
    }

    public void setTipeJaminan(int tipeJaminan) {
        this.tipeJaminan = tipeJaminan;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getNamaJaminan() {
        return jaminan;
    }

    public void setNamaJaminan(String namaJaminan) {
        this.jaminan = namaJaminan;
    }

    public int getIdJaminan() {
        return idJaminan;
    }

    public void setIdJaminan(int idJaminan) {
        this.idJaminan = idJaminan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.tipeJaminan);
        dest.writeInt(this.total);
        dest.writeString(this.jaminan);
        dest.writeInt(this.idJaminan);
    }

    public ListJaminan() {
    }

    protected ListJaminan(Parcel in) {
        this.tipeJaminan = in.readInt();
        this.total = in.readInt();
        this.jaminan = in.readString();
        this.idJaminan = in.readInt();
    }

    public static final Creator<ListJaminan> CREATOR = new Creator<ListJaminan>() {
        @Override
        public ListJaminan createFromParcel(Parcel source) {
            return new ListJaminan(source);
        }

        @Override
        public ListJaminan[] newArray(int size) {
            return new ListJaminan[size];
        }
    };
}