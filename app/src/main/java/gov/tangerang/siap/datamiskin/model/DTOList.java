package gov.tangerang.siap.datamiskin.model;

import java.util.List;

/**
 * Created by tri on 7/27/16.
 */
public class DTOList<T> {

    String message;
    boolean success;
    String errorCode;
    List<T> data;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}