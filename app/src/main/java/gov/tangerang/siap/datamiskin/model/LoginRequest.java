package gov.tangerang.siap.datamiskin.model;

/**
 * Created by tri on 8/1/16.
 */
public class LoginRequest {

    private String username;
    private String password;

    public LoginRequest(String nip, String password) {
        this.username = nip;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
