package gov.tangerang.siap.datamiskin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.List;
import java.util.concurrent.Callable;

import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.database.DbHelper;
import gov.tangerang.siap.datamiskin.database.DbUser;
import gov.tangerang.siap.datamiskin.model.DTO;
import gov.tangerang.siap.datamiskin.model.LoginRequest;
import gov.tangerang.siap.datamiskin.model.response.LoginResp;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.util.Constant;
import gov.tangerang.siap.datamiskin.util.MyProgressDialog;
import gov.tangerang.siap.datamiskin.util.PrefUtil;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by tri on 8/1/16.
 */
public class Login extends AppCompatActivity {

    @Bind(R.id.nip_login)
    AutoCompleteTextView nipLogin;
    @Bind(R.id.password_login)
    EditText passwordLogin;
    @Bind(R.id.btn_login)
    Button btnLogin;
    @Bind(R.id.checkbox_password)
    CheckBox mCheckboxPassword;

    MyProgressDialog progressDialog;
    DbHelper db;
    List listNIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.ic_login));
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        progressDialog = new MyProgressDialog(this, "Mohon tunggu", false, true, "");
        db = new DbHelper(this);
        listNIP = db.getAllUser();

        ArrayAdapter<String> adapterNIP = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listNIP);
        nipLogin.setAdapter(adapterNIP);

        mCheckboxPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    passwordLogin.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }else{
                    passwordLogin.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PrefUtil.getStringPref(this, Constant.isLogedIn, null) == null) {
            return;
        }
        if (PrefUtil.getStringPref(this, Constant.isLogedIn, null).equals("1")) {
            startActivity(new Intent(Login.this, HomeActivity.class));
            finish();
        }
    }

    @OnClick(R.id.btn_login)
    void doLogin() {
        String nip = nipLogin.getText().toString();
        String pass = passwordLogin.getText().toString();
        if (nip.trim().equals("")) {
            nipLogin.setError("Silahkan masukan NIP");
            return;
        }

        if (pass.trim().equals("")) {
            passwordLogin.setError("Silahkan masukan password");
            return;
        }
        progressDialog.show();
        LoginRequest lg = new LoginRequest(nip, pass);
        new Service().getApi(this).doLogin(lg, "application/json").enqueue(new Callback<DTO<LoginResp>>() {
            @Override
            public void onResponse(Call<DTO<LoginResp>> call, Response<DTO<LoginResp>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getErrorCode().equals("00")) {
                        try {
                            createUser(response.body().getData());
                            startActivity(new Intent(Login.this, HomeActivity.class));
                            PrefUtil.setStringPref(Login.this, Constant.isLogedIn, "1");
                            finish();
                        } catch (Exception e) {
                            Timber.e(e.getMessage());
                            ToastUtil.toastShow(Login.this, "11 : Upss, something error");
                        }
                    } else {
                        ToastUtil.toastOnUi(Login.this, response.body().getMessage());
                    }
                } else {
                    ToastUtil.toastOnUi(Login.this, response.message());
                }
            }

            @Override
            public void onFailure(Call<DTO<LoginResp>> call, Throwable t) {
                progressDialog.dismiss();
                if (call.isCanceled()) {
                    Timber.e("Request login was cancel");
                } else {
                    Timber.e(t.getMessage());
                    ToastUtil.toastOnUi(Login.this, "Koneksi error silahkan coba beberapa saat lagi");
                }
            }
        });
    }

    void createUser(final LoginResp lg) {
        Task.call(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                DbUser user = new DbUser();
                user.setIdPegawai(lg.getIdPegawai());
                user.setNip(lg.getNip());
                user.setUserId(lg.getUserId());
                db.insertUser(user);
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }
}
