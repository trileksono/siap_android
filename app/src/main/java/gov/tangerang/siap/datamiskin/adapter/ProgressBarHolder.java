package gov.tangerang.siap.datamiskin.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;

/**
 * Created by tri on 8/11/16.
 */
public class ProgressBarHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.progressBar1)
    ProgressBar progresbar;
    public ProgressBarHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
