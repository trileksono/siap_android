package gov.tangerang.siap.datamiskin.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;

import gov.tangerang.siap.datamiskin.R;

/**
 * Created by tri on 7/27/16.
 */
public class MyProgressDialog  extends ProgressDialog {
    private String pesan;
    private String judul;
    private boolean isCancelable;
    private boolean isDetermine;
    private Context mContext;
    private ProgressDialog p;
    int theme;

    public MyProgressDialog(Context context, String pesan, boolean isCancelable, boolean isDetermine, String judul) {
        super(context);
        this.theme = R.style.DialogCustomTheme;
        this.mContext = context;
        this.isCancelable = isCancelable;
        this.isDetermine = isDetermine;
        this.pesan = pesan;
        this.judul = judul;
    }

    public void show() {
        p = new ProgressDialog(mContext,theme);
        p.setMessage(pesan);
        p.setTitle(judul);
        p.setCancelable(isCancelable);
        p.setIndeterminate(isDetermine);
        p.setIndeterminateDrawable(ContextCompat.getDrawable(mContext, R.drawable.loading));
        p.show();
    }

    public void dismiss() {
        if (p.isShowing()) {
            p.dismiss();
        }
    }
}