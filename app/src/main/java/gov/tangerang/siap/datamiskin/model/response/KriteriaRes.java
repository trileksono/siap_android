package gov.tangerang.siap.datamiskin.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tri on 8/4/16.
 */
public class KriteriaRes implements Parcelable {
    private String kk;
    private List<DatasBean> datas;

    public String getKk() {
        return kk;
    }

    public void setKk(String kk) {
        this.kk = kk;
    }

    public List<DatasBean> getDatas() {
        return datas;
    }

    public void setDatas(List<DatasBean> datas) {
        this.datas = datas;
    }

    public static class DatasBean implements Parcelable {
        private String keterangan;
        private int idKriteria;

        public String getKeterangan() {
            return keterangan;
        }

        public void setKeterangan(String keterangan) {
            this.keterangan = keterangan;
        }

        public int getIdKriteria() {
            return idKriteria;
        }

        public void setIdKriteria(int idKriteria) {
            this.idKriteria = idKriteria;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.keterangan);
            dest.writeInt(this.idKriteria);
        }

        public DatasBean() {
        }

        protected DatasBean(Parcel in) {
            this.keterangan = in.readString();
            this.idKriteria = in.readInt();
        }

        public static final Creator<DatasBean> CREATOR = new Creator<DatasBean>() {
            @Override
            public DatasBean createFromParcel(Parcel source) {
                return new DatasBean(source);
            }

            @Override
            public DatasBean[] newArray(int size) {
                return new DatasBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.kk);
        dest.writeList(this.datas);
    }

    public KriteriaRes() {
    }

    protected KriteriaRes(Parcel in) {
        this.kk = in.readString();
        this.datas = new ArrayList<DatasBean>();
        in.readList(this.datas, DatasBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<KriteriaRes> CREATOR = new Parcelable.Creator<KriteriaRes>() {
        @Override
        public KriteriaRes createFromParcel(Parcel source) {
            return new KriteriaRes(source);
        }

        @Override
        public KriteriaRes[] newArray(int size) {
            return new KriteriaRes[size];
        }
    };
}
