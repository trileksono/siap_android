package gov.tangerang.siap.datamiskin.database;

/**
 * Created by tri on 7/31/16.
 */
public class DbJaminan {

    int idJaminan;
    String nama;
    String tipe;

    public DbJaminan() {
    }

    public DbJaminan(int idJaminan, String nama, String tipe) {
        this.idJaminan = idJaminan;
        this.nama = nama;
        this.tipe = tipe;
    }

    public int getIdJaminan() {
        return idJaminan;
    }

    public void setIdJaminan(int idJaminan) {
        this.idJaminan = idJaminan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }
}
