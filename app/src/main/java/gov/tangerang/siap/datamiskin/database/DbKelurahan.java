package gov.tangerang.siap.datamiskin.database;

/**
 * Created by tri on 7/29/16.
 */
public class DbKelurahan {

    int idKelurahan;
    String nama;
    int idKecamatan;

    public DbKelurahan() {
    }

    public DbKelurahan(int idKelurahan, String nama, int idKecamatan) {
        this.idKelurahan = idKelurahan;
        this.nama = nama;
        this.idKecamatan = idKecamatan;
    }

    public DbKelurahan(String nama, int idKecamatan) {
        this.nama = nama;
        this.idKecamatan = idKecamatan;
    }

    public int getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(int idKelurahan) {
        this.idKelurahan = idKelurahan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(int idKecamatan) {
        this.idKecamatan = idKecamatan;
    }
}
