package gov.tangerang.siap.datamiskin.ui;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.adapter.GridFotoAdapter;
import gov.tangerang.siap.datamiskin.adapter.ListAnggotaKeluargaAdapter;
import gov.tangerang.siap.datamiskin.database.DbHelper;
import gov.tangerang.siap.datamiskin.model.DTO;
import gov.tangerang.siap.datamiskin.model.DTOList;
import gov.tangerang.siap.datamiskin.model.response.AnggotaKeluarga;
import gov.tangerang.siap.datamiskin.model.response.CariKeluarga;
import gov.tangerang.siap.datamiskin.model.response.FotoKeluarga;
import gov.tangerang.siap.datamiskin.model.response.JKeluarga;
import gov.tangerang.siap.datamiskin.model.response.KriteriaRes;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.ui.dialog.ImgZoom;
import gov.tangerang.siap.datamiskin.util.Constant;
import gov.tangerang.siap.datamiskin.util.ExpandableHeightGridView;
import gov.tangerang.siap.datamiskin.util.FindNamaJalan;
import gov.tangerang.siap.datamiskin.util.GPSTracker;
import gov.tangerang.siap.datamiskin.util.ImgCompress;
import gov.tangerang.siap.datamiskin.util.ImgUri;
import gov.tangerang.siap.datamiskin.util.MyProgressDialog;
import gov.tangerang.siap.datamiskin.util.NestedListView;
import gov.tangerang.siap.datamiskin.util.PrefUtil;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by tri on 7/31/16.
 */
public class JKeluargaActivity extends AppCompatActivity implements GridFotoAdapter.onDeleteImage,
        GridFotoAdapter.OnImageClick {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.et_nik)
    TextInputEditText etNik;
    @Bind(R.id.btn_cek)
    Button btnCek;
    @Bind(R.id.et_alamat)
    TextInputEditText etAlamat;
    @Bind(R.id.list_anggota)
    NestedListView listAnggota;
    @Bind(R.id.recycle_foto)
    ExpandableHeightGridView recycleFoto;
    @Bind(R.id.btn_tambahfoto)
    Button btnTambahfoto;
    @Bind(R.id.tbl_jaminan)
    TableLayout tblJaminan;
    @Bind(R.id.btn_updatelokasi)
    Button btnUpdatelokasi;
    @Bind(R.id.btn_simpan)
    Button btnSimpan;
    @Bind(R.id.data_tidak_ada)
    TextView txtKeluargaNull;
    @Bind(R.id.et_lokasi)
    TextInputEditText etLokasi;
    @Bind(R.id.et_kecamatan)
    TextInputEditText etKecamatan;
    @Bind(R.id.et_kelurahan)
    TextInputEditText etKelurahan;
    @Bind(R.id.txt_rt)
    TextInputEditText mTxtRt;
    @Bind(R.id.txt_rw)
    TextInputEditText mTxtRw;

    private int SELECT_FILE = 200;
    private int REQUEST_CAMERA = 100;
    private int REQUEST_MAP = 18;

    private List<AnggotaKeluarga> anggotaList = new ArrayList<>();
    private List<FotoKeluarga> fotoList = new ArrayList<>();
    private List<String> listPath = new ArrayList<>();
    private DbHelper db;
    private List datas;
    private ListAnggotaKeluargaAdapter listViewAdapter;
    private GridFotoAdapter listFotoAdapter;
    private ImgCompress imgCompress;
    private Uri fileUri; // file url to store image
    private GPSTracker gps;
    private double lat;
    private double lang;
    private String namaJalan;
    private String filePath;

    private boolean IS_EDIT = false;

    private MyProgressDialog progressDialog;
    private ProgressBar pgBar;
    private FindNamaJalan findJalan;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jkeluarga);
        ButterKnife.bind(this);
        db = new DbHelper(this);
        gps = new GPSTracker(this);
        findJalan = new FindNamaJalan(this);
        progressDialog = new MyProgressDialog(this, "Mohon tunggu", false, true, "");
        pgBar = new ProgressBar(this);
        imgCompress = new ImgCompress();

        Timber.e("%s", gps.getLatitude() + "  " + gps.getLongitude());
        if (gps.getLatitude() != 0 || gps.getLongitude() != 0) {
            Task.callInBackground(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return findJalan.pengguna(gps.getLatitude(), gps.getLongitude());
                }
            }).onSuccess(new Continuation<String, String>() {
                @Override
                public String then(Task<String> task) throws Exception {
                    Timber.e(task.getResult());
                    etLokasi.setText(task.getResult());
                    lat = gps.getLatitude();
                    lang = gps.getLongitude();
                    return null;
                }
            }, Task.UI_THREAD_EXECUTOR);
        }
        initView();
    }

    private void initView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etLokasi.setText(namaJalan);

        listViewAdapter = new ListAnggotaKeluargaAdapter(anggotaList, JKeluargaActivity.this);
        listAnggota.setAdapter(listViewAdapter);
        listAnggota.setVisibility(View.VISIBLE);

        listFotoAdapter = new GridFotoAdapter(listPath, JKeluargaActivity.this, true, 5);
        recycleFoto.setExpanded(true);
        recycleFoto.setAdapter(listFotoAdapter);
        listFotoAdapter.setOnDeleteImage(this);
        listFotoAdapter.setOnImageClick(this);
        Task.call(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                datas = db.getJaminan("2");
                initialTblJaminan(null);
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

    @OnClick(R.id.btn_simpan)
    void doSave() {
        if (IS_EDIT) {
            update();
        } else {
            simpan();
        }
    }

    private void update() {
        /*if (lat == 0 || lang == 0) {
            ToastUtil.toastShow(JKeluargaActivity.this, "Silahkan masukan lokasi keluarga ini");
            return;
        }*/
        if (mTxtRt.getText().toString().trim().equals("")) {
            mTxtRt.setError("Silahkan diisi");
            return;
        }
        if (mTxtRw.getText().toString().trim().equals("")) {
            mTxtRw.setError("Silahkan diisi");
            return;
        }
        progressDialog.show();
        MultipartBody.Builder body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("kk", etNik.getText().toString())
                .addFormDataPart("alamat", etAlamat.getText().toString())
                .addFormDataPart("kecamatan", etKecamatan.getText().toString())
                .addFormDataPart("kelurahan", etKelurahan.getText().toString())
                .addFormDataPart("lat", String.valueOf(lat))
                .addFormDataPart("lang", String.valueOf(lang))
                .addFormDataPart("rt", mTxtRt.getText().toString())
                .addFormDataPart("rw", mTxtRw.getText().toString())
                .addFormDataPart("jaminan", getJaminan());

        if (anggotaList != null) {
            if (!anggotaList.isEmpty()) {
                body.addFormDataPart("anggota", getListAnggota());
            }
        }

        new Service().getApi(JKeluargaActivity.this).updateJKeluarga(body.build())
                .enqueue(new Callback<DTOList<KriteriaRes.DatasBean>>() {
                    @Override
                    public void onResponse(Call<DTOList<KriteriaRes.DatasBean>> call, Response<DTOList<KriteriaRes.DatasBean>> response) {
                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body().getErrorCode().equals("00")) {
                                if (response.body().getData().size() > 0) {
                                    //Jika data ditemukan EDITKRITERIA
                                    Intent i = new Intent(JKeluargaActivity.this, KriteriaEditActivity.class);
                                    i.putParcelableArrayListExtra("response", (ArrayList<? extends Parcelable>) response.body().getData());
                                    startActivity(i);
                                    finish();
                                    ToastUtil.toastOnUi(JKeluargaActivity.this, "Cek KK berhasil dilakukan");
                                } else {
                                    //SIMPAN KRITERIA BARU
                                    startActivity(new Intent(JKeluargaActivity.this, KriteriaNewActivity.class));
                                    finish();
                                    ToastUtil.toastOnUi(JKeluargaActivity.this, "Cek KK berhasil dilakukan");
                                }
                            } else {
                                ToastUtil.toastOnUi(JKeluargaActivity.this, response.body().getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DTOList<KriteriaRes.DatasBean>> call, Throwable t) {
                        progressDialog.dismiss();
                        ToastUtil.toastOnUi(JKeluargaActivity.this, getResources().getString(R.string.error_message1));
                    }
                });
    }

    private void simpan() {
        if (this.listPath.size() < 1) {
            ToastUtil.toastShow(JKeluargaActivity.this, "Silahkan masukan foto keluarga");
            return;
        }
        if (lat == 0 || lang == 0) {
            ToastUtil.toastShow(JKeluargaActivity.this, "Silahkan masukan lokasi keluarga ini");
            return;
        }
        if (mTxtRt.getText().toString().equals("")) {
            mTxtRt.setError("Silahkan diisi");
            return;
        }
        if (mTxtRw.getText().toString().equals("")) {
            mTxtRw.setError("Silahkan diisi");
            return;
        }
        progressDialog.show();
        MultipartBody.Builder body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("kk", etNik.getText().toString())
                .addFormDataPart("alamat", etAlamat.getText().toString())
                .addFormDataPart("kecamatan", etKecamatan.getText().toString())
                .addFormDataPart("kelurahan", etKelurahan.getText().toString())
                .addFormDataPart("lat", String.valueOf(lat))
                .addFormDataPart("lang", String.valueOf(lang))
                .addFormDataPart("rt", mTxtRt.getText().toString())
                .addFormDataPart("rw", mTxtRw.getText().toString())
                .addFormDataPart("jaminan", getJaminan());

        if (anggotaList != null) {
            if (!anggotaList.isEmpty()) {
                body.addFormDataPart("anggota", getListAnggota());
            }
        }

        for (int x = 0; x < listPath.size(); x++) {
            File file = new File(listPath.get(x));
            if (file.exists()) {
                body.addFormDataPart("foto", file.getName(), RequestBody.create(MediaType.parse("image/jpeg"), file));
            } else {
                ToastUtil.toastShow(JKeluargaActivity.this, "Error : Foto tidak ditemukan");
                return;
            }
        }

        RequestBody bodys = body.build();

        new Service().getApi(JKeluargaActivity.this).saveJKeluarga(bodys).enqueue(new Callback<DTO>() {
            @Override
            public void onResponse(Call<DTO> call, Response<DTO> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getErrorCode().equals("00")) {
                        startActivity(new Intent(JKeluargaActivity.this, KriteriaNewActivity.class));
                    } else {
                        ToastUtil.toastOnUi(JKeluargaActivity.this, response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DTO> call, Throwable t) {
                progressDialog.dismiss();
                ToastUtil.toastOnUi(JKeluargaActivity.this, getResources().getString(R.string.error_message1));
            }
        });
    }

    private String getJaminan() {
        String result = "";
        for (int x = 0; x < tblJaminan.getChildCount(); x++) {
            String status = null;
            String nama = null;
            ViewGroup row = (ViewGroup) tblJaminan.getChildAt(x);
            for (int i = 0; i < row.getChildCount(); i++) {
                TextView txt = (TextView) row.getChildAt(0);
                nama = txt.getText().toString();
                AppCompatCheckBox chk = (AppCompatCheckBox) row.getChildAt(1);
                if (chk.isChecked()) {
                    status = "0";
                }
            }
            result += nama + "|" + status;
            if (x < tblJaminan.getChildCount() - 1) {
                result += ";";
            }
        }
        Timber.i(result);
        return result;
    }

    @OnClick(R.id.btn_updatelokasi)
    void updateLokasi() {
        Intent intent = new Intent(JKeluargaActivity.this, MapsActivity.class);
        if (lat != 0.0 && lang != 0.0) {
            intent.putExtra("lat", String.valueOf(lat));
            intent.putExtra("lon", String.valueOf(lang));
        }
        startActivityForResult(intent, REQUEST_MAP);
    }

    @OnClick(R.id.btn_tambahfoto)
    void tambahFoto() {
        final CharSequence[] items = {"Ambil Foto", "Cari dari Galery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(JKeluargaActivity.this);
        builder.setTitle("Masukan Foto");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Ambil Foto")) {
                    int permissionCheck = ContextCompat.checkSelfPermission(JKeluargaActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    final int cameraCheck = ContextCompat.checkSelfPermission(JKeluargaActivity.this,
                            Manifest.permission.CAMERA);
                    if (permissionCheck == PackageManager.PERMISSION_GRANTED && cameraCheck == PackageManager.PERMISSION_GRANTED) {
                        cameraIntent();
                    } else {
                        Nammu.askForPermission(JKeluargaActivity.this,
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, new PermissionCallback() {
                                    @Override
                                    public void permissionGranted() {
                                        cameraIntent();
                                    }

                                    @Override
                                    public void permissionRefused() {
                                        ToastUtil.toastShow(JKeluargaActivity.this, "Tidak dapat mengambil foto");
                                    }
                                });
                    }
                } else if (items[item].equals("Cari dari Galery")) {
                    int permissionCheck = ContextCompat.checkSelfPermission(JKeluargaActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                        galleryIntent();
                    } else {
                        Nammu.askForPermission(JKeluargaActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE, new PermissionCallback() {
                                    @Override
                                    public void permissionGranted() {
                                        galleryIntent();
                                    }

                                    @Override
                                    public void permissionRefused() {
                                        ToastUtil.toastShow(JKeluargaActivity.this, "Tidak dapat membuka galery");
                                    }
                                });
                    }
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                String path = fileUri.getPath();
                path = imgCompress.compressImage(JKeluargaActivity.this, path);
                filePath = path;
                BitmapFactory.decodeFile(path);
                if (IS_EDIT) {
                    uploadFoto();
                } else {
                    listPath.add(filePath);
                    listFotoAdapter.notifyDataSetChanged();
                }

            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                fileUri = selectedImageUri;
                String selectedImagePath = ImgUri.selectedImagePath(JKeluargaActivity.this, fileUri);
                if (null != selectedImagePath) {
                    if (selectedImageUri.toString().contains("images")) {
                        //handle image
                        selectedImagePath = imgCompress.compressImage(JKeluargaActivity.this,
                                selectedImagePath);
                        filePath = selectedImagePath;
                        BitmapFactory.decodeFile(selectedImagePath);
                        if (IS_EDIT) {
                            uploadFoto();
                        } else {
                            listPath.add(filePath);
                            Timber.e("%s", filePath);
                            listFotoAdapter.notifyDataSetChanged();
                        }
                    } else {
                        ToastUtil.toastShow(JKeluargaActivity.this, "Mohon pilih salah satu foto");
                    }
                } else {
                    Toast.makeText(JKeluargaActivity.this, "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }
            } else if (requestCode == REQUEST_MAP) {
                lat = Double.parseDouble(data.getStringExtra("lat"));
                lang = Double.parseDouble(data.getStringExtra("lang"));

                Task.callInBackground(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return findJalan.pengguna(lat, lang);
                    }
                }).onSuccess(new Continuation<String, String>() {
                    @Override
                    public String then(Task<String> task) throws Exception {
                        Timber.e(task.getResult());
                        etLokasi.setText(task.getResult());
                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR);
            }
        }
    }

    void uploadFoto() {
        if (this.filePath == null) {
            ToastUtil.toastShow(JKeluargaActivity.this, "Maaf terjadi kesalahan");
            return;
        }
        File f = new File(this.filePath);
        if (f.exists()) {
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("foto", f.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), f))
                    .build();

            new Service().getApi(JKeluargaActivity.this)
                    .tambahFoto(body, etNik.getText().toString()).enqueue(new Callback<DTOList<FotoKeluarga>>() {
                @Override
                public void onResponse(Call<DTOList<FotoKeluarga>> call, Response<DTOList<FotoKeluarga>> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getErrorCode().equals("00")) {
                            listPath = new ArrayList<String>();
                            for (int x = 0; x < response.body().getData().size(); x++) {
                                listPath.add(response.body().getData().get(x).getFoto());
                            }
                            listFotoAdapter.setData(listPath);
                        } else {
                            ToastUtil.toastOnUi(JKeluargaActivity.this, response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<DTOList<FotoKeluarga>> call, Throwable t) {
                    ToastUtil.toastOnUi(JKeluargaActivity.this, getResources().getString(R.string.error_message1));
                }
            });
        }
    }

    @OnClick(R.id.btn_cek)
    void cekKK() {
        if (etNik.getText().toString().trim().length() < 15) {
            etNik.setError("NO KK / NIK tidak sesuai");
            return;
        }
        progressDialog.show();
        new Service().getApi(JKeluargaActivity.this)
                .cariJKeluarga(etNik.getText().toString()).enqueue(new Callback<DTO<CariKeluarga>>() {
            @Override
            public void onResponse(Call<DTO<CariKeluarga>> call, Response<DTO<CariKeluarga>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getErrorCode().equals("00")) {
                        PrefUtil.setStringPref(JKeluargaActivity.this, Constant.PREF_KK, etNik.getText().toString());
                        if (response.body().getMessage() == null) {
                            //Jika kk belum terdaftar
                            enableAll();
                            anggotaList = response.body().getData().getAnggotaKeluarga();
                            listViewAdapter.setData(anggotaList);
                            etAlamat.setText(response.body().getData().getAlamat());
                            etKecamatan.setText(response.body().getData().getKecamatan());
                            etKelurahan.setText(response.body().getData().getKelurahan());
                            if (response.body().getData().getNoRt() == null) {
                                mTxtRt.setText("");
                                mTxtRt.setEnabled(true);
                                mTxtRt.setInputType(InputType.TYPE_CLASS_NUMBER);
                            } else {
                                mTxtRt.setText(response.body().getData().getNoRt());
                                mTxtRt.setEnabled(false);
                            }
                            if (response.body().getData().getNoRw() == null) {
                                mTxtRw.setText("");
                                mTxtRw.setEnabled(true);
                                mTxtRw.setInputType(InputType.TYPE_CLASS_NUMBER);
                            } else {
                                mTxtRw.setText(response.body().getData().getNoRw());
                                mTxtRw.setEnabled(false);
                            }
                        } else {
                            //Jika kk sudah terdaftar
                            showDialogEdit(response.body().getMessage(), response.body().getData());
                        }
                    } else if (response.body().getErrorCode().equals("20")) {
                        PrefUtil.setStringPref(JKeluargaActivity.this, Constant.isLogedIn, "0");
                        Intent intent = new Intent(JKeluargaActivity.this, Login.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        ToastUtil.toastOnUi(JKeluargaActivity.this, response.body().getMessage());
                    } else {
                        ToastUtil.toastOnUi(JKeluargaActivity.this, response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DTO<CariKeluarga>> call, Throwable t) {
                progressDialog.dismiss();
                ToastUtil.toastOnUi(JKeluargaActivity.this, getResources().getString(R.string.error_message1));
                Timber.e("%s", t.getCause());
            }
        });
    }

    private void enableAll() {
        etNik.setEnabled(false);
        btnCek.setEnabled(false);
        btnSimpan.setEnabled(true);
        btnTambahfoto.setEnabled(true);
        btnUpdatelokasi.setEnabled(true);
    }

    private void showDialogEdit(String pesan, @Nullable final CariKeluarga data) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(pesan + ", lanjutkan ?");
        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                enableAll();
                if (data != null) {
                    pgBar.setVisibility(View.VISIBLE);
                    listPath = new ArrayList<>();
                    IS_EDIT = true;
                    initialTblJaminan(data.getjKeluarga());
                    new isiViews().execute(data);
                }
            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public class isiViews extends AsyncTask<CariKeluarga, String, CariKeluarga> {

        @Override
        protected CariKeluarga doInBackground(CariKeluarga... data) {
            fotoList = data[0].getFotoKeluarga();
            anggotaList = data[0].getAnggotaKeluarga();
            for (int f = 0; f < fotoList.size(); f++) {
                listPath.add(fotoList.get(f).getFoto());
                if (listPath.size() == 6) {
                    break;
                }
            }
            if (data[0].getLon() == null || data[0].getLat() == null) {
                try {
                    lat = gps.getLatitude();
                    lang = gps.getLongitude();
                } catch (Exception e) {
                    Timber.e("%s", e.fillInStackTrace());
                    ToastUtil.toastOnUi(JKeluargaActivity.this, "Error getting location");
                }
            } else {
                lat = Double.parseDouble(data[0].getLat());
                lang = Double.parseDouble(data[0].getLon());
            }

            return data[0];
        }

        @Override
        protected void onPostExecute(CariKeluarga data) {
            super.onPostExecute(data);
            etNik.setText(data.getNoKk());
            etAlamat.setText(data.getAlamat());
            etKelurahan.setText(data.getKelurahan());
            etKecamatan.setText(data.getKecamatan());
            initialTblJaminan(data.getjKeluarga());
            listViewAdapter.setData(anggotaList);

            listFotoAdapter = new GridFotoAdapter(listPath, JKeluargaActivity.this, false, 0);
            recycleFoto.setAdapter(listFotoAdapter);
            listFotoAdapter.setOnImageClick(JKeluargaActivity.this);

            if (lat != 0 || lang != 0) {
                namaJalan = findJalan.pengguna(lat, lang);
            }
            etLokasi.setText(namaJalan);
            if (anggotaList.isEmpty()) {
                txtKeluargaNull.setVisibility(View.VISIBLE);
                listAnggota.setVisibility(View.GONE);
            }

            if (pgBar.getVisibility() == View.VISIBLE) {
                pgBar.setVisibility(View.GONE);
            }
            if (data.getNoRt() == null) {
                mTxtRt.setText("");
                mTxtRt.setEnabled(true);
            } else {
                if (data.getNoRt().trim().equals("")) {
                    mTxtRt.setText("");
                    mTxtRt.setEnabled(true);
                    mTxtRt.setInputType(InputType.TYPE_CLASS_NUMBER);
                } else {
                    mTxtRt.setText(data.getNoRt());
                    mTxtRt.setEnabled(false);
                }
            }
            if (data.getNoRw() == null) {
                mTxtRw.setText("");
                mTxtRw.setEnabled(true);
            } else {
                if (data.getNoRw().trim().equals("")) {
                    mTxtRw.setText("");
                    mTxtRw.setEnabled(true);
                    mTxtRw.setInputType(InputType.TYPE_CLASS_NUMBER);
                } else {
                    mTxtRw.setText(data.getNoRw());
                    mTxtRw.setEnabled(false);
                }
            }
        }
    }

    private void galleryIntent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, SELECT_FILE);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(intent, SELECT_FILE);
        }
    }

    private void cameraIntent() {
        if (JKeluargaActivity.this.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = ImgUri.getOutputMediaFileUri();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            // no camera on this device
            ToastUtil.toastShow(JKeluargaActivity.this, "Sorry! Your device doesn't support camera");
        }
    }

    private void initialTblJaminan(@Nullable List<JKeluarga> jp) {
        ColorStateList colorStateList = new ColorStateList(
                new int[][]{new int[]{android.R.attr.state_enabled}},
                new int[]{ContextCompat.getColor(JKeluargaActivity.this, R.color.colorAccent)}
        );
        tblJaminan.removeAllViews();
        for (int x = 0; x < datas.size(); x++) {
            TableRow row = new TableRow(this);
            row.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            TableRow.LayoutParams tx = new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            TextView jaminan = new TextView(this);
            jaminan.setText(datas.get(x).toString());
            jaminan.setPadding(5, 5, 5, 5);
            jaminan.setLayoutParams(tx);

            AppCompatCheckBox chk = new AppCompatCheckBox(this);
            chk.setChecked(false);
            chk.setSupportButtonTintList(colorStateList);

            if (jp != null) {
                for (int i = 0; i < jp.size(); i++) {
                    if (jp.get(i).getNama().equals(datas.get(x).toString())) { //&& jp.get(i).getStatus() != null) {
                        chk.setChecked(true);
                    }
                }
            }
            row.addView(jaminan);
            row.addView(chk);
            tblJaminan.addView(row);
        }
    }

    @Override
    public void onDeleteImageListener(int position, int x) {
        if (!IS_EDIT) {
            listPath.remove(position);
            listFotoAdapter.notifyDataSetChanged();
        } else {
            ToastUtil.toastShow(JKeluargaActivity.this, "Gunakan fasilitas web untuk menghapus foto");
        }
    }

    @Override
    public void onImageClickListener(int position, int v) {
        if (listPath.get(position) == null) {
            ToastUtil.toastShow(JKeluargaActivity.this, "Gambar tidak dapat di tampilkan");
            return;
        }

        ImgZoom imgZoom = new ImgZoom(JKeluargaActivity.this, listPath.get(position));
        imgZoom.setCanceledOnTouchOutside(true);
        imgZoom.show();
    }

    private String getListAnggota() {
        String anggpta = "";
        for (int x = 0; x < anggotaList.size(); x++) {
            String nik = anggotaList.get(x).getNik();
            String nama = anggotaList.get(x).getNama();
            anggpta += nik + ";" + nama;
            if (x < anggotaList.size() - 1) {
                anggpta += "::";
            }
        }
        Timber.i(anggpta);
        return anggpta;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
