package gov.tangerang.siap.datamiskin.util;

import android.app.Activity;
import android.widget.Toast;

/**
 * Created by tri on 7/27/16.
 */
public class ToastUtil {

    public static void toastOnUi(final Activity activity,final String text){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity,text,Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void toastOnUi(final Activity activity,final int textId){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity,activity.getResources().getText(textId),Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void toastShow(Activity activity, String text){
        Toast.makeText(activity,text, Toast.LENGTH_SHORT).show();
    }

}
