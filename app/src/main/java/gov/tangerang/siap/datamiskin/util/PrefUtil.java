package gov.tangerang.siap.datamiskin.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashSet;

import timber.log.Timber;

/**
 * Created by tri on 7/27/16.
 */
public class PrefUtil {
    public static void setStringPref(Context mContext, String key, String value) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(key, value);
        edit.apply();
    }

    public static void setMapPref(Context mContext, String key, HashSet value) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor edit = pref.edit();
        edit.putStringSet(key, value);
        edit.apply();
    }

    public static String getStringPref(Context mContext, String key, String value) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        try {
            return pref.getString(key, value);
        } catch (Exception e) {
            Timber.e("Error getting sharedPreference");
            return value;
        }
    }

    public static HashSet<String> getMapPref(Context mContext, String key) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
        try {
            return (HashSet<String>) pref.getStringSet(key, null);
        } catch (Exception e) {
            Timber.e("Error getting sharedPreference");
            return null;
        }
    }
}