package gov.tangerang.siap.datamiskin.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.adapter.GridFotoAdapter;
import gov.tangerang.siap.datamiskin.model.DTO;
import gov.tangerang.siap.datamiskin.model.DTOList;
import gov.tangerang.siap.datamiskin.model.KriteriaReq;
import gov.tangerang.siap.datamiskin.model.response.KriteriaRes;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.ui.dialog.ImgZoom;
import gov.tangerang.siap.datamiskin.util.Constant;
import gov.tangerang.siap.datamiskin.util.ExpandableHeightGridView;
import gov.tangerang.siap.datamiskin.util.ImgCompress;
import gov.tangerang.siap.datamiskin.util.ImgUri;
import gov.tangerang.siap.datamiskin.util.MultiSpinner;
import gov.tangerang.siap.datamiskin.util.MyProgressDialog;
import gov.tangerang.siap.datamiskin.util.NumberFormat;
import gov.tangerang.siap.datamiskin.util.PrefUtil;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tri on 8/4/16.
 */
public class KriteriaEditActivity extends AppCompatActivity implements MultiSpinner.MultiSpinnerListener,
        RadioGroup.OnCheckedChangeListener, GridFotoAdapter.OnImageClick {

    @Bind(R.id.rg_penghasilan_tetap_ya)
    RadioButton mRgPenghasilanTetapYa;
    @Bind(R.id.rg_penghasilan_tetap_tidak)
    RadioButton mRgPenghasilanTetapTidak;
    @Bind(R.id.rg_penghasilan_tetap)
    RadioGroup mRgPenghasilanTetap;
    @Bind(R.id.rg_penghasilan_cukup_ya)
    RadioButton mRgPenghasilanCukupYa;
    @Bind(R.id.rg_penghasilan_cukup_tidak)
    RadioButton mRgPenghasilanCukupTidak;
    @Bind(R.id.rg_penghasilan_cukup)
    RadioGroup mRgPenghasilanCukup;
    @Bind(R.id.tx_penghasilan_rata)
    EditText mTxPenghasilanRata;
    @Bind(R.id.penghasilan)
    LinearLayout mPenghasilan;
    @Bind(R.id.tx_pengeluaran_sebulan)
    EditText mTxPengeluaranSebulan;
    @Bind(R.id.multi_mkn_pokok)
    MultiSpinner mMultiMknPokok;
    @Bind(R.id.multi_mkn_lauk)
    MultiSpinner mMultiMknLauk;
    @Bind(R.id.multi_mkn_sayur)
    MultiSpinner mMultiMknSayur;
    @Bind(R.id.multi_mkn_buah)
    MultiSpinner mMultiMknBuah;
    @Bind(R.id.pengeluaran)
    LinearLayout mPengeluaran;
    @Bind(R.id.spn_kesehatan1)
    Spinner mSpnKesehatan1;
    @Bind(R.id.mlt_kesehatan2)
    MultiSpinner mMltKesehatan2;
    @Bind(R.id.kesehatan)
    LinearLayout mKesehatan;
    @Bind(R.id.rb_pakaian_ya)
    RadioButton mRbPakaianYa;
    @Bind(R.id.rb_pakaian_tidak)
    RadioButton mRbPakaianTidak;
    @Bind(R.id.rg_pakaian)
    RadioGroup mRgPakaian;
    @Bind(R.id.tx_pakaian)
    EditText mTxPakaian;
    @Bind(R.id.container_pakaian)
    LinearLayout mContainerPakaian;
    @Bind(R.id.pakaian)
    LinearLayout mPakaian;
    @Bind(R.id.spn_pendidikan_maks)
    Spinner mSpnPendidikanMaks;
    @Bind(R.id.spn_pendidikan_dana)
    Spinner mSpnPendidikanDana;
    @Bind(R.id.container_pendidikan)
    LinearLayout mContainerPendidikan;
    @Bind(R.id.pendidikan)
    LinearLayout mPendidikan;
    @Bind(R.id.rb_rumah1_ya)
    RadioButton mRbRumah1Ya;
    @Bind(R.id.rb_rumah1_tidak)
    RadioButton mRbRumah1Tidak;
    @Bind(R.id.rg_rumah1)
    RadioGroup mRgRumah1;
    @Bind(R.id.spn_dinding_rumah)
    Spinner mSpnDindingRumah;
    @Bind(R.id.spn_kondisi_dinding)
    Spinner mSpnKondisiDinding;
    @Bind(R.id.container_foto_dinding)
    ExpandableHeightGridView mContainerFotoDinding;
    @Bind(R.id.btn_foto_dinding)
    Button mBtnFotoDinding;
    @Bind(R.id.spn_lantai_rumah)
    Spinner mSpnLantaiRumah;
    @Bind(R.id.spn_kondisi_lantai)
    Spinner mSpnKondisiLantai;
    @Bind(R.id.container_foto_lantai)
    ExpandableHeightGridView mContainerFotoLantai;
    @Bind(R.id.btn_foto_lantai)
    Button mBtnFotoLantai;
    @Bind(R.id.et_lantai_rumah)
    EditText mEtLantaiRumah;
    @Bind(R.id.et_jumlah_keluarga)
    EditText mEtJumlahKeluarga;
    @Bind(R.id.spn_atap_rumah)
    Spinner mSpnAtapRumah;
    @Bind(R.id.spn_kondisi_atap)
    Spinner mSpnKondisiAtap;
    @Bind(R.id.container_foto_atap)
    ExpandableHeightGridView mContainerFotoAtap;
    @Bind(R.id.btn_foto_atap)
    Button mBtnFotoAtap;
    @Bind(R.id.spn_mck)
    Spinner mSpnMck;
    @Bind(R.id.container_foto_mck)
    ExpandableHeightGridView mContainerFotoMck;
    @Bind(R.id.btn_foto_mck)
    Button mBtnFotoMck;
    @Bind(R.id.spn_penerangan)
    Spinner mSpnPenerangan;
    @Bind(R.id.spn_daya)
    Spinner mSpnDaya;
    @Bind(R.id.container_daya)
    LinearLayout mContainerDaya;
    @Bind(R.id.container_rumah_ya)
    LinearLayout mContainerRumahYa;
    @Bind(R.id.rumah)
    LinearLayout mRumah;
    @Bind(R.id.spn_air_minum)
    Spinner mSpnAirMinum;
    @Bind(R.id.air_minum)
    LinearLayout mAirMinum;
    @Bind(R.id.btn_simpan)
    Button mBtnSimpan;
    @Bind(R.id.rb_susu_ya)
    RadioButton mRbSusuYa;
    @Bind(R.id.rb_susu_tidak)
    RadioButton mRbSusuTidak;
    @Bind(R.id.rg_susu)
    RadioGroup mRgSusu;

    private String[] pokok;
    private String[] lauk;
    private String[] sayur;
    private String[] buah;
    private String[] berobat;
    private String[] penyakit;
    private String[] pendidikan;
    private String[] biayaPendidikan;
    private String[] dinding;
    private String[] kondisiFull;
    private String[] kondisi;
    private String[] lantai;
    private String[] lampu;
    private String[] atap;
    private String[] mck;
    private String[] daya;
    private String[] air;

    private ArrayAdapter<String> arrPenyakit;
    private ArrayAdapter<String> arrBerobat;
    private ArrayAdapter<String> arrBuah;
    private ArrayAdapter<String> arrPokok;
    private ArrayAdapter<String> arrLauk;
    private ArrayAdapter<String> arrSayur;
    private ArrayAdapter<String> arrPendidikan;
    private ArrayAdapter<String> arrBiayaPendidikan;
    private ArrayAdapter<String> arrDinding;
    private ArrayAdapter<String> arrKondisiFull;
    private ArrayAdapter<String> arrKondisi;
    private ArrayAdapter<String> arrLantai;
    private ArrayAdapter<String> arrLampu;
    private ArrayAdapter<String> arrAtap;
    private ArrayAdapter<String> arrMck;
    private ArrayAdapter<String> arrDaya;
    private ArrayAdapter<String> arrAir;

    private GridFotoAdapter mAdapterDinding;
    private GridFotoAdapter mAdapterLantai;
    private GridFotoAdapter mAdapterMck;
    private GridFotoAdapter mAdapterAtap;

    private List<String> mListFotoDinding = new ArrayList<>();
    private List<String> mListFotoMck = new ArrayList<>();
    private List<String> mListFotoLantai = new ArrayList<>();
    private List<String> mListFotoAtap = new ArrayList<>();

    private int REQUEST_FILE_DINDING = 201;
    private int REQUEST_FILE_LANTAI = 301;
    private int REQUEST_FILE_MCK = 401;
    private int REQUEST_CAMERA_DINDING = 200;
    private int REQUEST_CAMERA_LANTAI = 300;
    private int REQUEST_CAMERA_MCK = 400;
    private int REQUEST_CAMERA_ATAP = 500;
    private int REQUEST_FILE_ATAP = 501;

    private ImgCompress mImgCompress;
    private Uri fileUri; // file url to store image
    private String mfilePath;
    private MyProgressDialog mDialog;
    private List<KriteriaRes.DatasBean> mKriteriaResList = new ArrayList<>();
    private Bundle b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kriteria_base);
        ButterKnife.bind(this);

        mTxPengeluaranSebulan.addTextChangedListener(new NumberFormat(mTxPengeluaranSebulan));
        mTxPenghasilanRata.addTextChangedListener(new NumberFormat(mTxPenghasilanRata));

        b = this.getIntent().getExtras();
        initialView();
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }*/

    private void initialView() {
        initializeAdapter();
        initializeSpinner();
        try {
            Task.UI_THREAD_EXECUTOR.execute(new Runnable() {
                @Override
                public void run() {
                    if (!b.isEmpty()) {
                        mKriteriaResList = b.getParcelableArrayList("response");
                    } else {
                        ToastUtil.toastOnUi(getParent(), "Data tidak ditemukan");
                        finish();
                    }

                    for (int x = 0; x < mKriteriaResList.size(); x++) {
                        if (mKriteriaResList.get(x).getIdKriteria() == 2) {
                            if (mKriteriaResList.get(x).getKeterangan().equals("1")) {
                                mRgPenghasilanCukupYa.setChecked(true);
                            } else {
                                mRgPenghasilanCukupTidak.setChecked(true);
                            }
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 3) {
                            if (mKriteriaResList.get(x).getKeterangan().equals("1")) {
                                mRgPenghasilanCukupYa.setChecked(true);
                            } else {
                                mRgPenghasilanCukupTidak.setChecked(true);
                            }
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 4) {
                            mTxPenghasilanRata.setText(mKriteriaResList.get(x).getKeterangan());
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 6) {
                            mTxPengeluaranSebulan.setText(mKriteriaResList.get(x).getKeterangan());
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 9) {
                            mSpnKesehatan1.setSelection(arrBerobat
                                    .getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 10) {
                            mMltKesehatan2.setText(mKriteriaResList.get(x).getKeterangan());
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 12) {
                            if (mKriteriaResList.get(x).getKeterangan().equals("1")) {
                                mRbPakaianYa.setChecked(true);
                                mContainerPakaian.setVisibility(View.VISIBLE);
                            } else {
                                mRbPakaianTidak.setChecked(true);
                                mContainerPakaian.setVisibility(View.GONE);
                            }
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 13) {
                            mTxPakaian.setText(mKriteriaResList.get(x).getKeterangan());
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 15) {
                            mSpnPendidikanMaks.setSelection(arrPendidikan
                                    .getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 16) {
                            mSpnPendidikanDana.setSelection(arrBiayaPendidikan
                                    .getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 18) {
                            if (mKriteriaResList.get(x).getKeterangan().equals("1")) {
                                mRbRumah1Ya.setChecked(true);
                            } else {
                                mRbRumah1Tidak.setChecked(true);
                                mContainerRumahYa.setVisibility(View.GONE);
                            }
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 19) {
                            mSpnDindingRumah.setSelection(arrDinding
                                    .getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 20) {
                            mSpnKondisiDinding.setSelection(arrKondisiFull
                                    .getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 22) {
                            mSpnLantaiRumah.setSelection(arrLantai
                                    .getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 23) {
                            mSpnKondisiLantai.setSelection(arrKondisi
                                    .getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 24) {
                            mEtLantaiRumah.setText(mKriteriaResList.get(x).getKeterangan());
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 25) {
                            mEtJumlahKeluarga.setText(mKriteriaResList.get(x).getKeterangan());
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 27) {
                            mSpnAtapRumah.setSelection(arrAtap.getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 28) {
                            mSpnKondisiAtap.setSelection(arrKondisi.getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 30) {
                            mSpnMck.setSelection(arrMck.getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 32) {
                            mSpnPenerangan.setSelection(arrLampu.getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 33) {
                            mSpnDaya.setSelection(arrDaya.getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        if (mKriteriaResList.get(x).getIdKriteria() == 35) {
                            mSpnAirMinum.setSelection(arrAir.getPosition(mKriteriaResList.get(x).getKeterangan()));
                        }
                        //makanan
                        if (mKriteriaResList.get(x).getIdKriteria() == 7) {
                            String makanan = mKriteriaResList.get(x).getKeterangan();
                            mMultiMknPokok.setText(makanan.split(";")[0].split(":")[1]);
                            mMultiMknLauk.setText(makanan.split(";")[1].split(":")[1]);
                            mMultiMknSayur.setText(makanan.split(";")[2].split(":")[1]);
                            mMultiMknBuah.setText(makanan.split(";")[3].split(":")[1]);
                            String susu = makanan.split(";")[4].split(":")[1];
                            if (susu.equals("1")) {
                                mRbSusuYa.setChecked(true);
                            } else {
                                mRbSusuTidak.setChecked(true);
                            }
                        }
                        //fotoDinding
                        if (mKriteriaResList.get(x).getIdKriteria() == 21) {
                            String foto = mKriteriaResList.get(x).getKeterangan();
                            for (int f = 0; f < foto.split("::").length; f++) {
                                mListFotoDinding.add(foto.split("::")[f]);
                            }
                            checkMaksFoto(mListFotoDinding, mBtnFotoDinding);
                            mAdapterDinding.notifyDataSetChanged();
                        }

                        if (mKriteriaResList.get(x).getIdKriteria() == 26) {
                            String foto = mKriteriaResList.get(x).getKeterangan();
                            for (int f = 0; f < foto.split("::").length; f++) {
                                mListFotoLantai.add(foto.split("::")[f]);
                            }
                            checkMaksFoto(mListFotoLantai, mBtnFotoLantai);
                            mAdapterLantai.notifyDataSetChanged();
                        }

                        if (mKriteriaResList.get(x).getIdKriteria() == 29) {
                            String foto = mKriteriaResList.get(x).getKeterangan();
                            for (int f = 0; f < foto.split("::").length; f++) {
                                mListFotoAtap.add(foto.split("::")[f]);
                            }
                            checkMaksFoto(mListFotoAtap, mBtnFotoAtap);
                            mAdapterAtap.notifyDataSetChanged();
                        }

                        if (mKriteriaResList.get(x).getIdKriteria() == 31) {
                            String foto = mKriteriaResList.get(x).getKeterangan();
                            for (int f = 0; f < foto.split("::").length; f++) {
                                mListFotoMck.add(foto.split("::")[f]);
                            }
                            checkMaksFoto(mListFotoMck, mBtnFotoMck);
                            mAdapterMck.notifyDataSetChanged();
                        }
                    }
                }
            });
        } catch (Exception e) {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Terjadi kesalahan");
            finish();
        }

        mAdapterDinding.setOnImageClick(this);
        mAdapterMck.setOnImageClick(this);
        mAdapterLantai.setOnImageClick(this);
        mAdapterAtap.setOnImageClick(this);

        mImgCompress = new ImgCompress();
        mDialog = new MyProgressDialog(this, "Mohon tunggu..", false, true, "");
    }

    @OnClick(R.id.btn_simpan)
    void doSave() {
        if (mTxPenghasilanRata.getText().toString().equals("")) {
            mTxPenghasilanRata.setError("Silahkan diisi terlebih dahulu");
            return;
        }
        if (mTxPengeluaranSebulan.getText().toString().equals("")) {
            mTxPengeluaranSebulan.setError("Silahkan diisi terlebih dahulu");
            return;
        }

        if (mMltKesehatan2.getText().toString().equals("")) {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Silahkan pilih minimal satu penyakit");
            return;
        }
        if (mMultiMknPokok.getText().toString().equals("") || mMultiMknLauk.getText().toString().equals("")) {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Silahkan lengkapi makanan sehari-hari untuk keluarga ini");
            return;
        }
        if (mMultiMknSayur.getText().toString().equals("") || mMultiMknBuah.getText().toString().equals("")) {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Silahkan lengkapi makanan sehari-hari untuk keluarga ini");
            return;
        }

        int penghasilanTetap = selectedRadio(mRgPenghasilanTetap);
        int penghasilanCukup = selectedRadio(mRgPenghasilanCukup);
        String penghasilanBulan = mTxPenghasilanRata.getText().toString();

        String pengeluaranBulan = mTxPengeluaranSebulan.getText().toString();
        String pokok = mMultiMknPokok.getText().toString();
        String lauk = mMultiMknLauk.getText().toString();
        String sayur = mMultiMknSayur.getText().toString();
        String buah = mMultiMknBuah.getText().toString();
        int susu = selectedRadio(mRgSusu);

        String lokasiBerobat = mSpnKesehatan1.getSelectedItem().toString();
        String penyakit = mMltKesehatan2.getText().toString();

        int pakaianBaru = selectedRadio(mRgPakaian);
        String jumlahPakaian = mTxPakaian.getText().toString();

        String pendidikan1 = mSpnPendidikanMaks.getSelectedItem().toString();
        String pendidikan2 = mSpnPendidikanDana.getSelectedItem().toString();

        int punyaRumah = selectedRadio(mRgRumah1);
        String dinding = (String) mSpnDindingRumah.getSelectedItem();
        String kondisiDinding = mSpnKondisiDinding.getSelectedItem().toString();
        String lantai = mSpnLantaiRumah.getSelectedItem().toString();
        String kondisiLantai = mSpnKondisiLantai.getSelectedItem().toString();
        String luasLantai = mEtLantaiRumah.getText().toString();
        String anggotaKeluarga = mEtJumlahKeluarga.getText().toString();
        String atapRumah = mSpnAtapRumah.getSelectedItem().toString();
        String mck = mSpnMck.getSelectedItem().toString();
        String penerangan = mSpnPenerangan.getSelectedItem().toString();
        String daya = mSpnDaya.getSelectedItem().toString();
        String minum = mSpnAirMinum.getSelectedItem().toString();
        String kondisiAtap = mSpnKondisiAtap.getSelectedItem().toString();

        String makanan = "Pokok:" + pokok + ";Lauk:" + lauk + ";Sayur:" + sayur + ";Buah:" + buah + ";Susu:" + susu;
        String kk = PrefUtil.getStringPref(KriteriaEditActivity.this, Constant.PREF_KK, "");
        List<KriteriaReq> listReq = new ArrayList<>();
        listReq.add(createRequest(kk, 2, penghasilanTetap + "", "0"));
        listReq.add(createRequest(kk, 3, penghasilanCukup + "", "0"));
        listReq.add(createRequest(kk, 4, penghasilanBulan, "0"));
        listReq.add(createRequest(kk, 6, pengeluaranBulan, "0"));
        listReq.add(createRequest(kk, 7, makanan, "0"));
        listReq.add(createRequest(kk, 9, lokasiBerobat, "0"));
        listReq.add(createRequest(kk, 10, penyakit, "0"));
        listReq.add(createRequest(kk, 12, pakaianBaru + "", "0"));
        listReq.add(createRequest(kk, 15, pendidikan1, "0"));
        listReq.add(createRequest(kk, 18, punyaRumah + "", "0"));
        listReq.add(createRequest(kk, 35, minum, "0"));

        if (pakaianBaru == 1) {
            listReq.add(createRequest(kk, 13, jumlahPakaian, "0"));
        }
        if (mSpnPendidikanMaks.getSelectedItemPosition() != 0) {
            listReq.add(createRequest(kk, 16, pendidikan2, "0"));
        }
        if (punyaRumah == 1) {
            listReq.add(createRequest(kk, 19, dinding, "0"));
            listReq.add(createRequest(kk, 20, kondisiDinding, "0"));
            listReq.add(createRequest(kk, 22, lantai, "0"));
            listReq.add(createRequest(kk, 23, kondisiLantai, "0"));
            listReq.add(createRequest(kk, 24, luasLantai, "0"));
            listReq.add(createRequest(kk, 25, anggotaKeluarga, "0"));
            listReq.add(createRequest(kk, 27, atapRumah, "0"));
            listReq.add(createRequest(kk, 28, kondisiAtap, "0"));
            listReq.add(createRequest(kk, 30, mck, "0"));
            listReq.add(createRequest(kk, 32, penerangan, "0"));
        }
        if(penerangan.equals("Listrik")){
            listReq.add(createRequest(kk, 33, daya,"0"));
        }
        mDialog.show();
        new Service().getApi(KriteriaEditActivity.this)
                .updateKriteriaKeluarga(listReq).enqueue(new Callback<DTO>() {
            @Override
            public void onResponse(Call<DTO> call, Response<DTO> response) {
                mDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getErrorCode().equals("00")) {
                        Intent i = new Intent(KriteriaEditActivity.this, HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        ToastUtil.toastOnUi(KriteriaEditActivity.this, "Kriteria berhasil disimpan");
                        finish();
                    } else {
                        ToastUtil.toastOnUi(KriteriaEditActivity.this, response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DTO> call, Throwable t) {
                mDialog.dismiss();
                ToastUtil.toastOnUi(KriteriaEditActivity.this, getResources().getString(R.string.error_message1));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if ((requestCode == REQUEST_CAMERA_DINDING || requestCode == REQUEST_CAMERA_LANTAI)
                    || requestCode == REQUEST_CAMERA_MCK || requestCode == REQUEST_CAMERA_ATAP) {

                String path = fileUri.getPath();
                path = mImgCompress.compressImage(KriteriaEditActivity.this, path);
                mfilePath = path;
                BitmapFactory.decodeFile(path);

                if (requestCode == REQUEST_CAMERA_DINDING) {
                    updateFotoDinding();
                    checkMaksFoto(mListFotoDinding, mBtnFotoDinding);
                } else if (requestCode == REQUEST_CAMERA_LANTAI) {
                    updateFotoLantai();
                    checkMaksFoto(mListFotoLantai, mBtnFotoLantai);
                } else if (requestCode == REQUEST_CAMERA_MCK) {
                    updateFotoMck();
                    checkMaksFoto(mListFotoMck, mBtnFotoMck);
                } else {
                    updateFotoAtap();
                    checkMaksFoto(mListFotoAtap, mBtnFotoAtap);
                }

            } else if ((requestCode == REQUEST_FILE_DINDING || requestCode == REQUEST_FILE_LANTAI)
                    || requestCode == REQUEST_FILE_MCK || requestCode == REQUEST_FILE_ATAP) {

                Uri selectedImageUri = data.getData();
                fileUri = selectedImageUri;
                String selectedImagePath = ImgUri.selectedImagePath(KriteriaEditActivity.this, fileUri);
                if (null != selectedImagePath) {
                    if (selectedImageUri.toString().contains("images")) {
                        selectedImagePath = mImgCompress.compressImage(KriteriaEditActivity.this,
                                selectedImagePath);
                        mfilePath = selectedImagePath;
                        BitmapFactory.decodeFile(selectedImagePath);
                        if (requestCode == REQUEST_FILE_DINDING) {
                            updateFotoDinding();
                            checkMaksFoto(mListFotoDinding, mBtnFotoDinding);
                        } else if (requestCode == REQUEST_FILE_LANTAI) {
                            updateFotoLantai();
                            checkMaksFoto(mListFotoLantai, mBtnFotoLantai);
                        } else if (requestCode == REQUEST_FILE_MCK) {
                            updateFotoMck();
                            checkMaksFoto(mListFotoMck, mBtnFotoMck);
                        } else {
                            updateFotoAtap();
                            checkMaksFoto(mListFotoAtap, mBtnFotoAtap);
                        }
                    } else {
                        ToastUtil.toastShow(KriteriaEditActivity.this, "Mohon pilih salah satu foto");
                    }
                } else {
                    Toast.makeText(KriteriaEditActivity.this, "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    void updateFotoDinding() {
        if (this.mfilePath == null) {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Maaf terjadi kesalahan");
            return;
        }
        File f = new File(this.mfilePath);
        if (f.exists()) {
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("foto", f.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), f))
                    .addFormDataPart("kk", PrefUtil.getStringPref(KriteriaEditActivity.this, Constant.PREF_KK, ""))
                    .build();

            new Service().getApi(KriteriaEditActivity.this)
                    .updateFotoDinding(body).enqueue(new Callback<DTOList>() {
                @Override
                public void onResponse(Call<DTOList> call, Response<DTOList> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getErrorCode().equals("00")) {
                            mListFotoDinding = new ArrayList<String>();
                            for (int x = 0; x < response.body().getData().size(); x++) {
                                mListFotoDinding.add(response.body().getData().get(x).toString());
                            }
                            mAdapterDinding.setData(mListFotoDinding);
                        } else {
                            ToastUtil.toastOnUi(KriteriaEditActivity.this, response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<DTOList> call, Throwable t) {
                    ToastUtil.toastOnUi(KriteriaEditActivity.this, getResources().getString(R.string.error_message1));
                }
            });
        }
    }

    void updateFotoAtap() {
        if (this.mfilePath == null) {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Maaf terjadi kesalahan");
            return;
        }
        File f = new File(this.mfilePath);
        if (f.exists()) {
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("foto", f.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), f))
                    .addFormDataPart("kk", PrefUtil.getStringPref(KriteriaEditActivity.this, Constant.PREF_KK, ""))
                    .build();

            new Service().getApi(KriteriaEditActivity.this)
                    .updateFotoAtap(body).enqueue(new Callback<DTOList>() {
                @Override
                public void onResponse(Call<DTOList> call, Response<DTOList> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getErrorCode().equals("00")) {
                            mListFotoAtap = new ArrayList<String>();
                            for (int x = 0; x < response.body().getData().size(); x++) {
                                mListFotoAtap.add(response.body().getData().get(x).toString());
                            }
                            mAdapterAtap.setData(mListFotoAtap);
                        } else {
                            ToastUtil.toastOnUi(KriteriaEditActivity.this, response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<DTOList> call, Throwable t) {
                    ToastUtil.toastOnUi(KriteriaEditActivity.this, getResources().getString(R.string.error_message1));
                }
            });
        }
    }

    void updateFotoLantai() {
        if (this.mfilePath == null) {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Maaf terjadi kesalahan");
            return;
        }
        File f = new File(this.mfilePath);
        if (f.exists()) {
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("foto", f.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), f))
                    .addFormDataPart("kk", PrefUtil.getStringPref(KriteriaEditActivity.this, Constant.PREF_KK, ""))
                    .build();

            new Service().getApi(KriteriaEditActivity.this)
                    .updateFotoLantai(body).enqueue(new Callback<DTOList>() {
                @Override
                public void onResponse(Call<DTOList> call, Response<DTOList> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getErrorCode().equals("00")) {
                            mListFotoLantai = new ArrayList<String>();
                            for (int x = 0; x < response.body().getData().size(); x++) {
                                mListFotoLantai.add(response.body().getData().get(x).toString());
                            }
                            mAdapterLantai.setData(mListFotoLantai);
                        } else {
                            ToastUtil.toastOnUi(KriteriaEditActivity.this, response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<DTOList> call, Throwable t) {
                    ToastUtil.toastOnUi(KriteriaEditActivity.this, getResources().getString(R.string.error_message1));
                }
            });
        }
    }

    void updateFotoMck() {
        if (this.mfilePath == null) {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Maaf terjadi kesalahan");
            return;
        }
        File f = new File(this.mfilePath);
        if (f.exists()) {
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("foto", f.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), f))
                    .addFormDataPart("kk", PrefUtil.getStringPref(KriteriaEditActivity.this, Constant.PREF_KK, ""))
                    .build();

            new Service().getApi(KriteriaEditActivity.this)
                    .updateFotoMck(body).enqueue(new Callback<DTOList>() {
                @Override
                public void onResponse(Call<DTOList> call, Response<DTOList> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getErrorCode().equals("00")) {
                            mListFotoMck = new ArrayList<String>();
                            for (int x = 0; x < response.body().getData().size(); x++) {
                                mListFotoMck.add(response.body().getData().get(x).toString());
                            }
                            mAdapterMck.setData(mListFotoMck);
                        } else {
                            ToastUtil.toastOnUi(KriteriaEditActivity.this, response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<DTOList> call, Throwable t) {
                    ToastUtil.toastOnUi(KriteriaEditActivity.this, getResources().getString(R.string.error_message1));
                }
            });
        }
    }

    private void checkMaksFoto(List<String> list, Button b) {
        if (list.size() >= 3) {
            b.setEnabled(false);
        } else {
            b.setEnabled(true);
        }
    }

    void updateFotoAdapter(final GridFotoAdapter adapter) {
        KriteriaEditActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    private KriteriaReq createRequest(String kk, int idKriteria, String keterangan, String status) {
        KriteriaReq r = new KriteriaReq();
        r.setKk(kk);
        r.setIdKriteria(idKriteria);
        r.setKeterangan(keterangan);
        if (status == null) {
            r.setStatus("0");
        } else {
            r.setStatus(status);
        }
        return r;
    }

    private void initializeSpinner() {
        arrPokok = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, pokok);
        arrLauk = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, lauk);
        arrSayur = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, sayur);
        arrBuah = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, buah);
        arrBerobat = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, berobat);
        arrPenyakit = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, penyakit);
        arrPendidikan = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, pendidikan);
        arrBiayaPendidikan = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, biayaPendidikan);
        arrDinding = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, dinding);
        arrKondisi = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, kondisi);
        arrKondisiFull = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, kondisiFull);
        arrLantai = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, lantai);
        arrLampu = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, lampu);
        arrAtap = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, atap);
        arrMck = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, mck);
        arrAir = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, air);
        arrDaya = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, daya);

        mMultiMknPokok.setAdapter(arrPokok, false, this);
        mMultiMknLauk.setAdapter(arrLauk, false, this);
        mMultiMknSayur.setAdapter(arrSayur, false, this);
        mMultiMknBuah.setAdapter(arrBuah, false, this);
        mSpnKesehatan1.setAdapter(arrBerobat);
        mMltKesehatan2.setAdapter(arrPenyakit, false, this);
        mSpnPendidikanMaks.setAdapter(arrPendidikan);
        mSpnPendidikanDana.setAdapter(arrBiayaPendidikan);
        mSpnDindingRumah.setAdapter(arrDinding);
        mSpnKondisiDinding.setAdapter(arrKondisiFull);
        mSpnLantaiRumah.setAdapter(arrLantai);
        mSpnKondisiLantai.setAdapter(arrKondisi);
        mSpnAtapRumah.setAdapter(arrAtap);
        mSpnMck.setAdapter(arrMck);
        mSpnPenerangan.setAdapter(arrLampu);
        mSpnDaya.setAdapter(arrDaya);
        mSpnAirMinum.setAdapter(arrAir);
        mSpnKondisiAtap.setAdapter(arrKondisiFull);

        mRgRumah1.setOnCheckedChangeListener(this);
        mRgPakaian.setOnCheckedChangeListener(this);

        mContainerFotoDinding.setAdapter(mAdapterDinding);
        mContainerFotoLantai.setAdapter(mAdapterLantai);
        mContainerFotoMck.setAdapter(mAdapterMck);
        mContainerFotoAtap.setAdapter(mAdapterAtap);
    }

    private void initializeAdapter() {
        pokok = getResources().getStringArray(R.array.makan_pokok);
        lauk = getResources().getStringArray(R.array.makan_lauk);
        sayur = getResources().getStringArray(R.array.makan_sayur);
        buah = getResources().getStringArray(R.array.makan_buah);
        berobat = getResources().getStringArray(R.array.lokasi_berobat);
        penyakit = getResources().getStringArray(R.array.list_penyakit);
        pendidikan = getResources().getStringArray(R.array.list_pendidikan);
        biayaPendidikan = getResources().getStringArray(R.array.biaya_pendidikan);
        dinding = getResources().getStringArray(R.array.list_dinding);
        kondisi = getResources().getStringArray(R.array.list_kondisi);
        kondisiFull = getResources().getStringArray(R.array.list_kondisi_full);
        lantai = getResources().getStringArray(R.array.list_lantai);
        lampu = getResources().getStringArray(R.array.list_lampu);
        atap = getResources().getStringArray(R.array.list_atap);
        mck = getResources().getStringArray(R.array.list_mck);
        air = getResources().getStringArray(R.array.list_air);
        daya = getResources().getStringArray(R.array.list_daya);

        mAdapterDinding = new GridFotoAdapter(mListFotoDinding, this, false, 1);
        mAdapterLantai = new GridFotoAdapter(mListFotoLantai, this, false, 2);
        mAdapterMck = new GridFotoAdapter(mListFotoMck, this, false, 4);
        mAdapterAtap = new GridFotoAdapter(mListFotoAtap, this, false, 3);
    }

    @OnClick({R.id.btn_foto_dinding, R.id.btn_foto_lantai, R.id.btn_foto_mck, R.id.btn_foto_atap})
    void tambahFotoClick(View v) {
        if (v == mBtnFotoDinding) {
            showDialogPilihan(REQUEST_FILE_DINDING, REQUEST_CAMERA_DINDING);
        } else if (v == mBtnFotoLantai) {
            showDialogPilihan(REQUEST_FILE_LANTAI, REQUEST_CAMERA_LANTAI);
        } else if (v == mBtnFotoMck) {
            showDialogPilihan(REQUEST_FILE_MCK, REQUEST_CAMERA_MCK);
        } else {
            showDialogPilihan(REQUEST_FILE_ATAP, REQUEST_CAMERA_ATAP);
        }
    }

    private void showDialogPilihan(final int FILE_CODE, final int CAMERA_CODE) {
        final CharSequence[] items = {"Ambil Foto", "Cari dari Galery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(KriteriaEditActivity.this);
        builder.setTitle("Masukan Foto");
        builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Ambil Foto")) {
                            cameraIntent(CAMERA_CODE);
                        } else if (items[item].equals("Cari dari Galery")) {
                            galleryIntent(FILE_CODE);
                        }
                    }
                }
        );
        builder.show();
    }

    @OnItemSelected({R.id.spn_penerangan, R.id.spn_pendidikan_maks})
    void showData(Spinner spinner, int position) {
        if (mSpnPenerangan.getSelectedItem().equals("Listrik")) {
            mContainerDaya.setVisibility(View.VISIBLE);
        } else {
            mContainerDaya.setVisibility(View.GONE);
        }

        if (mSpnPendidikanMaks.getSelectedItemPosition() == 0) {
            mContainerPendidikan.setVisibility(View.GONE);
        } else {
            mContainerPendidikan.setVisibility(View.VISIBLE);
        }
    }

    private void galleryIntent(int FILE_CODE) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, FILE_CODE);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(intent, FILE_CODE);
        }
    }

    private void cameraIntent(int CAMERA_CODE) {
        if (KriteriaEditActivity.this.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = ImgUri.getOutputMediaFileUri();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, CAMERA_CODE);
        } else {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Sorry! Your device doesn't support camera");
        }
    }

    private int selectedRadio(RadioGroup rg) {
        RadioButton selectRadio = (RadioButton) findViewById(rg.getCheckedRadioButtonId());
        if (selectRadio.getText().toString().equals("Ya")) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void onItemsSelected(boolean[] selected) {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (group == mRgRumah1) {
            if (mRbRumah1Ya.isChecked()) {
                mContainerRumahYa.setVisibility(View.VISIBLE);
            } else {
                mContainerRumahYa.setVisibility(View.GONE);
            }
        } else if (group == mRgPakaian) {
            if (mRbPakaianYa.isChecked()) {
                mContainerPakaian.setVisibility(View.VISIBLE);
            } else {
                mContainerPakaian.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onImageClickListener(int position, int v) {
        String path = null;
        switch (v) {
            case 1:
                path = mListFotoDinding.get(position);
                break;
            case 2:
                path = mListFotoLantai.get(position);
                break;
            case 3:
                path = mListFotoAtap.get(position);
                break;
            case 4:
                path = mListFotoMck.get(position);
                break;
        }
        if (path == null) {
            ToastUtil.toastShow(KriteriaEditActivity.this, "Gambar tidak dapat di tampilkan");
            return;
        }

        ImgZoom imgZoom = new ImgZoom(KriteriaEditActivity.this, path);
        imgZoom.setCanceledOnTouchOutside(true);
        imgZoom.show();

    }
}
