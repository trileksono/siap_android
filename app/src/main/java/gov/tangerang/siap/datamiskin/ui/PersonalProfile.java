package gov.tangerang.siap.datamiskin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.Callable;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.adapter.ListJaminanPersonalAdapter;
import gov.tangerang.siap.datamiskin.database.DbHelper;
import gov.tangerang.siap.datamiskin.model.DTO;
import gov.tangerang.siap.datamiskin.model.response.Warga;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.ui.dialog.ImgZoom;
import gov.tangerang.siap.datamiskin.util.Constant;
import gov.tangerang.siap.datamiskin.util.NestedListView;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by tri on 8/13/16.
 */
public class PersonalProfile extends AppCompatActivity {

    @Bind(R.id.img_profile)
    ImageView mImgProfile;
    @Bind(R.id.et_nik)
    TextInputEditText mEtNik;
    @Bind(R.id.lbl_nik)
    TextInputLayout mLblNik;
    @Bind(R.id.et_nama)
    TextInputEditText mEtNama;
    @Bind(R.id.et_no_kk)
    TextInputEditText mEtNoKk;
    @Bind(R.id.et_alamat)
    TextInputEditText mEtAlamat;
    @Bind(R.id.txt_rt)
    TextInputEditText mTxtRt;
    @Bind(R.id.txt_rw)
    TextInputEditText mTxtRw;
    @Bind(R.id.txt_kelurahan)
    TextInputEditText mTxtKelurahan;
    @Bind(R.id.txt_kecamatan)
    TextInputEditText mTxtKecamatan;
    @Bind(R.id.txt_status_kota)
    TextInputEditText mTxtStatusKota;
    @Bind(R.id.list_jaminan)
    NestedListView mListView;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.progresbar)
    ProgressBar mProgresbar;
    @Bind(R.id.scroll_view)
    ScrollView mScrollView;

    CancellationTokenSource mCancellation;
    String NIK = "";
    List jaminanDb;
    DbHelper mDbHelper;
    ListJaminanPersonalAdapter adapter;
    String STATUS_KOTA;
    String path;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_profile);
        ButterKnife.bind(this);
        mDbHelper = new DbHelper(this);
        mCancellation = new CancellationTokenSource();
        Intent i = getIntent();
        NIK = i.getStringExtra("nik");
        STATUS_KOTA = i.getStringExtra("status");

        jaminanDb = mDbHelper.getJaminan("1");

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mScrollView.setVisibility(View.GONE);

        Task.callInBackground(new Callable<Warga>() {
            @Override
            public Warga call() throws Exception {
                Response<DTO<Warga>> penduduk = new Service().getApi(PersonalProfile.this)
                        .findPenduduk(NIK).execute();
                if (penduduk.isSuccessful()) {
                    if (penduduk.body().getErrorCode().equals("00")) {
                        return penduduk.body().getData();
                    }
                }
                return null;
            }
        },mCancellation.getToken()).onSuccess(new Continuation<Warga, Object>() {
            @Override
            public Object then(Task<Warga> task) throws Exception {
                Timber.e("MASUK");
                if (task.getResult() != null) {
                    initialView(task.getResult());
                } else {
                    ToastUtil.toastShow(PersonalProfile.this, getResources().getString(R.string.error_message1));
                    finish();
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR, mCancellation.getToken());

    }

    @OnClick(R.id.img_profile)
    void showImage(){
        ImgZoom img = new ImgZoom(PersonalProfile.this,path);
        img.setCanceledOnTouchOutside(true);
        img.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCancellation.cancel();
    }

    private void initialView(Warga warga) {
        mEtNik.setText(NIK);
        mEtAlamat.setText(warga.getAlamat());
        mEtNama.setText(warga.getNama());
        mEtNoKk.setText(warga.getNoKk());
        mTxtRt.setText(warga.getNoRt());
        mTxtRw.setText(warga.getNoRw());
        mTxtKecamatan.setText(warga.getKecamatan());
        mTxtKelurahan.setText(warga.getKelurahan());
        String statusText;
        if(STATUS_KOTA == null){
            statusText = "Belum cek NIK";
        }else if(STATUS_KOTA.equals("0")){
            statusText = "Belum cek NIK";
        }else if(STATUS_KOTA.equals("1")){
            statusText = "Warga Kota Tangerang";
        }else{
            statusText = "Warga Luar Kota Tangerang";
        }
        mTxtStatusKota.setText(statusText);

        path = warga.getFoto();
        setImage(warga.getFoto());
        mProgresbar.setVisibility(View.GONE);
        mScrollView.setVisibility(View.VISIBLE);

        adapter = new ListJaminanPersonalAdapter(this,jaminanDb,warga.getJaminan());
        mListView.setAdapter(adapter);
    }

    private void setImage(String path) {
        Picasso.with(this)
                .load(Constant.IMG_URL + path)
                .error(R.drawable.no_foto)
                .fit().centerCrop()
                .into(mImgProfile);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
