package gov.tangerang.siap.datamiskin.ui;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.database.DbHelper;
import gov.tangerang.siap.datamiskin.model.DTO;
import gov.tangerang.siap.datamiskin.model.response.JPersonal;
import gov.tangerang.siap.datamiskin.model.response.Warga;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.ui.dialog.ImgZoom;
import gov.tangerang.siap.datamiskin.util.Constant;
import gov.tangerang.siap.datamiskin.util.ImgCompress;
import gov.tangerang.siap.datamiskin.util.ImgUri;
import gov.tangerang.siap.datamiskin.util.MyProgressDialog;
import gov.tangerang.siap.datamiskin.util.PrefUtil;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.tajchert.nammu.Nammu;
import pl.tajchert.nammu.PermissionCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by tri on 7/29/16.
 */
public class JPersonalActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.img_profile)
    ImageView imgProfile;
    @Bind(R.id.et_nik)
    TextInputEditText etNik;
    @Bind(R.id.et_nama)
    TextInputEditText etNama;
    @Bind(R.id.et_no_kk)
    TextInputEditText etNoKk;
    @Bind(R.id.et_alamat)
    TextInputEditText etAlamat;
    @Bind(R.id.tbl_jaminan)
    TableLayout tblJaminan;
    @Bind(R.id.btn_cek)
    Button btnCek;
    @Bind(R.id.btn_simpan)
    Button btnSave;
    @Bind(R.id.txt_rt)
    TextInputEditText mTxtRt;
    @Bind(R.id.txt_rw)
    TextInputEditText mTxtRw;
    @Bind(R.id.txt_kelurahan)
    TextInputEditText mTxtKelurahan;
    @Bind(R.id.txt_kecamatan)
    TextInputEditText mTxtKecamatan;
    @Bind(R.id.txt_status_kota)
    TextInputEditText mStatusKota;
    @Bind(R.id.btn_foto)
    Button mBtnFoto;

    private int REQUEST_CAMERA = 100;

    private DbHelper db;
    private List datas;
    private MyProgressDialog progressDialog;
    private ImgCompress imgCompress;
    private Uri fileUri; // file url to store image
    private String filePath;
    private Bitmap imgCamera;
    private String path;

    private String STATUS_KOTA = "0";
    private boolean IS_EDIT = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jpersonal);
        ButterKnife.bind(this);

        db = new DbHelper(this);
        imgCompress = new ImgCompress();
        initialView();
    }

    private void initialView() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new MyProgressDialog(this, "Mohon tunggu", false, true, "");
        Task.call(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                datas = db.getJaminan("1");
                initialTblJaminan(null);
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

    @OnClick(R.id.btn_cek)
    void cariNIK() {
        if (etNik.getText().toString().trim().length() < 15) {
            etNik.setError("NO KK / NIK tidak sesuai");
            return;
        }
        progressDialog.show();
        new Service().getApi(JPersonalActivity.this).cariPenduduk(etNik.getText().toString())
                .enqueue(new Callback<DTO<Warga>>() {
                    @Override
                    public void onResponse(Call<DTO<Warga>> call, Response<DTO<Warga>> response) {
                        progressDialog.dismiss();
                        try {
                            if (response.isSuccessful()) {
                                if (response.body().getErrorCode().equals("00")) {
                                    if (response.body().getMessage().equals("Nice request")) {
                                        etNama.setText(response.body().getData().getNama());
                                        etAlamat.setText((response.body().getData().getAlamat() == null) ?
                                                "" : response.body().getData().getAlamat());
                                        etNoKk.setText((response.body().getData().getNoKk() == null) ?
                                                "" : response.body().getData().getNoKk());
                                        mTxtKecamatan.setText(response.body().getData().getKecamatan());
                                        mTxtKelurahan.setText(response.body().getData().getKelurahan());
                                        mTxtRt.setText(response.body().getData().getNoRt());
                                        mTxtRw.setText(response.body().getData().getNoRw());

                                        STATUS_KOTA = response.body().getData().getStatusKota();

                                        mStatusKota.setText("Warga Kota Tangerang");
                                        etNik.setEnabled(false);
                                        btnSave.setEnabled(true);
                                        btnCek.setEnabled(false);
                                    } else {
                                        showDialogEdit(response.body().getMessage(), response.body().getData());
                                    }
                                } else if (response.body().getErrorCode().equals("20")) {
                                    PrefUtil.setStringPref(JPersonalActivity.this, Constant.isLogedIn, "0");
                                    Intent intent = new Intent(JPersonalActivity.this, Login.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    ToastUtil.toastOnUi(JPersonalActivity.this, response.body().getMessage());
                                } else {
                                    ToastUtil.toastOnUi(JPersonalActivity.this, response.body().getMessage());
                                }
                            }
                        } catch (Exception e) {
                            Timber.d("%s", e.getCause());
                            ToastUtil.toastOnUi(JPersonalActivity.this, "Terjadi kesalahan saat memproses data");
                        }
                    }

                    @Override
                    public void onFailure(Call<DTO<Warga>> call, Throwable t) {
                        Timber.d("%s", t.getCause());
                        progressDialog.dismiss();
                        ToastUtil.toastOnUi(JPersonalActivity.this, getResources().getString(R.string.error_message1));
                    }
                });
    }

    @OnClick(R.id.btn_foto)
    void getImage() {
        openCamera();
    }

    @OnClick(R.id.img_profile)
    void showImage(){
        if(path != null) {
            if(!path.equals("")) {
                ImgZoom img = new ImgZoom(JPersonalActivity.this, path);
                img.setCanceledOnTouchOutside(true);
                img.show();
            }
        }
    }

    @OnClick(R.id.btn_simpan)
    void simpanPerubahan() {
        if (IS_EDIT) {
            update();
        } else {
            simpan();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Nammu.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showDialogEdit(String pesan, @Nullable final Warga data) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(pesan + ", lanjutkan ? ");
        alertDialogBuilder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                if (data != null) {

                    IS_EDIT = true; // FLAG EDIT untuk bedain update atau save

                    etNik.setEnabled(false);
                    btnSave.setEnabled(true);
                    btnCek.setEnabled(false);

                    etNama.setText(data.getNama());
                    etAlamat.setText((data.getAlamat() == null) ? "" : data.getAlamat());
                    etNoKk.setText((data.getNoKk() == null) ? "" : data.getNoKk());
                    mTxtKecamatan.setText(data.getKecamatan());
                    mTxtKelurahan.setText(data.getKelurahan());
                    if (data.getNoRt() != null) {
                        if (data.getNoRt().equals("")) {
                            mTxtRt.setEnabled(true);
                            mTxtRt.setInputType(InputType.TYPE_CLASS_NUMBER);
                        } else {
                            mTxtRt.setText(data.getNoRt());
                        }
                    } else {
                        mTxtRt.setEnabled(true);
                        mTxtRt.setInputType(InputType.TYPE_CLASS_NUMBER);
                    }
                    if (data.getNoRw() != null) {
                        mTxtRw.setText(data.getNoRw());
                        if (data.getNoRw().trim().equals("")) {
                            mTxtRw.setEnabled(true);
                            mTxtRw.setInputType(InputType.TYPE_CLASS_NUMBER);
                        }
                    } else {
                        mTxtRw.setEnabled(true);
                        mTxtRw.setInputType(InputType.TYPE_CLASS_NUMBER);
                    }
                    setImage(data.getFoto());
                    if (data.getStatusKota() != null) {
                        STATUS_KOTA = data.getStatusKota();
                        if (data.getStatusKota().equals("1")) {
                            mStatusKota.setText("Warga Kota Tangerang");
                        } else {
                            mStatusKota.setText("Warga Luar Kota Tangerang");
                        }
                    } else {
                        STATUS_KOTA = "2";
                    }
                    initialTblJaminan(data.getJaminan());
                }
            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void openCamera() {
        int permissionCheck = ContextCompat.checkSelfPermission(JPersonalActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        final int cameraCheck = ContextCompat.checkSelfPermission(JPersonalActivity.this,
                Manifest.permission.CAMERA);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED && cameraCheck == PackageManager.PERMISSION_GRANTED) {
            cameraIntent();
        } else {
            Nammu.askForPermission(JPersonalActivity.this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, new PermissionCallback() {
                        @Override
                        public void permissionGranted() {
                            cameraIntent();
                        }

                        @Override
                        public void permissionRefused() {
                            ToastUtil.toastShow(JPersonalActivity.this, "Tidak dapat mengambil foto");
                        }
                    });
        }
    }

    private void simpan() {
        if (this.filePath == null) {
            ToastUtil.toastShow(JPersonalActivity.this, "Silahkan masukan foto penduduk");
            return;
        }

        if (mTxtRt.getText().toString().equals("")) {
            mTxtRt.setError("Silahkan diisi");
            return;
        }
        if (mTxtRw.getText().toString().equals("")) {
            mTxtRw.setError("Silahkan diisi");
            return;
        }

        progressDialog.show();
        File f = new File(this.filePath);
        if (f.exists()) {
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("foto", f.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), f))
                    .addFormDataPart("nik", etNik.getText().toString())
                    .addFormDataPart("nama", etNama.getText().toString())
                    .addFormDataPart("kk", etNoKk.getText().toString())
                    .addFormDataPart("alamat", etAlamat.getText().toString())
                    .addFormDataPart("kecamatan", mTxtKecamatan.getText().toString())
                    .addFormDataPart("kelurahan", mTxtKelurahan.getText().toString())
                    .addFormDataPart("rt", mTxtRt.getText().toString())
                    .addFormDataPart("rw", mTxtRw.getText().toString())
                    /*.addFormDataPart("kecamatan", spnKecamatan.getSelectedItem().toString())
                    .addFormDataPart("kelurahan", spnKelurahan.getSelectedItem().toString())*/
                    .addFormDataPart("jaminan", getJaminan())
                    .build();

            new Service().getApi(JPersonalActivity.this).simpanPenduduk(body).enqueue(new Callback<DTO>() {
                @Override
                public void onResponse(Call<DTO> call, Response<DTO> response) {
                    progressDialog.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body().getErrorCode().equals("00")) {
                            ToastUtil.toastOnUi(JPersonalActivity.this, "Data berhasil disimpan");
                            finish();
                        } else {
                            ToastUtil.toastOnUi(JPersonalActivity.this, response.body().getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<DTO> call, Throwable t) {
                    progressDialog.dismiss();
                    ToastUtil.toastOnUi(JPersonalActivity.this, getResources().getString(R.string.error_message1));
                }
            });
        }
    }

    private void update() {

        if (mTxtRt.getText().toString().equals("")) {
            mTxtRt.setError("Silahkan diisi");
            return;
        }
        if (mTxtRw.getText().toString().equals("")) {
            mTxtRw.setError("Silahkan diisi");
            return;
        }
        progressDialog.show();
        String jaminan = getJaminan();
        Timber.i("%s", jaminan);
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("nik", etNik.getText().toString())
                .addFormDataPart("nama", etNama.getText().toString())
                .addFormDataPart("kk", etNoKk.getText().toString())
                .addFormDataPart("alamat", etAlamat.getText().toString())
                .addFormDataPart("kecamatan", mTxtKecamatan.getText().toString())
                .addFormDataPart("kelurahan", mTxtKelurahan.getText().toString())
                .addFormDataPart("rt", mTxtRt.getText().toString())
                .addFormDataPart("rw", mTxtRw.getText().toString())
                .addFormDataPart("statusKota", STATUS_KOTA)
                .addFormDataPart("jaminan", jaminan)
                .build();

        new Service().getApi(JPersonalActivity.this).updatePenduduk(body).enqueue(new Callback<DTO>() {
            @Override
            public void onResponse(Call<DTO> call, Response<DTO> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getErrorCode().equals("00")) {
                        ToastUtil.toastOnUi(JPersonalActivity.this, response.body().getMessage());
                        finish();
                    } else {
                        ToastUtil.toastOnUi(JPersonalActivity.this, response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DTO> call, Throwable t) {
                progressDialog.dismiss();
                ToastUtil.toastOnUi(JPersonalActivity.this, getResources().getString(R.string.error_message1));
            }
        });
    }

    private String getJaminan() {
        String result = "";
        for (int x = 0; x < tblJaminan.getChildCount(); x++) {
            String status = null;
            String nama = null;
            ViewGroup row = (ViewGroup) tblJaminan.getChildAt(x);
            for (int i = 0; i < row.getChildCount(); i++) {
                TextView txt = (TextView) row.getChildAt(0);
                nama = txt.getText().toString();
                AppCompatCheckBox chk = (AppCompatCheckBox) row.getChildAt(1);
                if (chk.isChecked()) {
                    status = "0";
                }
            }
            result += nama + "|" + status;
            if (x < tblJaminan.getChildCount() - 1) {
                result += ";";
            }
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                path = fileUri.getPath();
                path = imgCompress.compressImage(JPersonalActivity.this, path);
                filePath = path;
                if (!filePath.equals("") || filePath != null) {
                    imgCamera = BitmapFactory.decodeFile(path);
                    if (IS_EDIT) {
                        updateFoto();
                    } else {
                        imgProfile.setImageBitmap(imgCamera);
                    }
                } else {
                    Toast.makeText(JPersonalActivity.this,
                            "Sorry, file path is missing!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void updateFoto() {
        if (this.filePath == null) {
            ToastUtil.toastShow(JPersonalActivity.this, "Maaf terjadi kesalahan");
            return;
        }
        File f = new File(this.filePath);
        if (f.exists()) {
            RequestBody body = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("foto", f.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), f))
                    .build();

            new Service().getApi(JPersonalActivity.this).updateFotoPersonal(body, etNik.getText().toString()).enqueue(new Callback<DTO>() {
                @Override
                public void onResponse(Call<DTO> call, Response<DTO> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getErrorCode().equals("00")) {
                            setImage((String) response.body().getData());
                        }
                    }
                }

                @Override
                public void onFailure(Call<DTO> call, Throwable t) {
                    ToastUtil.toastOnUi(JPersonalActivity.this, getResources().getString(R.string.error_message1));
                }
            });
        }
    }

    private void setImage(String path) {
        this.path = path;
        Picasso.with(JPersonalActivity.this)
                .load(Constant.IMG_URL+path)
                .error(R.drawable.no_foto)
                .fit().centerCrop()
                .into(imgProfile);
    }

    private void cameraIntent() {
        if (JPersonalActivity.this.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = ImgUri.getOutputMediaFileUri();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, REQUEST_CAMERA);
        } else {
            // no camera on this device
            ToastUtil.toastShow(JPersonalActivity.this, "Sorry! Your device doesn't support camera");
        }
    }

    private void initialTblJaminan(@Nullable List<JPersonal> jp) {
        ColorStateList colorStateList = new ColorStateList(
                new int[][]{new int[]{android.R.attr.state_enabled}},
                new int[]{ContextCompat.getColor(JPersonalActivity.this, R.color.colorAccent)}
        );
        tblJaminan.removeAllViews();
        for (int x = 0; x < datas.size(); x++) {
            TableRow row = new TableRow(this);
            row.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            TableRow.LayoutParams tx = new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            TextView jaminan = new TextView(this);
            jaminan.setText(datas.get(x).toString());
            jaminan.setPadding(5, 5, 5, 5);
            jaminan.setLayoutParams(tx);

            AppCompatCheckBox chk = new AppCompatCheckBox(this);
            chk.setChecked(false);
            chk.setSupportButtonTintList(colorStateList);

            if (jp != null) {
                for (int i = 0; i < jp.size(); i++) {
                    if (jp.get(i).getNama().equals(datas.get(x).toString())) { //&& jp.get(i).getStatus() != null) {
                        chk.setChecked(true);
                    }
                }
            }
            row.addView(jaminan);
            row.addView(chk);
            tblJaminan.addView(row);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
