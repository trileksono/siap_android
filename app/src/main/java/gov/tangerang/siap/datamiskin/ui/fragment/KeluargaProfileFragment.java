package gov.tangerang.siap.datamiskin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.adapter.GridFotoAdapter;
import gov.tangerang.siap.datamiskin.adapter.ListAnggotaKeluargaAdapter;
import gov.tangerang.siap.datamiskin.adapter.ListJaminanAdapter;
import gov.tangerang.siap.datamiskin.model.DTO;
import gov.tangerang.siap.datamiskin.model.response.AnggotaKeluarga;
import gov.tangerang.siap.datamiskin.model.response.CariKeluarga;
import gov.tangerang.siap.datamiskin.model.response.FotoKeluarga;
import gov.tangerang.siap.datamiskin.model.response.KriteriaRes;
import gov.tangerang.siap.datamiskin.model.response.ListJaminan;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.ui.dialog.ImgZoom;
import gov.tangerang.siap.datamiskin.util.ExpandableHeightGridView;
import gov.tangerang.siap.datamiskin.util.FindNamaJalan;
import gov.tangerang.siap.datamiskin.util.MyProgressDialog;
import gov.tangerang.siap.datamiskin.util.NestedListView;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by tri on 8/8/16.
 */
public class KeluargaProfileFragment extends Fragment implements OnMapReadyCallback {

    @Bind(R.id.progresbar)
    ProgressBar mProgresbar;
    @Bind(R.id.et_no_kk)
    TextInputEditText mEtNoKk;
    @Bind(R.id.et_alamat)
    TextInputEditText mEtAlamat;
    @Bind(R.id.et_kecamatan)
    TextInputEditText mEtKecamatan;
    @Bind(R.id.et_kelurahan)
    TextInputEditText mEtKelurahan;
    @Bind(R.id.list_keluarga)
    NestedListView mListKeluarga;
    @Bind(R.id.keluarga_container)
    LinearLayout mKeluargaContainer;
    @Bind(R.id.recycle_foto)
    ExpandableHeightGridView mRecycleFoto;
    @Bind(R.id.foto_container)
    LinearLayout mFotoContainer;
    @Bind(R.id.detail_kriteria)
    Button mDetailKriteria;
    @Bind(R.id.tv_detil_jaminan)
    TextView mTvDetilJaminan;
    @Bind(R.id.detail_jaminan)
    ImageView mDetailJaminan;
    @Bind(R.id.list_jaminan)
    NestedListView mListJaminan;
    @Bind(R.id.container)
    LinearLayout mContainer;
    @Bind(R.id.scroll_view)
    ScrollView mScrollView;
    @Bind(R.id.et_rt)
    TextInputEditText mEtRt;
    @Bind(R.id.et_rw)
    TextInputEditText mEtRw;
    @Bind(R.id.et_status_kota)
    TextInputEditText mStatusKota;
    @Bind(R.id.txt_foto_null)
    TextView mTextFotoNull;
    @Bind(R.id.text_kriteria)
    TextView mTextKriteria;

    private int REQUEST_MAP = 13;

    private String TAG = "PROFILE";
    private GoogleMap maps;
    private double lat = -6.181056;
    private double lang = 106.638021;
    private CariKeluarga datasKeluarga;
    private List<ListJaminan> listJaminan = new ArrayList<>();
    private List<AnggotaKeluarga> listKeluarga = new ArrayList<>();
    private List<FotoKeluarga> listFoto = new ArrayList<>();
    private GridFotoAdapter fotoAdapter;
    private ListAnggotaKeluargaAdapter keluargaAdapter;
    private List<String> listPath = new ArrayList<>();
    private MyProgressDialog mDialog;
    private ListJaminanAdapter jaminanAdapter;
    private FindNamaJalan findJalan;
    private String KK;
    private String STATUS_KOTA;
    private String STATUS = "1";
    private SupportMapFragment mMapFragment;
    private View mView;
    private CancellationTokenSource mCancellationToken;
    private String namaJalan;

    public static KeluargaProfileFragment newInstance(String kk, String statusKota) {
        KeluargaProfileFragment myFragment = new KeluargaProfileFragment();
        Bundle args = new Bundle();
        args.putString("kk", kk);
        args.putString("status", statusKota);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCancellationToken = new CancellationTokenSource();
        Timber.e("CREATE");
        KK = getArguments().getString("kk");
        STATUS_KOTA = getArguments().getString("status");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_keluarga_profile, container, false);
        }
        ButterKnife.bind(this, mView);
        mDialog = new MyProgressDialog(getActivity(), "Mohon tunggu", false, true, "");
        if (mMapFragment == null) {
            mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.maps);
            mMapFragment.getMapAsync(this);
        }
        findJalan = new FindNamaJalan(getActivity().getApplicationContext());
        if (savedInstanceState == null) {
            taskRequest();
        }
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        STATUS = "23";

        Timber.e("Destroy");
        mCancellationToken.cancel();
    }

    @OnClick(R.id.detail_kriteria)
    void showDetilKriteria() {
        mDialog.show();
        new Service().getApi(getActivity().getApplicationContext()).
                getAllKriteria(mEtNoKk.getText().toString()).enqueue(new Callback<DTO<KriteriaRes>>() {
            @Override
            public void onResponse(Call<DTO<KriteriaRes>> call, Response<DTO<KriteriaRes>> response) {
                mDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getErrorCode().equals("00")) {
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_fragment, KriteriaResultFragment.instance(response.body().getData()), TAG)
                                .addToBackStack(null)
                                .commit();
                    } else {
                        ToastUtil.toastOnUi(getActivity(), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DTO<KriteriaRes>> call, Throwable t) {
                Timber.e(t.getMessage(), t.getCause());
                mDialog.dismiss();
                ToastUtil.toastOnUi(getActivity(), getResources().getString(R.string.error_message1));
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.maps = googleMap;
    }

    private void taskRequest() {
        Task.callInBackground(new Callable<CariKeluarga>() {
            @Override
            public CariKeluarga call() {
                try {
                    Response<DTO<CariKeluarga>> res = new Service().getApi(getActivity().getApplicationContext()).findKeluarga(KK).execute();
                    if (res.isSuccessful()) {
                        if (res.body().getErrorCode().equals("00")) {
                            if (res.body().getData().getLat() != null) {
                                if (!res.body().getData().getLat().equals("0.0")) {
                                    Timber.e(res.body().getData().getLat());
                                    namaJalan = findJalan.pengguna(Double.parseDouble(res.body().getData().getLat()),
                                            Double.parseDouble(res.body().getData().getLon()));
                                }
                            }
                            return res.body().getData();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    getActivity().finish();
                    ToastUtil.toastOnUi(getActivity(), getResources().getString(R.string.error_message1));
                }
                return null;
            }
        }, mCancellationToken.getToken()).onSuccess(new Continuation<CariKeluarga, Object>() {
            @Override
            public Object then(Task<CariKeluarga> task) throws Exception {
                if (task.getResult() == null) {
                    getActivity().finish();
                    return null;
                }
                CariKeluarga cr = task.getResult();
                listJaminan = cr.getListJaminan();
                listKeluarga = cr.getAnggotaKeluarga();
                listFoto = cr.getFotoKeluarga();
                for (int x = 0; x < listFoto.size(); x++) {
                    listPath.add(listFoto.get(x).getFoto());
                    if (listPath.size() >= 6) {
                        break;
                    }
                }

                jaminanAdapter = new ListJaminanAdapter(getActivity(), listJaminan, listKeluarga.size());
                keluargaAdapter = new ListAnggotaKeluargaAdapter(listKeluarga, getActivity().getApplicationContext());
                fotoAdapter = new GridFotoAdapter(listPath, getActivity().getApplicationContext(), false, 0);
                if (cr.getLat() == null || cr.getLon() == null) {
                    ToastUtil.toastOnUi(getActivity(), "Tidak ada data lokasi untuk keluarga ini");
                } else if (cr.getLat().equals("0.0") || cr.getLon().equals("0.0")) {
                    ToastUtil.toastOnUi(getActivity(), "Tidak ada data lokasi untuk keluarga ini");
                } else {
                    lang = Double.parseDouble(cr.getLon());
                    lat = Double.parseDouble(cr.getLat());
                }

                String statusText;
                if (STATUS_KOTA == null) {
                    statusText = "Belum cek KK";
                } else if (STATUS_KOTA.equals("0")) {
                    statusText = "Belum cek KK";
                } else if (STATUS_KOTA.equals("1")) {
                    statusText = "Kota Tangerang";
                } else {
                    statusText = "Luar Kota Tangerang";
                }

                mStatusKota.setText(statusText);
                mEtNoKk.setText(cr.getNoKk());
                mEtAlamat.setText(cr.getAlamat());
                mEtKecamatan.setText(cr.getKecamatan());
                mEtKelurahan.setText(cr.getKelurahan());
                mEtRt.setText(cr.getNoRt());
                mEtRw.setText(cr.getNoRw());

                mListJaminan.setAdapter(jaminanAdapter);
                mListKeluarga.setAdapter(keluargaAdapter);
                mRecycleFoto.setAdapter(fotoAdapter);
                mRecycleFoto.setExpanded(true);
                fotoAdapter.setOnImageClick(new GridFotoAdapter.OnImageClick() {
                    @Override
                    public void onImageClickListener(int position, int jenis) {
                        ImgZoom imgZoom = new ImgZoom(getActivity(), listPath.get(position));
                        imgZoom.setCanceledOnTouchOutside(true);
                        imgZoom.show();
                    }
                });

                Timber.e(lang + " " + lat);
                LatLng latLng = new LatLng(lat, lang);

                CameraUpdate factory = CameraUpdateFactory.newLatLng(latLng);
                if (maps != null) {
                    maps.moveCamera(factory);
                    maps.animateCamera(CameraUpdateFactory.zoomTo(15.0f));
                    maps.getUiSettings().setZoomControlsEnabled(true);
                    if (lang != 106.638021) {
                        if (lang != 0) {
                            Timber.e("%s", namaJalan);
                            maps.addMarker(new MarkerOptions().position(latLng).title("Detail")
                                    .snippet(namaJalan)).showInfoWindow();
                        }
                    }
                }
                if (listPath.size() < 1) {
                    mTextFotoNull.setVisibility(View.VISIBLE);
                }

                if(cr.isKriteriaKeluarga()){
                    mDetailKriteria.setVisibility(View.VISIBLE);
                    mTextKriteria.setVisibility(View.GONE);
                }else{
                    mDetailKriteria.setVisibility(View.GONE);
                    mTextKriteria.setVisibility(View.VISIBLE);
                }
                Timber.e("%s",cr.isKriteriaKeluarga());
                mProgresbar.setVisibility(View.GONE);
                mScrollView.setVisibility(View.VISIBLE);
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR, mCancellationToken.getToken());
    }
}
