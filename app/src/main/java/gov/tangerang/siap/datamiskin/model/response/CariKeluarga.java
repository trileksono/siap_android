package gov.tangerang.siap.datamiskin.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by tri on 7/27/16.
 */
public class CariKeluarga implements Parcelable {
    private String noKk;
    private String kecamatan;
    private String lon;
    private String kelurahan;
    private String lat;
    private String alamat;
    private String noRt;
    private String noRw;
    private String statusKota;
    private String userId;
    private String statusAktif;
    private List<AnggotaKeluarga> anggotaKeluarga;
    private List<FotoKeluarga> fotoKeluarga;
    private List<ListJaminan> listJaminan;
    private List<JKeluarga> jKeluarga;
    private boolean kriteriaKeluarga;

    public boolean isKriteriaKeluarga() {
        return kriteriaKeluarga;
    }

    public void setKriteriaKeluarga(boolean kriteriaKeluarga) {
        this.kriteriaKeluarga = kriteriaKeluarga;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNoKk() {
        return noKk;
    }

    public void setNoKk(String noKk) {
        this.noKk = noKk;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public List<AnggotaKeluarga> getAnggotaKeluarga() {
        return anggotaKeluarga;
    }

    public void setAnggotaKeluarga(List<AnggotaKeluarga> anggotaKeluarga) {
        this.anggotaKeluarga = anggotaKeluarga;
    }

    public List<FotoKeluarga> getFotoKeluarga() {
        return fotoKeluarga;
    }

    public void setFotoKeluarga(List<FotoKeluarga> fotoKeluarga) {
        this.fotoKeluarga = fotoKeluarga;
    }

    public String getNoRw() {
        return noRw;
    }

    public void setNoRw(String noRw) {
        this.noRw = noRw;
    }

    public String getNoRt() {
        return noRt;
    }

    public void setNoRt(String noRt) {
        this.noRt = noRt;
    }

    public List<ListJaminan> getListJaminan() {
        return listJaminan;
    }

    public void setListJaminan(List<ListJaminan> listJaminan) {
        this.listJaminan = listJaminan;
    }

    public List<JKeluarga> getjKeluarga() {
        return jKeluarga;
    }

    public void setjKeluarga(List<JKeluarga> jKeluarga) {
        this.jKeluarga = jKeluarga;
    }

    public String getStatusKota() {
        return statusKota;
    }

    public void setStatusKota(String statusKota) {
        this.statusKota = statusKota;
    }

    public String getStatusAktif() {
        return statusAktif;
    }

    public void setStatusAktif(String statusAktif) {
        this.statusAktif = statusAktif;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.noKk);
        dest.writeString(this.kecamatan);
        dest.writeString(this.lon);
        dest.writeString(this.kelurahan);
        dest.writeString(this.lat);
        dest.writeString(this.alamat);
        dest.writeString(this.noRt);
        dest.writeString(this.noRw);
        dest.writeString(this.statusAktif);
        dest.writeString(this.statusKota);
        dest.writeString(this.userId);
        dest.writeTypedList(this.anggotaKeluarga);
        dest.writeTypedList(this.fotoKeluarga);
        dest.writeTypedList(this.listJaminan);
        dest.writeTypedList(this.jKeluarga);
    }

    public CariKeluarga() {
    }

    protected CariKeluarga(Parcel in) {
        this.noKk = in.readString();
        this.kecamatan = in.readString();
        this.lon = in.readString();
        this.kelurahan = in.readString();
        this.lat = in.readString();
        this.alamat = in.readString();
        this.noRt = in.readString();
        this.noRw = in.readString();
        this.statusAktif = in.readString();
        this.statusKota = in.readString();
        this.userId = in.readString();
        this.anggotaKeluarga = in.createTypedArrayList(AnggotaKeluarga.CREATOR);
        this.fotoKeluarga = in.createTypedArrayList(FotoKeluarga.CREATOR);
        this.listJaminan = in.createTypedArrayList(ListJaminan.CREATOR);
        this.jKeluarga = in.createTypedArrayList(JKeluarga.CREATOR);
    }

    public static final Creator<CariKeluarga> CREATOR = new Creator<CariKeluarga>() {
        @Override
        public CariKeluarga createFromParcel(Parcel source) {
            return new CariKeluarga(source);
        }

        @Override
        public CariKeluarga[] newArray(int size) {
            return new CariKeluarga[size];
        }
    };
}
