package gov.tangerang.siap.datamiskin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.concurrent.Callable;

import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.database.DbHelper;
import gov.tangerang.siap.datamiskin.model.DTOList;
import gov.tangerang.siap.datamiskin.model.response.ListJaminan;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.ui.Login;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tri on 8/3/16.
 */
public class SplashScreen extends AppCompatActivity {

    @Bind(R.id.img_splash)
    ImageView imgSplash;
    @Bind(R.id.prog_splash)
    ProgressBar progSplash;

    DbHelper db;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        ButterKnife.bind(this);

        db = new DbHelper(this);
        progSplash.setVisibility(View.VISIBLE);
        getJaminanBackground();
        Thread background = new Thread() {
            public void run() {
                try {
                    sleep(1500);
                    Intent i = new Intent(SplashScreen.this, Login.class);
                    startActivity(i);
                    finish();
                } catch (Exception e) {
                }
            }
        };
        background.start();
    }

    void getJaminanBackground() {
        new Service().getApi(this).getJaminan("").enqueue(new Callback<DTOList<ListJaminan>>() {
            @Override
            public void onResponse(Call<DTOList<ListJaminan>> call, final Response<DTOList<ListJaminan>> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().getErrorCode().equals("00")) {
                            Task.call(new Callable<Object>() {
                                @Override
                                public Object call() throws Exception {
                                    db.insertJaminan(response.body().getData());
                                    return null;
                                }
                            },Task.UI_THREAD_EXECUTOR);
                        } else {
                            ToastUtil.toastOnUi(SplashScreen.this, response.body().getMessage());
                        }
                    }
                } catch (Exception e) {}
            }

            @Override
            public void onFailure(Call<DTOList<ListJaminan>> call, Throwable t) {
                ToastUtil.toastOnUi(SplashScreen.this, getResources().getString(R.string.error_message1));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
