package gov.tangerang.siap.datamiskin;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import gov.tangerang.siap.datamiskin.database.DbHelper;
import pl.tajchert.nammu.Nammu;
import timber.log.Timber;

/**
 * Created by tri on 6/25/16.
 */
public class Siap extends Application {

    private static Siap instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Nammu.init(getApplicationContext());
        SQLiteDatabase db = new DbHelper(this).getWritableDatabase();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree() {
                @Override
                protected String createStackElementTag(StackTraceElement element) {
                    return super.createStackElementTag(element) + " : " + element.getLineNumber();
                }
            });
        }
        Timber.i("Launch Application");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static Siap getInstance() {
        return instance;
    }

    public static boolean memilikiNetwork() {
        return instance.checkNetwork();
    }

    public boolean checkNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}