package gov.tangerang.siap.datamiskin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.util.FindNamaJalan;
import gov.tangerang.siap.datamiskin.util.GPSTracker;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import timber.log.Timber;

/**
 * Created by tri on 8/1/16.
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    @Bind(R.id.btn_update_lokasi)
    Button mBtnUpdateLokasi;
    @Bind(R.id.btn_set_lokasi)
    Button mBtnSetLokasi;

    private GoogleMap googleMap;
    private GPSTracker gps;
    private double cameraLat = 0;
    private double cameraLang = 0;
    private String intentLat = "0";
    private String intentLang = "0";
    private String namaJalan = "";
    private FindNamaJalan mFindNamaJalan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        gps = new GPSTracker(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        intentLat = intent.getStringExtra("lat");
        intentLang = intent.getStringExtra("lon");

        Timber.e(intentLang);
        Timber.e(intentLat);

        if (intentLat == null && intentLang == null) {
            intentLang = "0";
            intentLat = "0";
        }
        mFindNamaJalan = new FindNamaJalan(getApplicationContext());
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (this.googleMap != null) {
            this.googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    cameraLang = cameraPosition.target.longitude;
                    cameraLat = cameraPosition.target.latitude;

                    /*Task.delay(1500).onSuccess(new Continuation<Void, String>() {
                        @Override
                        public String then(Task<Void> task){
                            try{
                                namaJalan = mFindNamaJalan.pengguna(cameraLat,cameraLang);
                                return namaJalan;
                            }catch (Exception e){}
                            return null;
                        }
                    },Task.BACKGROUND_EXECUTOR).onSuccess(new Continuation<String, Object>() {
                        @Override
                        public Object then(Task<String> task) throws Exception {
                            Timber.e(task.getResult());
                            return null;
                        }
                    },Task.UI_THREAD_EXECUTOR);*/
                }
            });
            if (intentLat.equals("0") || intentLang.equals("0")) {
                initDefaultMap(new LatLng(-6.17135965877, 106.6406523));
            }else{
                initDefaultMap(new LatLng(Double.parseDouble(intentLat), Double.parseDouble(intentLang)));
            }
        }
    }

    @OnClick(R.id.btn_update_lokasi)
    void updateLokasi(){
        gps = new GPSTracker(MapsActivity.this);
        if(gps.canGetLocation()){
            try{
                double lat = gps.getLatitude();
                double lang = gps.getLongitude();
                initDefaultMap(new LatLng(lat,lang));
            }catch (Exception e){
                ToastUtil.toastShow(MapsActivity.this,"Koneksi network bermasalah");
            }
        }else{
            ToastUtil.toastShow(MapsActivity.this,"Tidak bisa mendapatkan lokasi anda, mohon nyalakan GPS anda");
        }
    }

    @OnClick(R.id.btn_set_lokasi)
    void setNewLocation(){
        if(cameraLang != 0 && cameraLat != 0){
            Intent returnIntent = new Intent();
            returnIntent.putExtra("lat",String.valueOf(cameraLat));
            returnIntent.putExtra("lang",String.valueOf(cameraLang));
            setResult(RESULT_OK,returnIntent);
            finish();
        }
    }

    void initDefaultMap(LatLng latLng) {
        CameraPosition factory = new CameraPosition.Builder().target(latLng).zoom(16f).build();
        this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(factory));
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setMapToolbarEnabled(true);
    }
}
