package gov.tangerang.siap.datamiskin.adapter;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.interf.OnLoadMoreListener;
import gov.tangerang.siap.datamiskin.interf.OnRowClickListener;
import gov.tangerang.siap.datamiskin.interf.OnRowLongClickListener;
import gov.tangerang.siap.datamiskin.model.response.CariKeluarga;

/**
 * Created by tri on 8/11/16.
 */
public class ListKeluargaAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private OnRowLongClickListener mOnRowLongClickListener;

    List<CariKeluarga> mKeluargas;

    public void setData(List<CariKeluarga> mkKeluargas) {
        mKeluargas = mkKeluargas;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mKeluargas.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public ListKeluargaAdapter(List<CariKeluarga> mKeluargas, RecyclerView recyclerView) {
        this.mKeluargas = mKeluargas;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.content_list_keluarga, parent, false);
            vh = new HolderKeluarga(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_item, parent, false);
            vh = new ProgressBarHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HolderKeluarga) {
            CariKeluarga keluarga = mKeluargas.get(position);
            HolderKeluarga hold = (HolderKeluarga) holder;
            hold.mTxtKk.setText(keluarga.getNoKk());
            hold.mTxtRt.setText(keluarga.getNoRt());
            hold.mTxtRw.setText(keluarga.getNoRw());

            if (keluarga.getStatusAktif() != null) {
                switch (keluarga.getStatusAktif()) {
                    case "0":
                        hold.mTxtStatusAktif.setText("Tidak Aktif");
                        break;
                    case "1":
                        hold.mTxtStatusAktif.setText("Aktif");
                        break;
                    case "2":
                        hold.mTxtStatusAktif.setText("Pindah rumah");
                        break;
                    case "3":
                        hold.mTxtStatusAktif.setText("Meninggal");
                        break;
                }
            }

            if (keluarga.getStatusKota() != null) {
                switch (keluarga.getStatusKota()) {
                    case "0":
                        hold.mTxtStatus.setText(" Belum cek KK");
                        hold.mColorStatus.setBackgroundColor(Color.parseColor("#eeeeee"));
                        hold.mLayoutRt.setVisibility(View.GONE);
                        break;
                    case "1":
                        hold.mTxtStatus.setText(" Kota Tangerang");
                        hold.mColorStatus.setBackgroundColor(Color.parseColor("#03A9F4"));
                        hold.mLayoutRt.setVisibility(View.VISIBLE);
                        break;
                    case "2":
                        hold.mTxtStatus.setText(" Luar Kota Tangerang");
                        hold.mColorStatus.setBackgroundColor(Color.parseColor("#FF5252"));
                        hold.mLayoutRt.setVisibility(View.VISIBLE);
                        break;
                }
            }else{
                hold.mTxtStatus.setText(" Belum cek KK");
                hold.mColorStatus.setBackgroundColor(Color.parseColor("#eeeeee"));
                hold.mLayoutRt.setVisibility(View.GONE);
            }
        } else {
            ((ProgressBarHolder) holder).progresbar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mKeluargas.size();
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    class HolderKeluarga extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        @Bind(R.id.txt_kk)
        TextView mTxtKk;
        @Bind(R.id.txt_rt)
        TextView mTxtRt;
        @Bind(R.id.layout_rt)
        LinearLayout mLayoutRt;
        @Bind(R.id.txt_rw)
        TextView mTxtRw;
        @Bind(R.id.txt_status_aktif)
        TextView mTxtStatusAktif;
        @Bind(R.id.color_status)
        View mColorStatus;
        @Bind(R.id.txt_status)
        TextView mTxtStatus;
        @Bind(R.id.relative_card)
        RelativeLayout mRelativeCard;
        @Bind(R.id.card)
        CardView mCard;


        HolderKeluarga(View view) {
            super(view);
            ButterKnife.bind(this, view);
            mCard.setOnClickListener(this);
            mRelativeCard.setOnClickListener(this);
            mRelativeCard.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnRowClickListener.onRowClicked(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            mOnRowLongClickListener.onLongRowClick(getAdapterPosition());
            return true;
        }
    }

    OnRowClickListener mOnRowClickListener;

    public void setOnRowLongClickListener(OnRowLongClickListener onRowLongClickListener) {
        mOnRowLongClickListener = onRowLongClickListener;
    }

    public void setOnRowClickListener(OnRowClickListener onRowClickListener) {
        mOnRowClickListener = onRowClickListener;
    }
}
