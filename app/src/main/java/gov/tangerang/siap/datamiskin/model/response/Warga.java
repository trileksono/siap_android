package gov.tangerang.siap.datamiskin.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by tri on 7/29/16.
 */
public class Warga implements Parcelable {
    String nik;
    String noKk;
    String nama;
    String foto;
    String alamat;
    String kecamatan;
    String kelurahan;
    String noRt;
    String noRw;
    String statusKota;
    String statusAktif;
    String userId;
    List<JPersonal> jaminan;

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getStatusAktif() {
        return statusAktif;
    }

    public void setStatusAktif(String statusAktif) {
        this.statusAktif = statusAktif;
    }

    public String getStatusKota() {
        return statusKota;
    }

    public void setStatusKota(String statusKota) {
        this.statusKota = statusKota;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<JPersonal> getJaminan() {
        return jaminan;
    }

    public void setJaminan(List<JPersonal> jaminan) {
        this.jaminan = jaminan;
    }

    public String getNoKk() {
        return noKk;
    }

    public void setNoKk(String noKk) {
        this.noKk = noKk;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public String getNoRt() {
        return noRt;
    }

    public void setNoRt(String noRt) {
        this.noRt = noRt;
    }

    public String getNoRw() {
        return noRw;
    }

    public void setNoRw(String noRw) {
        this.noRw = noRw;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nik);
        dest.writeString(this.noKk);
        dest.writeString(this.nama);
        dest.writeString(this.foto);
        dest.writeString(this.alamat);
        dest.writeString(this.kecamatan);
        dest.writeString(this.kelurahan);
        dest.writeString(this.noRt);
        dest.writeString(this.noRw);
        dest.writeString(this.statusAktif);
        dest.writeString(this.statusKota);
        dest.writeString(this.userId);
        dest.writeTypedList(this.jaminan);
    }

    public Warga() {
    }

    protected Warga(Parcel in) {
        this.nik = in.readString();
        this.noKk = in.readString();
        this.nama = in.readString();
        this.foto = in.readString();
        this.alamat = in.readString();
        this.kecamatan = in.readString();
        this.kelurahan = in.readString();
        this.noRt = in.readString();
        this.noRw = in.readString();
        this.statusKota = in.readString();
        this.statusAktif = in.readString();
        this.userId = in.readString();
        this.jaminan = in.createTypedArrayList(JPersonal.CREATOR);
    }

    public static final Creator<Warga> CREATOR = new Creator<Warga>() {
        @Override
        public Warga createFromParcel(Parcel source) {
            return new Warga(source);
        }

        @Override
        public Warga[] newArray(int size) {
            return new Warga[size];
        }
    };
}
