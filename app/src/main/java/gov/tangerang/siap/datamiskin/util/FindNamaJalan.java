package gov.tangerang.siap.datamiskin.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by tri on 8/7/16.
 */
public class FindNamaJalan {

    Context mContext;

    public FindNamaJalan(Context context) {
        mContext = context;
    }

    public String pengguna(double lat, double lang){
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(mContext, Locale.getDefault());
        if (lat == 0 || lang == 0) {
            return "";
        }
        try {
            addresses = geocoder.getFromLocation(lat, lang, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            return address + ", " + city + ", " + state + "," + postalCode;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
