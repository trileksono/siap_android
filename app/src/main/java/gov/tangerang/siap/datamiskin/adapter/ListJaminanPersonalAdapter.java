package gov.tangerang.siap.datamiskin.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.model.response.JPersonal;

/**
 * Created by tri on 8/14/16.
 */
public class ListJaminanPersonalAdapter extends BaseAdapter {

    private Context mContext;
    private List mDbList;
    private LayoutInflater mInflater;
    private List<JPersonal> mResList;

    public ListJaminanPersonalAdapter(Context context, List list, List<JPersonal> resList) {
        mContext = context;
        mDbList= list;
        mResList = resList;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mDbList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDbList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.content_list_jaminan, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.mNamaJaminan.setText(mDbList.get(position).toString());
        holder.mChkPart.setVisibility(View.VISIBLE);
        holder.mChkPart.setChecked(false);
        for(int x = 0; x < mResList.size(); x++){
            if(mResList.get(x).getNama().equals(mDbList.get(position))){
                if(mResList.get(x).getStatus() == null){
                    holder.mChkPart.setChecked(true);
                }
            }
        }

        return convertView;
    }

    class ViewHolder {
        @Bind(R.id.nama_jaminan)
        TextView mNamaJaminan;
        @Bind(R.id.tipe_jaminan)
        TextView mTipeJaminan;
        @Bind(R.id.chk_part)
        AppCompatCheckBox mChkPart;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
