package gov.tangerang.siap.datamiskin.adapter;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.interf.OnLoadMoreListener;
import gov.tangerang.siap.datamiskin.interf.OnRowClickListener;
import gov.tangerang.siap.datamiskin.interf.OnRowLongClickListener;
import gov.tangerang.siap.datamiskin.model.response.Warga;

/**
 * Created by tri on 8/13/16.
 */
public class ListPersonalAdapter extends RecyclerView.Adapter {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private OnRowClickListener mOnRowClickListener;
    private OnRowLongClickListener mRowLongClickListener;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;

    public void setOnRowClickListener(OnRowClickListener onRowClickListener) {
        mOnRowClickListener = onRowClickListener;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setRowLongClickListener(OnRowLongClickListener rowLongClickListener) {
        mRowLongClickListener = rowLongClickListener;
    }

    List<Warga> mList;

    public ListPersonalAdapter(List<Warga> list, RecyclerView recyclerView) {
        this.mList = list;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.content_list_personal, parent, false);
            vh = new PersonalHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_item, parent, false);
            vh = new ProgressBarHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PersonalHolder) {
            Warga warga = mList.get(position);
            PersonalHolder hold = (PersonalHolder) holder;
            hold.mTxtKk.setText(warga.getNik());
            hold.mTxtRt.setText(warga.getNoRt());
            hold.mTxtRw.setText(warga.getNoRw());

            String HURUF = "";
            if(warga.getNama() != null){
                hold.mTxtNama.setText(warga.getNama());
                HURUF = String.valueOf(warga.getNama().charAt(0));
            }

            if (warga.getStatusAktif() != null) {
                switch (warga.getStatusAktif()) {
                    case "0":
                        hold.mTxtStatusAktif.setText("Meninggal");
                        break;
                    case "1":
                        hold.mTxtStatusAktif.setText("Aktif");
                        break;
                    case "2":
                        hold.mTxtStatusAktif.setText("Pindah rumah");
                        break;
                    case "3":
                        hold.mTxtStatusAktif.setText("Mampu");
                        break;
                    case "4":
                        hold.mTxtStatusAktif.setText("Tidak Ditemukan");
                }
            }else{
                hold.mTxtStatusAktif.setText("Aktif");
            }
            TextDrawable tex= null;
            if (warga.getStatusKota() != null) {
                switch (warga.getStatusKota()) {
                    case "0":
                        hold.mTxtStatus.setText(" Belum cek NIK");
                        hold.mColorStatus.setBackgroundColor(Color.parseColor("#eeeeee"));
                        hold.mLayoutRt.setVisibility(View.GONE);
                        tex = TextDrawable.builder().buildRound(HURUF,
                                Color.parseColor("#eeeeee"));
                        hold.mIconWord.setImageDrawable(tex);
                        break;
                    case "1":
                        hold.mTxtStatus.setText(" Kota Tangerang");
                        hold.mColorStatus.setBackgroundColor(Color.parseColor("#03A9F4"));
                        hold.mLayoutRt.setVisibility(View.VISIBLE);
                        tex = TextDrawable.builder().buildRound(HURUF,
                                Color.parseColor("#03A9F4"));
                        hold.mIconWord.setImageDrawable(tex);
                        break;
                    case "2":
                        hold.mTxtStatus.setText(" Luar Kota Tangerang");
                        hold.mColorStatus.setBackgroundColor(Color.parseColor("#FF5252"));
                        hold.mLayoutRt.setVisibility(View.VISIBLE);
                        tex = TextDrawable.builder().buildRound(HURUF,
                                Color.parseColor("#FF5252"));
                        hold.mIconWord.setImageDrawable(tex);
                        break;
                }
            }else{
                hold.mTxtStatus.setText(" Belum cek NIK");
                hold.mColorStatus.setBackgroundColor(Color.parseColor("#eeeeee"));
                hold.mLayoutRt.setVisibility(View.GONE);
            }

        } else {
            ((ProgressBarHolder) holder).progresbar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class PersonalHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener{
        @Bind(R.id.txt_kk)
        TextView mTxtKk;
        @Bind(R.id.txt_rt)
        TextView mTxtRt;
        @Bind(R.id.layout_rt)
        LinearLayout mLayoutRt;
        @Bind(R.id.txt_rw)
        TextView mTxtRw;
        @Bind(R.id.txt_status_aktif)
        TextView mTxtStatusAktif;
        @Bind(R.id.color_status)
        View mColorStatus;
        @Bind(R.id.txt_status)
        TextView mTxtStatus;
        @Bind(R.id.relative_card)
        RelativeLayout mRelativeCard;
        @Bind(R.id.card)
        CardView mCard;
        @Bind(R.id.txt_nama)
        TextView mTxtNama;
        @Bind(R.id.icon_word)
        ImageView mIconWord;

        public PersonalHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mRelativeCard.setOnClickListener(this);
            mCard.setOnClickListener(this);
            mRelativeCard.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnRowClickListener.onRowClicked(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            mRowLongClickListener.onLongRowClick(getAdapterPosition());
            return true;
        }
    }
}
