package gov.tangerang.siap.datamiskin.database;

/**
 * Created by tri on 7/29/16.
 */
public class DbUser {

    String nip;
    String nama;
    String jabatan;
    String kecamatan;
    String kelurahan;
    String idPegawai;
    String userId;
    String login;

    public DbUser() {
    }

    public DbUser(String nip, String nama, String jabatan, String kecamatan, String kelurahan,
                  String idPegawai, String userId, String login) {
        this.nip = nip;
        this.nama = nama;
        this.jabatan = jabatan;
        this.kecamatan = kecamatan;
        this.kelurahan = kelurahan;
        this.idPegawai = idPegawai;
        this.userId = userId;
        this.login = login;
    }

    public DbUser(String nama, String jabatan, String kecamatan, String kelurahan,
                  String idPegawai, String userId, String login) {
        this.nama = nama;
        this.jabatan = jabatan;
        this.kecamatan = kecamatan;
        this.kelurahan = kelurahan;
        this.idPegawai = idPegawai;
        this.userId = userId;
        this.login = login;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getIdPegawai() {
        return idPegawai;
    }

    public void setIdPegawai(String idPegawai) {
        this.idPegawai = idPegawai;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
