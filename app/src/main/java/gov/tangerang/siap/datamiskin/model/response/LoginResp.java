package gov.tangerang.siap.datamiskin.model.response;

/**
 * Created by tri on 8/1/16.
 */
public class LoginResp {

    private String kecamatan;
    private String jabatan;
    private String idPegawai;
    private String kodeUnor;
    private String userId;
    private String idUnor;
    private String nama;
    private String nip;
    private String kelurahan;

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getIdPegawai() {
        return idPegawai;
    }

    public void setIdPegawai(String idPegawai) {
        this.idPegawai = idPegawai;
    }

    public String getKodeUnor() {
        return kodeUnor;
    }

    public void setKodeUnor(String kodeUnor) {
        this.kodeUnor = kodeUnor;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIdUnor() {
        return idUnor;
    }

    public void setIdUnor(String idUnor) {
        this.idUnor = idUnor;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }
}
