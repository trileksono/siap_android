package gov.tangerang.siap.datamiskin.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;

/**
 * Created by tri on 8/11/16.
 */
public class MenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public OnClickMenu mOnClickMenu;
    List<Integer> listColor = new ArrayList<>();
    List<String> lisJudul = new ArrayList<>();
    List<Drawable> listIcon = new ArrayList();
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;

    public MenuAdapter(Context mContext) {
        lisJudul.add("Profile Keluarga");
        lisJudul.add("Profile Personal");
        lisJudul.add("Jaminan Keluarga");
        lisJudul.add("Jaminan Personal");

        listIcon.add(ContextCompat.getDrawable(mContext,R.drawable.ic_profile));
        listIcon.add(ContextCompat.getDrawable(mContext,R.drawable.ic_persons));
        listIcon.add(ContextCompat.getDrawable(mContext,R.drawable.ic_jkeluarga));
        listIcon.add(ContextCompat.getDrawable(mContext,R.drawable.ic_jpersonal));

//        listColor.add(Color.parseColor("#9C27B0"));
//        listColor.add(Color.parseColor("#00BCD4"));
//        listColor.add(Color.parseColor("#009688"));
//        listColor.add(Color.parseColor("#4CAF50"));
    }

    public interface OnClickMenu{
        public void onClickMenu(int position);
    }

    public void setOnClickMenu(OnClickMenu onClickMenu) {
        mOnClickMenu = onClickMenu;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_item_menu, parent, false);
        return new MenuHolder(mView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MenuHolder hold = (MenuHolder) holder;
//        hold.mCardView.setBackgroundColor(listColor.get(position));
        hold.mCardView.setBackgroundColor(mColorGenerator.getRandomColor());
        hold.mTitle.setText(lisJudul.get(position));
        hold.mImageView.setImageDrawable(listIcon.get(position));

    }


    @Override
    public int getItemCount() {
        return lisJudul.size();
    }

    class MenuHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.title)
        TextView mTitle;
        @Bind(R.id.img_menu)
        ImageView mImageView;
        @Bind(R.id.card_menu)
        CardView mCardView;

        public MenuHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            mCardView.setOnClickListener(this);
            mTitle.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnClickMenu.onClickMenu(getAdapterPosition());
        }
    }
}
