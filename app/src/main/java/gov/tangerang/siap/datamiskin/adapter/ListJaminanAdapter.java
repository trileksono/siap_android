package gov.tangerang.siap.datamiskin.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.model.response.ListJaminan;

/**
 * Created by tri on 8/3/16.
 */
public class ListJaminanAdapter extends BaseAdapter {

    private Context mContext;
    private List<ListJaminan> mList;
    private LayoutInflater mInflater;
    private int jumlahAnggota;

    public ListJaminanAdapter(Context context, List<ListJaminan> list,int jumlahKeluarga) {
        mContext = context;
        mList = list;
        mInflater = LayoutInflater.from(mContext);
        this.jumlahAnggota = jumlahKeluarga;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.content_list_jaminan, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mNamaJaminan.setText(mList.get(position).getNamaJaminan());

        if (mList.get(position).getTipeJaminan() == 1) {
            holder.mTipeJaminan.setText("Personal");
            if(mList.get(position).getTotal() != 0){
                if(mList.get(position).getTotal() / jumlahAnggota == 1){
                    holder.mChkFull.setVisibility(View.VISIBLE);
                }else{
                    holder.mChkPart.setVisibility(View.VISIBLE);
                }
            }else{
                holder.mChkFull.setVisibility(View.VISIBLE);
                holder.mChkFull.setChecked(false);
            }
        }else{
            holder.mTipeJaminan.setText("Keluarga");
            holder.mChkFull.setVisibility(View.VISIBLE);
            if(mList.get(position).getTotal() != 0){
                holder.mChkFull.setChecked(true);
            }else{
                holder.mChkFull.setChecked(false);
            }
        }
        return convertView;
    }

    class ViewHolder {
        @Bind(R.id.nama_jaminan)
        TextView mNamaJaminan;
        @Bind(R.id.tipe_jaminan)
        TextView mTipeJaminan;
        @Bind(R.id.chk_full)
        AppCompatCheckBox mChkFull;
        @Bind(R.id.chk_part)
        AppCompatCheckBox mChkPart;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
