package gov.tangerang.siap.datamiskin.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.model.response.ListJaminan;

/**
 * Created by tri on 7/28/16.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final String DBNAME = "SIAP";
    private static final int DBVERSION = 1;


    // ######## DbKecamatan ######
    private static final String TBL_CAMAT = "kecamatan";
    private static final String CMT_ID = "idKecamatan";
    private static final String CMT_NAMA = "nama";

    //######## DbKelurahan #######
    private static final String TBL_LURAH = "kelurahan";
    private static final String LRH_ID = "idKelurahan";
    private static final String LRH_NAMA = "nama";
    private static final String LRH_CMT = "idKecamatan";

    //####### USER ########
    private static final String TBL_USER = "user";
    private static final String NIP = "nip";
    private static final String NAMA = "nama";
    private static final String JABATAN = "jabatan";
    private static final String KECAMATAN = "kecamatan";
    private static final String KELURAHAN = "kelurahan";
    private static final String IDPEGAWAI = "idpegawai";
    private static final String USERID = "userid";
    private static final String LOGIN = "login";

    // ########## JAMINAN ###########
    private static final String TBL_JAMINAN = "jaminan";
    private static final String idJaminan = "idJaminan";
    private static final String namaJaminan = "nama";
    private static final String tipeJaminan = "tipe";

    private Context mContext;

    public DbHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_KECAMATAN = "CREATE TABLE " + TBL_CAMAT + "("
                + CMT_ID + " INTEGER PRIMARY KEY," + CMT_NAMA + " TEXT )";

        String CREATE_KELURAHAN = "CREATE TABLE " + TBL_LURAH + "("
                + LRH_ID + " INTEGER PRIMARY KEY," + LRH_NAMA + " TEXT,"
                + LRH_CMT + " INTEGER)";

        String CREATE_USER = "CREATE TABLE " + TBL_USER + "("
                + NIP + " TEXT PRIMARY KEY," + NAMA + " TEXT,"
                + JABATAN + " TEXT," + KECAMATAN + " TEXT,"
                + KELURAHAN + " TEXT," + IDPEGAWAI + " TEXT,"
                + USERID + " TEXT," + LOGIN + " TEXT)";

        String CREATE_JAMINAN = "CREATE TABLE " + TBL_JAMINAN + "("
                + idJaminan + " TEXT PRIMARY KEY," + namaJaminan + " TEXT,"
                + tipeJaminan + " TEXT)";

        db.execSQL(CREATE_KECAMATAN);
        db.execSQL(CREATE_KELURAHAN);
        db.execSQL(CREATE_USER);
        db.execSQL(CREATE_JAMINAN);

        this.initKecamatan(db);
        this.initKelurahan(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private void initKecamatan(SQLiteDatabase db) {
        String[] listCamat = mContext.getResources().getStringArray(R.array.list_camat);
        for (int x = 0; x < listCamat.length; x++) {
            ContentValues v = new ContentValues();
            v.put(CMT_NAMA, listCamat[x]);
            db.insert(TBL_CAMAT, null, v);
        }
    }

    private void initKelurahan(SQLiteDatabase db) {
        String[] idCamat = mContext.getResources().getStringArray(R.array.id_cmt);
        String[] listLurah = mContext.getResources().getStringArray(R.array.list_kelurahan);
        for (int x = 0; x < listLurah.length; x++) {
            ContentValues v = new ContentValues();
            v.put(LRH_NAMA, listLurah[x]);
            v.put(LRH_CMT, idCamat[x]);
            db.insert(TBL_LURAH, null, v);
        }
    }

    public List getAllKecamatan() {
        List<String> contactList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TBL_CAMAT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                contactList.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        db.close();
        return contactList;
    }

    public List getAllKelurahan() {
        List<String> contactList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TBL_LURAH;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                contactList.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        db.close();
        return contactList;
    }

    public List<String> getKecamatanByLurah(String idCamat) {
        List<String> listCamat = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TBL_CAMAT+ " WHERE " + CMT_ID
                + " = (select "+LRH_CMT +" from "+TBL_LURAH +" where "+LRH_ID +" = "+ idCamat + ")";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                listCamat.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        db.close();
        return listCamat;
    }

    public String getKelurahanByNama(String nama) {
        String kelurahan = "";
        String selectQuery = "SELECT  * FROM " + TBL_LURAH + " WHERE " + LRH_NAMA
                + " = " + nama + "";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            kelurahan = cursor.getString(1);
        }
        db.close();
        return kelurahan;
    }

    public List getJaminan(String tipe) {
        List<String> listJaminan = new ArrayList<>();
        String select = "select * from " + TBL_JAMINAN + " where " + tipeJaminan + " = " + tipe + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(select, null);
        if (cursor.moveToFirst()) {
            do {
                listJaminan.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        db.close();
        return listJaminan;
    }

    public List getAllJaminan() {
        List<DbJaminan> listJaminan = new ArrayList<>();
        String select = "select * from " + TBL_JAMINAN + ";";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(select, null);
        if (cursor.moveToFirst()) {
            do {
                DbJaminan jm = new DbJaminan();
                jm.setIdJaminan(Integer.parseInt(cursor.getString(0)));
                jm.setNama(cursor.getString(1));
                jm.setTipe(cursor.getString(2));
                listJaminan.add(jm);
            } while (cursor.moveToNext());
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        db.close();
        return listJaminan;
    }

    public void insertJaminan(List<ListJaminan> list) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            for (int x = 0; x < list.size(); x++) {
                ContentValues values = new ContentValues();
                values.put(idJaminan, list.get(x).getIdJaminan());
                values.put(namaJaminan, list.get(x).getNamaJaminan());
                values.put(tipeJaminan, list.get(x).getTipeJaminan());

                db.insertWithOnConflict(TBL_JAMINAN, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }
        } finally {
            db.close();
        }
    }

    public void insertUser(DbUser user) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(NIP, user.getNip());
            values.put(NAMA, user.getNama());
            values.put(USERID, user.getUserId());
            db.insertWithOnConflict(TBL_USER, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        } finally {
            db.close();
        }
    }

    public List<String> getAllUser(){
        List<String> listUser = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TBL_USER;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                listUser.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        if(!cursor.isClosed()){
            cursor.close();
        }
        db.close();
        return listUser;
    }
}
