package gov.tangerang.siap.datamiskin.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.util.Constant;
import timber.log.Timber;

/**
 * Created by tri on 8/4/16.
 */
public class ImgZoom extends Dialog {

    String path;
    Context mContext;

    public ImgZoom(Context context,String path) {
        super(context);
        this.path = path;
        this.mContext = context;
    }

    public OnZoomClickListener myListener;

    // This is my interf //
    public interface OnZoomClickListener {
        void onImageZoom();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.image_zoom);
        ImageView img = (ImageView) findViewById(R.id.img);

        if (path.startsWith("/")) {
            Timber.e(path);
            Picasso.with(mContext).load(new File(path)).error(R.drawable.no_foto).fit().into(img);
        } else {
            Timber.e(Constant.IMG_URL+path);
            Picasso.with(mContext).load(Constant.IMG_URL+path).error(R.drawable.no_foto).fit().into(img);
        }
    }


}
