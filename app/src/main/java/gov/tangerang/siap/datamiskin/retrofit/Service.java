package gov.tangerang.siap.datamiskin.retrofit;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import gov.tangerang.siap.datamiskin.BuildConfig;
import gov.tangerang.siap.datamiskin.Siap;
import gov.tangerang.siap.datamiskin.util.Constant;
import gov.tangerang.siap.datamiskin.util.PrefUtil;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by tri on 7/27/16.
 */
public class Service {

    private static final String CACHE_CONTROL = "Cache-Control";
    private Context mContext;

    public API getApi(Context mContext) {
        this.mContext = mContext;

        return mRetrofit().create(API.class);
    }

    public Retrofit mRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Constant.URL_BASE)
                .client(mClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private OkHttpClient mClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor())
//                .addInterceptor(whenOfflineCacheInterceptor())
//                .addNetworkInterceptor(cacheInterceptor())
                .addInterceptor(setCookie())
                .addInterceptor(getCookie())
                .connectTimeout(Constant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(Constant.WRITE_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(Constant.READ_TIME_OUT, TimeUnit.SECONDS)
//                .cache(buatCache())
                .build();
    }

    private static Cache buatCache() {
        Cache cache = null;
        try {
            cache = new Cache(new File(Siap.getInstance().getCacheDir(), "http-cache"), 10 * 1024 * 1024); // 10 MB
        } catch (Exception e) {
            Timber.e(e, "Error creating cache file");
        }
        return cache;
    }

    private static HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor =
                new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Timber.d(message);
                    }
                });
        httpLoggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.HEADERS : HttpLoggingInterceptor.Level.NONE);
        return httpLoggingInterceptor;
    }

    public static Interceptor cacheInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());

                // re-write response header to force use of cache
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES)
                        .build();

                return response.newBuilder()
                        .header(CACHE_CONTROL, cacheControl.toString())
                        .build();
            }
        };
    }

    public static Interceptor whenOfflineCacheInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();


                if (!Siap.memilikiNetwork()) {
                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale(7, TimeUnit.DAYS)
                            .build();

                    request = request.newBuilder()
                            .cacheControl(cacheControl)
                            .build();
                }
                return chain.proceed(request);
            }
        };
    }

    public Interceptor setCookie() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response originalResponse = chain.proceed(chain.request());

                if (!originalResponse.headers("Set-Cookie").isEmpty()) {
                    HashSet<String> cookies = new HashSet<>();

                    for (String header : originalResponse.headers("Set-Cookie")) {
                        cookies.add(header);
                    }
                    PrefUtil.setMapPref(mContext, Constant.PREF_SES, cookies);
                }

                return originalResponse;
            }
        };
    }

    public Interceptor getCookie() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();
                HashSet<String> preferences = PrefUtil.getMapPref(mContext, Constant.PREF_SES);
                if (preferences != null) {
                    for (String cookie : preferences) {
                        builder.addHeader("Cookie", cookie);
                        Timber.i(cookie); // This is done so I know which headers are being added; this interceptor is used after the normal logging of OkHttp
                    }
                }

                return chain.proceed(builder.build());
            }
        };
    }
}
