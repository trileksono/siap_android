package gov.tangerang.siap.datamiskin.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.adapter.ListKeluargaAdapter;
import gov.tangerang.siap.datamiskin.interf.OnLoadMoreListener;
import gov.tangerang.siap.datamiskin.interf.OnRowClickListener;
import gov.tangerang.siap.datamiskin.interf.OnRowLongClickListener;
import gov.tangerang.siap.datamiskin.model.DTOList;
import gov.tangerang.siap.datamiskin.model.response.CariKeluarga;
import gov.tangerang.siap.datamiskin.retrofit.Service;
import gov.tangerang.siap.datamiskin.ui.fragment.KeluargaProfileActivity;
import gov.tangerang.siap.datamiskin.util.Constant;
import gov.tangerang.siap.datamiskin.util.MyProgressDialog;
import gov.tangerang.siap.datamiskin.util.PrefUtil;
import gov.tangerang.siap.datamiskin.util.ToastUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by tri on 8/11/16.
 */
public class ListKeluargaActivity extends AppCompatActivity implements OnLoadMoreListener, OnRowClickListener,
        OnRowLongClickListener{

    @Bind(R.id.recycle_keluarga)
    RecyclerView mRecycleKeluarga;
    @Bind(R.id.progresbar)
    ProgressBar mProgresbar;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    int page = 0;
    ListKeluargaAdapter adapter;
    LinearLayoutManager mLayoutManager;
    List<CariKeluarga> mKeluargas = new ArrayList<>();

    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;
    private String KEYWORD = "";
    private MyProgressDialog mProgresDialog;
    private CancellationTokenSource mCancellation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_list);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mProgresDialog = new MyProgressDialog(this, "Mohon tunggu", false, true, "");
        mCancellation = new CancellationTokenSource();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Task.delay(1000).onSuccess(new Continuation<Void, Response<DTOList<CariKeluarga>>>() {
            @Override
            public Response<DTOList<CariKeluarga>> then(Task<Void> task) {
                Response<DTOList<CariKeluarga>> res = null;
                try {
                    res = new Service().getApi(ListKeluargaActivity.this).getKeluargaPagging(page, "").execute();
                    if (res.isSuccessful()) {
                        return res;
                    }
                } catch (IOException e) {
                    finish();
                    ToastUtil.toastOnUi(ListKeluargaActivity.this, getResources().getString(R.string.error_message1));
                }
                return null;
            }
        }, Task.BACKGROUND_EXECUTOR, mCancellation.getToken()).onSuccess(new Continuation<Response<DTOList<CariKeluarga>>, Object>() {
            @Override
            public Object then(Task<Response<DTOList<CariKeluarga>>> task) throws Exception {
                if (task.getResult() == null) {
                    Timber.e("NULL");
                    ToastUtil.toastShow(ListKeluargaActivity.this, getResources().getString(R.string.error_message1));
                    finish();
                    return null;
                }
                if (task.getResult().body().getErrorCode().equals("20")) {
                    PrefUtil.setStringPref(ListKeluargaActivity.this, Constant.isLogedIn, "0");
                    Intent intent = new Intent(ListKeluargaActivity.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    ToastUtil.toastOnUi(ListKeluargaActivity.this, task.getResult().body().getMessage());
                    finish();

                } else if (task.getResult().body().getErrorCode().equals("00")) {
                    mKeluargas = task.getResult().body().getData();
                    mRecycleKeluarga.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(ListKeluargaActivity.this);
                    mRecycleKeluarga.setLayoutManager(mLayoutManager);
                    adapter = new ListKeluargaAdapter(mKeluargas, mRecycleKeluarga);
                    mRecycleKeluarga.setAdapter(adapter);
                    adapter.setOnLoadMoreListener(ListKeluargaActivity.this);
                    adapter.setOnRowClickListener(ListKeluargaActivity.this);
                    adapter.setOnRowLongClickListener(ListKeluargaActivity.this);
                    mProgresbar.setVisibility(View.GONE);
                    mRecycleKeluarga.setVisibility(View.VISIBLE);
                } else {
                    Timber.e("MASUK");
                    ToastUtil.toastShow(ListKeluargaActivity.this, task.getResult().body().getMessage());
                    finish();
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR, mCancellation.getToken());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCancellation.cancel();
    }

    @Override
    public void onLoadMore() {
        Timber.e(KEYWORD);
        mKeluargas.add(null);
        adapter.notifyItemInserted(mKeluargas.size() - 1);
        loadMore(KEYWORD);
    }

    private void loadMore(String status) {
        page++;
        if (page > 400) {
            mKeluargas.remove(mKeluargas.size() - 1);
            adapter.notifyItemRemoved(mKeluargas.size());
            ToastUtil.toastShow(ListKeluargaActivity.this, "Tidak ada lagi data");
            return;
        }
        new Service().getApi(ListKeluargaActivity.this).getKeluargaPagging(page, status).enqueue(new Callback<DTOList<CariKeluarga>>() {
            @Override
            public void onResponse(Call<DTOList<CariKeluarga>> call, Response<DTOList<CariKeluarga>> response) {
                mKeluargas.remove(mKeluargas.size() - 1);
                adapter.notifyItemRemoved(mKeluargas.size());
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        for (int x = 0; x < response.body().getData().size(); x++) {
                            mKeluargas.add(response.body().getData().get(x));
                            adapter.notifyItemInserted(mKeluargas.size());
                        }
                        adapter.setLoaded();
                        if (response.body().getData().size() < 1) {
                            page = 500;
                        }
                    } else {
                        ToastUtil.toastOnUi(ListKeluargaActivity.this, response.body().getMessage());
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<DTOList<CariKeluarga>> call, Throwable t) {
                ToastUtil.toastOnUi(ListKeluargaActivity.this, getResources().getString(R.string.error_message1));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_search:
                handleMenuSearch();
                return true;
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.menu_search);
        return super.onPrepareOptionsMenu(menu);
    }

    protected void handleMenuSearch() {
        ActionBar action = getSupportActionBar(); //get the actionbar

        if (isSearchOpened) { //test if the search is open
            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
            action.setDisplayShowTitleEnabled(true); //show the title in the action bar
            //hides the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtSeach.getWindowToken(), 0);

            //add the search icon in the action bar
            mSearchAction.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_search));

            isSearchOpened = false;
        } else { //open the search entry
            action.setDisplayShowCustomEnabled(true); //enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.search_bar);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title
            edtSeach = (EditText) action.getCustomView().findViewById(R.id.edtSearch); //the text editor

            //this is a listener to do a search when the user clicks on search button
            edtSeach.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    v.setInputType(InputType.TYPE_CLASS_NUMBER);
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        doSearch(v.getText().toString());
                        return true;
                    }
                    return false;
                }
            });

            edtSeach.requestFocus();

            //open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);
            //add the close icon
            mSearchAction.setIcon(ContextCompat.getDrawable(this, android.R.drawable.ic_menu_close_clear_cancel));
            isSearchOpened = true;
        }
    }

    private void doSearch(final String kk) {
        mRecycleKeluarga.setVisibility(View.GONE);
        mProgresbar.setVisibility(View.VISIBLE);
        new Service().getApi(this).getKeluargaPagging(0, kk).enqueue(new Callback<DTOList<CariKeluarga>>() {
            @Override
            public void onResponse(Call<DTOList<CariKeluarga>> call, Response<DTOList<CariKeluarga>> response) {
                mProgresbar.setVisibility(View.GONE);
                mRecycleKeluarga.setVisibility(View.VISIBLE);
                if (response.isSuccessful()) {
                    if (response.body().getErrorCode().equals("00")) {
                        if (!response.body().getData().isEmpty()) {
                            KEYWORD = kk;
                            mKeluargas = response.body().getData();
                            adapter = new ListKeluargaAdapter(mKeluargas, mRecycleKeluarga);
                            mRecycleKeluarga.setAdapter(adapter);
                            adapter.setOnLoadMoreListener(ListKeluargaActivity.this);
                            adapter.setOnRowClickListener(ListKeluargaActivity.this);
                            adapter.setOnRowLongClickListener(ListKeluargaActivity.this);
                            page = 0;
                        } else {
                            ToastUtil.toastOnUi(ListKeluargaActivity.this, "Data tidak ditemukan");
                        }
                    } else {
                        ToastUtil.toastOnUi(ListKeluargaActivity.this, response.body().getMessage());
                    }
                } else {
                    ToastUtil.toastOnUi(ListKeluargaActivity.this, getResources().getString(R.string.error_message1));
                }
            }

            @Override
            public void onFailure(Call<DTOList<CariKeluarga>> call, Throwable t) {
                mProgresbar.setVisibility(View.GONE);
                mRecycleKeluarga.setVisibility(View.VISIBLE);
                ToastUtil.toastOnUi(ListKeluargaActivity.this, getResources().getString(R.string.error_message1));
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isSearchOpened) {
            handleMenuSearch();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onRowClicked(int position) {
        Timber.d(mKeluargas.get(position).getNoKk());
        Intent intent = new Intent(ListKeluargaActivity.this, KeluargaProfileActivity.class);
        intent.putExtra("KK", mKeluargas.get(position).getNoKk());
        intent.putExtra("status", mKeluargas.get(position).getStatusKota());
        startActivity(intent);
    }


    @Override
    public void onLongRowClick(int position) {
        Timber.d(mKeluargas.get(position).getNoKk());
        ClipboardManager clipboard = (android.content.ClipboardManager) this.getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = android.content.ClipData.newPlainText("Text Coppied", mKeluargas.get(position).getNoKk());
        clipboard.setPrimaryClip(clip);
        ToastUtil.toastShow(this,"No KK telah dicopy");
    }
}
