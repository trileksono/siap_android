package gov.tangerang.siap.datamiskin.interf;

public interface OnLoadMoreListener{
    void onLoadMore();
}