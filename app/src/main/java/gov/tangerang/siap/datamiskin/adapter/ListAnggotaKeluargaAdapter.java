package gov.tangerang.siap.datamiskin.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import gov.tangerang.siap.datamiskin.R;
import gov.tangerang.siap.datamiskin.model.response.AnggotaKeluarga;

/**
 * Created by tri on 7/31/16.
 */
public class ListAnggotaKeluargaAdapter extends BaseAdapter {

    private List<AnggotaKeluarga> anggotaList;
    private LayoutInflater inflater;
    private Context mContext;

    public ListAnggotaKeluargaAdapter(List<AnggotaKeluarga> anggotaList, Context mContext) {
        this.anggotaList = anggotaList;
        this.mContext = mContext;
        inflater = LayoutInflater.from(this.mContext);
    }

    public void setData(List<AnggotaKeluarga> anggotaList){
        this.anggotaList = anggotaList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return anggotaList.size();
    }

    @Override
    public Object getItem(int position) {
        return anggotaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.content_list_anggota, parent, false);
            mViewHolder = new ViewHolder(convertView);
            convertView.setTag(mViewHolder);
        }else{
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.nik.setText(anggotaList.get(position).getNik());
        mViewHolder.nama.setText(anggotaList.get(position).getNama());
        if(anggotaList.get(position).getHubungan().equals("")){
            mViewHolder.status.setVisibility(View.GONE);
            mViewHolder.usia.setVisibility(View.GONE);
        }
        mViewHolder.status.setText(anggotaList.get(position).getHubungan());
        mViewHolder.usia.setText(anggotaList.get(position).getTglLahir());
        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.status)
        AppCompatTextView status;
        @Bind(R.id.nik)
        AppCompatTextView nik;
        @Bind(R.id.nama)
        AppCompatTextView nama;
        @Bind(R.id.usia)
        AppCompatTextView usia;
        @Bind(R.id.container_data)
        LinearLayout containerData;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
