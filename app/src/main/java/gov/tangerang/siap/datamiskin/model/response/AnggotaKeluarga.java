package gov.tangerang.siap.datamiskin.model.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tri on 7/28/16.
 */
public class AnggotaKeluarga implements Parcelable {
    private String nik;
    private String tglLahir;
    private String nama;
    private String foto;
    private String hubungan;
    private String alamat;

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getHubungan() {
        return hubungan;
    }

    public void setHubungan(String hubungan) {
        this.hubungan = hubungan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nik);
        dest.writeString(this.tglLahir);
        dest.writeString(this.nama);
        dest.writeString(this.foto);
        dest.writeString(this.hubungan);
        dest.writeString(this.alamat);
    }

    public AnggotaKeluarga() {
    }

    protected AnggotaKeluarga(Parcel in) {
        this.nik = in.readString();
        this.tglLahir = in.readString();
        this.nama = in.readString();
        this.foto = in.readString();
        this.hubungan = in.readString();
        this.alamat = in.readString();
    }

    public static final Creator<AnggotaKeluarga> CREATOR = new Creator<AnggotaKeluarga>() {
        @Override
        public AnggotaKeluarga createFromParcel(Parcel source) {
            return new AnggotaKeluarga(source);
        }

        @Override
        public AnggotaKeluarga[] newArray(int size) {
            return new AnggotaKeluarga[size];
        }
    };
}
