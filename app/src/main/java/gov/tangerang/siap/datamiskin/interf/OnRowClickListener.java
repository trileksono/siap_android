package gov.tangerang.siap.datamiskin.interf;

/**
 * Created by tri on 8/13/16.
 */
public interface OnRowClickListener {
    public void onRowClicked(int position);
}
